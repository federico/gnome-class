extern crate cc;
extern crate libgir;
extern crate pkg_config;

mod buildrs;

use buildrs::{BuildConfig, Cc, GirCodegen, GirScanner};

fn main() {
    let buildconfig = BuildConfig::new();

    Cc::build(&buildconfig);
    GirScanner::build_gir(&buildconfig);
    GirCodegen::process_gir(&buildconfig);
}
