use super::BuildConfig;
use libgir::WorkMode;
use std::io::{BufRead, BufReader, Read, Write};
use std::path::Path;

pub(crate) struct GirCodegen;

impl GirCodegen {
    pub(crate) fn process_gir(buildconfig: &BuildConfig) {
        Self::process_gir_mode(buildconfig, WorkMode::Sys);
        Self::process_gir_mode(buildconfig, WorkMode::Normal);
        Self::prepare_auto();
        Self::copy_normal_output(buildconfig);
        Self::copy_ffi_output(buildconfig);
    }

    fn prepare_auto() {
        if Path::new("auto").exists() {
            ::std::fs::remove_dir_all("auto").expect("Error removing auto");
        }
        ::std::fs::create_dir("auto").expect("Error creating auto directory");
    }

    fn copy_normal_output(buildconfig: &BuildConfig) {
        fn copy_mod<P: AsRef<Path>>(file: P) {
            let mut src = ::std::fs::File::open(file).expect("Error opening file for copy_mod");
            let mut dst = ::std::fs::File::create("auto/mod.rs")
                .expect("Error opening file for copy_mod for writing");
            let mut buf = [0u8; 1024];

            dst.write("pub mod ffi;".as_bytes())
                .expect("Error while writing");
            loop {
                let len = src.read(&mut buf).expect("Error while reading");
                if len == 0 {
                    break;
                }
                dst.write(&buf[0..len]).expect("Error while writing");
            }
        }

        fn copy_innermod<P: AsRef<Path>>(file: P) {
            let mut src = BufReader::new(
                ::std::fs::File::open(&file).expect("Error opening file for copy_mod"),
            );
            let mut dst = ::std::fs::File::create(
                Path::new("auto")
                    .join(file.as_ref().file_name().expect("innermod expect filename")),
            )
            .expect("Error opening file for copy_mod for writing");
            let mut line = String::new();

            while src.read_line(&mut line).expect("Error reading file") > 0 {
                {
                    let content = match line.as_str() {
                        "use ffi;\n" => "use super::ffi;\n",
                        "use gobject_ffi;\n" => "use gobject_sys as gobject_ffi;\n",
                        "use glib_ffi;\n" => "use glib_sys as glib_ffi;\n",
                        _ => line.as_str(),
                    };
                    dst.write(content.as_bytes()).expect("Error while writing");
                }
                line.clear();
            }
        }

        let mut builddir = buildconfig.gir_gendir();
        builddir.push("src");
        builddir.push("auto");
        ::std::fs::read_dir(&builddir)
            .expect("Unable to read gir_gendir")
            .into_iter()
            .map(|src| src.expect("Unable to read gir_gendir file").path())
            .for_each(|src| {
                if src
                    .file_name()
                    .map(|stem| stem == "mod.rs")
                    .unwrap_or(false)
                {
                    copy_mod(src)
                } else {
                    copy_innermod(src)
                }
            });
    }

    fn copy_ffi_output(buildconfig: &BuildConfig) {
        let mut src = BufReader::new(
            ::std::fs::File::open(buildconfig.gir_ffi_gendir().join("src/lib.rs"))
                .expect("Error opening ffi-file for reading"),
        );
        let mut dst =
            ::std::fs::File::create("auto/ffi.rs").expect("Error opening ffi.rs for writing");
        let mut line = String::new();
        let mut first = true;

        while src.read_line(&mut line).expect("Error reading file") > 0 {
            {
                if line.starts_with("extern crate") {
                } else if line.starts_with("use glib::") {
                    dst.write("use glib_sys::".as_bytes())
                        .expect("Error while writing");
                    dst.write(line["use glib::".len()..].as_bytes())
                        .expect("Error while writing");
                } else {
                    if line.starts_with("#[") && first {
                        dst.write(
                            "#[allow(unused_imports)]\nuse gobject_sys as gobject;\n".as_bytes(),
                        )
                        .expect("Error while writing");
                        dst.write("#[allow(unused_imports)]\nuse glib_sys as glib;\n".as_bytes())
                            .expect("Error while writing");
                        first = false;
                    }
                    dst.write(line.as_bytes()).expect("Error while writing");
                }
            }
            line.clear();
        }
    }

    fn process_gir_mode(buildconfig: &BuildConfig, workmode: WorkMode) {
        let outputdir = match workmode {
            WorkMode::Sys => buildconfig.gir_ffi_gendir(),
            WorkMode::Normal => buildconfig.gir_gendir(),
            _ => panic!("Unsupported workmode"),
        };
        let outputdir = outputdir
            .to_str()
            .expect("gir output dir is expected to convert to utf8");

        let mut cfg = ::libgir::Config::new(
            Some("Gir.toml"),
            Some(workmode),
            Some(
                buildconfig
                    .outdir()
                    .to_str()
                    .expect("OURDIR is expected to convert to utf-8"),
            ),
            Some("Ctest"),
            Some("0.1"),
            Some(outputdir),
            None,
            None,
            None,
        )
        .expect("Error constructing configuration for libgir");
        let mut library = ::libgir::Library::new(&cfg.library_name);
        library
            .read_file(&cfg.girs_dir, &cfg.library_full_name())
            .expect("Error reading gir file");
        library.preprocessing(cfg.work_mode);
        ::libgir::update_version::apply_config(&mut library, &cfg);
        library.postprocessing(&cfg);
        cfg.resolve_type_ids(&library);
        ::libgir::update_version::check_function_real_version(&mut library);

        let mut env;

        let namespaces = ::libgir::namespaces_run(&library);
        let symbols = ::libgir::symbols_run(&library, &namespaces);
        let class_hierarchy = ::libgir::class_hierarchy_run(&library);

        env = ::libgir::Env {
            library,
            config: cfg,
            namespaces,
            symbols: ::std::cell::RefCell::new(symbols),
            class_hierarchy,
            analysis: Default::default(),
        };

        if env.config.work_mode != ::libgir::WorkMode::Sys {
            ::libgir::analysis_run(&mut env);
        }

        if env.config.work_mode != ::libgir::WorkMode::DisplayNotBound {
            ::libgir::codegen_generate(&env);
        }

        if env.config.work_mode == ::libgir::WorkMode::DisplayNotBound {
            env.library.show_non_bound_types(&env);
        }
    }
}
