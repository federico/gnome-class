use super::BuildConfig;
use std::fs::read_dir;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::path::Path;
use std::process::{Command, Output};

pub(crate) struct GirScanner;

impl GirScanner {
    pub(crate) fn build_gir(buildconfig: &BuildConfig) {
        let h_files = read_dir("c").expect("Cannot read directory with c-files")
            .into_iter()
            .map(|hpath| hpath.expect("Error unpacking c-file").path())
            .filter(|hpath| hpath.extension().map(|ext| ext == "h").unwrap_or(false) /*&& hpath.file_stem().map(|hpath| hpath == "counter").unwrap_or(false)*/);

        // The path of the gir-file in the compiling directory
        let out_girfile = buildconfig.gir_file();
        // The path of gir-file which exists in the repository
        let repos_girfile = Path::new("gir/Ctest.gir");
        let mut command = Command::new("g-ir-scanner");
        command.arg("-o");
        command.arg(&out_girfile);
        command.arg("--namespace");
        command.arg("Ctest");
        command.arg("--nsversion");
        command.arg("0.1");
        command.arg("-lgobject-2.0");
        command.arg("--pkg=glib-2.0");
        command.arg("--include");
        command.arg("GObject-2.0");
        command.args(h_files);
        command.env("LDFLAGS", buildconfig.library_archive());

        if let Some(output) = Self::try_run(&mut command).expect("Error running g-ir-scanner") {
            // g-ir-scanner found and runned correctly
            if !output.status.success() {
                println!("Running g-ir-scanner {:?}", &command);
                panic!(
                    "Error executing g-ir-scanner {}",
                    ::std::str::from_utf8(&output.stderr).expect("Error converting stderr to utf8")
                );
            }
            // Copy the generated girfile to the repository to be used on systems without
            // g-ir-scanner
            ::std::fs::copy(&out_girfile, &repos_girfile)
                .expect("Error copying output girfile to girfile in the repository");
        } else {
            // Copy the checked-in girfile to the outdir.
            println!("cargo:warning=g-ir-scanner not found. Using Ctest.gir from repository");
            ::std::fs::copy(&repos_girfile, &out_girfile)
                .expect("Error copying output girfile in the repository to the output girfile");
        }

        let mut target = buildconfig.outdir().clone();
        target.push("GObject-2.0.gir");
        ::std::fs::copy("gir/GObject-2.0.gir", target.as_path())
            .expect("Copying GObject-2.0.gir failed");
        target.pop();
        target.push("GLib-2.0.gir");
        ::std::fs::copy("gir/GLib-2.0.gir", target.as_path()).expect("Copying GLib-2.0.gir failed");
    }

    fn try_run(command: &mut Command) -> Result<Option<Output>, IoError> {
        command.output().map(|output| Some(output)).or_else(|err| {
            if IoErrorKind::NotFound == err.kind() {
                Ok(None)
            } else {
                Err(err)
            }
        })
    }
}
