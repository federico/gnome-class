use super::BuildConfig;
use cc::Build;
use std::fs::read_dir;

pub(crate) struct Cc;

impl Cc {
    fn builder() -> Build {
        let glib: ::pkg_config::Library =
            ::pkg_config::probe_library("glib-2.0").expect("glib or pkgconfig not found");

        let mut build = ::cc::Build::new();
        for include in glib.include_paths.iter() {
            build.include(include);
        }
        build
    }

    pub(crate) fn build(_config: &BuildConfig) {
        // FIXME: this is unused.  Should we just pass it to .compile() below?
        // Note that .compile() is already writing to target/debug/build/.../out/libctest.a
        // let out_name = _config.outdir().clone().push("libctest.a");
        Self::builder()
            .files(
                read_dir("c")
                    .expect("Cannot read directory with c-files")
                    .map(|cfile| cfile.expect("Error reading directory listing"))
                    .map(|cfile| cfile.path())
                    .filter(|cfile| cfile.extension().map(|ext| ext == "c").unwrap_or(false)),
            )
            .compile("libctest.a");
    }
}
