mod cc;
mod gir_codegen;
mod girscanner;

pub(crate) use self::cc::Cc;
pub(crate) use self::gir_codegen::GirCodegen;
pub(crate) use self::girscanner::GirScanner;

use std::path::PathBuf;

pub(crate) struct BuildConfig {
    outdir: PathBuf,
}

impl BuildConfig {
    pub(crate) fn new() -> Self {
        let outdir = ::std::env::var_os("OUT_DIR")
            .map(PathBuf::from)
            .expect("Expected available OUT_DIR variable");

        BuildConfig { outdir }
    }

    pub(crate) fn library_archive(&self) -> PathBuf {
        let mut res = self.outdir.clone();
        res.push("libctest.a");
        res
    }

    pub(crate) fn gir_file(&self) -> PathBuf {
        let mut res = self.outdir.clone();
        res.push("Ctest-0.1.gir");
        res
    }

    pub(crate) fn outdir(&self) -> &PathBuf {
        &self.outdir
    }

    pub(crate) fn gir_gendir(&self) -> PathBuf {
        let mut res = self.outdir.clone();
        res.push("auto");
        res
    }

    pub(crate) fn gir_ffi_gendir(&self) -> PathBuf {
        let mut res = self.outdir.clone();
        res.push("autoffi");
        res
    }
}
