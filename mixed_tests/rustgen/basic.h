/* inclusion guard */
#ifndef __COUNTER_H__
#define __COUNTER_H__

#include <glib-object.h>
/*
 * Potentially, include other headers on which this header depends.
 */

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define COUNTER_TYPE counter_get_type ()
G_DECLARE_FINAL_TYPE (Counter, counter, RS, COUNTER, GObject)

/*
 * Method definitions.
 */
Counter* counter_new();
void counter_add (Counter*, int unsigned);
int unsigned counter_get (Counter*);

G_END_DECLS

#endif /* __COUNTER_H__ */