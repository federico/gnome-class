#include "../rustgen/basic.h"
#include <stdio.h>

extern int basic_test()
{
	Counter* counter = g_object_new(COUNTER_TYPE, NULL);
	counter_add(counter, 3);
	printf("Inter: %d\n", counter_get(counter));
	counter_add(counter, 5);
	printf("Def: %d\n", counter_get(counter));

	return 0;
}
