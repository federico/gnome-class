/*
 * Copyright/Licensing information.
 */

/* inclusion guard */
#ifndef __CTEST_COUNTERIMPL_H__
#define __CTEST_COUNTERIMPL_H__

#include <glib-object.h>
/*
 * Potentially, include other headers on which this header depends.
 */

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define CTEST_COUNTERIMPL_TYPE ctest_counterimpl_get_type ()
G_DECLARE_DERIVABLE_TYPE (CtestCounterImpl, ctest_counterimpl, CTEST, COUNTERIMPL, GObject)

struct _CtestCounterImplClass
{
  GObjectClass parent_class;

  /* Padding to allow adding up to 12 new virtual functions without
   * breaking ABI. */
  gpointer padding[12];
};

/*
 * Method definitions.
 */
CtestCounterImpl* ctest_counterimpl_new (void);
gint ctest_counterimpl_get(CtestCounterImpl* this);

G_END_DECLS

#endif /* __CTEST_COUNTERIMPL_H__ */
