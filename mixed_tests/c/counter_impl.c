#include <stdio.h>
#include "counter_impl.h"
#include "interface_counter.h"

static void ctest_counterimpl_mutable_counter_interface_init(CtestMutableCounterInterface *iface);

typedef struct
{
	gint value;
} CtestCounterImplPrivate;

G_DEFINE_TYPE_WITH_CODE(CtestCounterImpl, ctest_counterimpl, G_TYPE_OBJECT,
    G_IMPLEMENT_INTERFACE (CTEST_MUTABLE_COUNTER_TYPE, ctest_counterimpl_mutable_counter_interface_init)
    G_ADD_PRIVATE(CtestCounterImpl)
)

CtestCounterImpl* ctest_counterimpl_new()
{
	return g_object_new(CTEST_COUNTERIMPL_TYPE, NULL);
}

gint ctest_counterimpl_get (CtestCounterImpl *self)
{
	g_return_val_if_fail (CTEST_IS_COUNTERIMPL (self), 0);

	//CtestCounterClass* selfclass = CTEST_COUNTER_GET_CLASS (self);
	CtestCounterImplPrivate *priv = ctest_counterimpl_get_instance_private (CTEST_COUNTERIMPL (self));

	return priv->value;
}

static void ctest_counterimpl_set_property (GObject      *self,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
    CtestCounterImplPrivate *priv = ctest_counterimpl_get_instance_private (CTEST_COUNTERIMPL (self));

    switch (property_id)
    {
        case 1:
        priv->value = g_value_get_int(value);
        break;

    default:
        /* We don't have any other property... */
        G_OBJECT_WARN_INVALID_PROPERTY_ID (self, property_id, pspec);
        break;
    }
}

static void ctest_counterimpl_get_property (GObject    *self,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
    CtestCounterImplPrivate *priv = ctest_counterimpl_get_instance_private (CTEST_COUNTERIMPL (self));

    switch (property_id)
    {
        case 1:
            g_value_set_uint (value, priv->value);
            break;

        default:
            /* We don't have any other property... */
            G_OBJECT_WARN_INVALID_PROPERTY_ID (self, property_id, pspec);
            break;
    }
}


static void ctest_counterimpl_class_init(CtestCounterImplClass *selfclass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (selfclass);

    object_class->set_property = ctest_counterimpl_set_property;
    object_class->get_property = ctest_counterimpl_get_property;

    g_object_class_override_property(object_class, 1, "iface_prop");
}

static void ctest_counterimpl_init(CtestCounterImpl* self)
{
	CtestCounterImplPrivate *priv = ctest_counterimpl_get_instance_private (CTEST_COUNTERIMPL (self));
	priv->value = 0;
}

static gint ctest_counterimpl_mutable_counter_increment(CtestMutableCounter* self, gint value)
{
    g_return_val_if_fail (CTEST_IS_COUNTERIMPL (self), 0);
    CtestCounterImplPrivate *priv = ctest_counterimpl_get_instance_private (CTEST_COUNTERIMPL (self));
    priv->value += value;
    return priv->value;
}

static gint ctest_counterimpl_mutable_counter_get(CtestMutableCounter* self)
{
    g_return_val_if_fail (CTEST_IS_COUNTERIMPL (self), 0);
    CtestCounterImplPrivate *priv = ctest_counterimpl_get_instance_private (CTEST_COUNTERIMPL (self));
    return priv->value;
}


static void ctest_counterimpl_mutable_counter_interface_init(CtestMutableCounterInterface *iface)
{
    iface->increment = ctest_counterimpl_mutable_counter_increment;
    iface->get = ctest_counterimpl_mutable_counter_get;
}

gint test_increment_in_c_impl(CtestMutableCounter *counter)
{
	ctest_mutable_counter_increment(counter, 19);
	ctest_mutable_counter_increment(counter, 4);
	return ctest_mutable_counter_get(counter);
}