/*
 * Copyright/Licensing information.
 */

/* inclusion guard */
#ifndef __CTEST_COUNTER_H__
#define __CTEST_COUNTER_H__

#include <glib-object.h>
/*
 * Potentially, include other headers on which this header depends.
 */

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define CTEST_COUNTER_TYPE ctest_counter_get_type ()
G_DECLARE_DERIVABLE_TYPE (CtestCounter, ctest_counter, CTEST, COUNTER, GObject)

struct _CtestCounterClass
{
  GObjectClass parent_class;

  /* Class virtual function fields. */
  void (* increment) (CtestCounter  *self,
                 gint value);
  gint (*get) (CtestCounter *self);

  /* Padding to allow adding up to 12 new virtual functions without
   * breaking ABI. */
  gpointer padding[10];
};

/*
 * Method definitions.
 */
CtestCounter *ctest_counter_new (void);
void ctest_counter_increment(CtestCounter* this, gint value);
gint ctest_counter_get(CtestCounter* this);

G_END_DECLS

#endif /* __COUNTER_H__ */
