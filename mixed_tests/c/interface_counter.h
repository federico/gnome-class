#ifndef INTERFACE_H
#define INTERFACE_H

#include <glib-object.h>

G_BEGIN_DECLS

#define CTEST_MUTABLE_COUNTER_TYPE ctest_mutable_counter_get_type()
G_DECLARE_INTERFACE (CtestMutableCounter, ctest_mutable_counter, CTEST, MUTABLE_COUNTER, GObject)

struct _CtestMutableCounterInterface
{
  GTypeInterface parent_iface;

  gint (*increment) (CtestMutableCounter* self, gint value);
  gint (*get) (CtestMutableCounter* self);
};

gint ctest_mutable_counter_increment(CtestMutableCounter* self, gint value);
gint ctest_mutable_counter_get(CtestMutableCounter* self);

G_END_DECLS


#endif //INTERFACE_H
