#include <stdio.h>
#include "counter.h"

#include <gobject/gobject.h>

static GParamSpec *obj_properties[2] = { NULL, };

typedef struct
{
	gint value;
} CtestCounterPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (CtestCounter, ctest_counter, G_TYPE_OBJECT)

CtestCounter* ctest_counter_new()
{
	return g_object_new(CTEST_COUNTER_TYPE, NULL);
}

void ctest_counter_increment (CtestCounter *self, gint value)
{
	CtestCounterClass *klass;

	g_return_if_fail (CTEST_IS_COUNTER (self));

	klass = CTEST_COUNTER_GET_CLASS (self);
	g_return_if_fail (klass->increment != NULL);

	klass->increment (self, value);
}

gint ctest_counter_get (CtestCounter *self)
{
	g_return_val_if_fail (CTEST_IS_COUNTER (self), 0);

	//CtestCounterClass* selfclass = CTEST_COUNTER_GET_CLASS (self);
	CtestCounterPrivate *priv = ctest_counter_get_instance_private (CTEST_COUNTER (self));

	return priv->value;
}

static void ctest_counter_set_property (GObject      *self,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
    CtestCounterPrivate *priv = ctest_counter_get_instance_private (CTEST_COUNTER (self));

    switch (property_id)
    {
        case 1:
        priv->value = g_value_get_int(value) - 10;
        break;

    default:
        /* We don't have any other property... */
        G_OBJECT_WARN_INVALID_PROPERTY_ID (self, property_id, pspec);
        break;
    }
}

static void ctest_counter_get_property (GObject    *self,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
    CtestCounterPrivate *priv = ctest_counter_get_instance_private (CTEST_COUNTER (self));

    switch (property_id)
    {
        case 1:
            g_value_set_int (value, priv->value + 10);
            break;

        default:
            /* We don't have any other property... */
            G_OBJECT_WARN_INVALID_PROPERTY_ID (self, property_id, pspec);
            break;
    }
}

static void ctest_counter_real_increment(CtestCounter *self, gint value)
{
	CtestCounterPrivate *priv = ctest_counter_get_instance_private (CTEST_COUNTER (self));
	priv->value += value;
}

static void ctest_counter_class_init(CtestCounterClass *selfclass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (selfclass);

	selfclass->increment = ctest_counter_real_increment;
	object_class->get_property = ctest_counter_get_property;
	object_class->set_property = ctest_counter_set_property;

	obj_properties[1] = g_param_spec_int ("get_prop",
                                 "Get with property Prop",
                                 "Property to get the value",
                                 -1000000,  /* minimum */
                                 1000000,  /* maximum */
                                 0.0,  /* default */
                                 G_PARAM_READWRITE);
    g_object_class_install_properties (object_class,
                                     2,
                                     obj_properties);
}

static void ctest_counter_init(CtestCounter* self)
{
	CtestCounterPrivate *priv = ctest_counter_get_instance_private (CTEST_COUNTER (self));
	priv->value = 0;
}

gint test_increment_in_c(CtestCounter *counter)
{
	ctest_counter_increment(counter, 19);
	ctest_counter_increment(counter, 4);
	return ctest_counter_get(counter);
}

int main_()
{
	CtestCounter* counter = g_object_new(CTEST_COUNTER_TYPE, NULL);
	//printf("Update from %d, ", ctest_counter_get(counter, &err));
	ctest_counter_increment(counter, 20);
	//printf("to %d\n", ctest_counter_get(counter, &err));
	return 0;
}

