#include "interface_counter.h"

G_DEFINE_INTERFACE (CtestMutableCounter, ctest_mutable_counter, 0)

static void
ctest_mutable_counter_default_init (CtestMutableCounterInterface * iface)
{
    iface->increment = ctest_mutable_counter_increment;
    iface->get = ctest_mutable_counter_get;
    /* add properties and signals to the interface here */
    g_object_interface_install_property (iface,
           g_param_spec_int ("iface_prop",
                             "Interface Prop",
                             "Property as defined by the interface",
                             -1000000,  /* minimum */
                             1000000,  /* maximum */
                             0.0,  /* default */
                             G_PARAM_READWRITE));
}

gint ctest_mutable_counter_increment(CtestMutableCounter *self, gint value)
{
    CtestMutableCounterInterface *iface;

    g_return_val_if_fail (CTEST_IS_MUTABLE_COUNTER (self), 0);

    iface = CTEST_MUTABLE_COUNTER_GET_IFACE (self);
    return iface->increment(self, value);
}

gint ctest_mutable_counter_get(CtestMutableCounter * self)
{
    CtestMutableCounterInterface *iface;

    g_return_val_if_fail (CTEST_IS_MUTABLE_COUNTER (self), -1);

    iface = CTEST_MUTABLE_COUNTER_GET_IFACE (self);
    return iface->get(self);
}
