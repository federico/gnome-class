# THIS PROJECT IS NO LONGER ACTIVE!

As of 2022, if you want to write GObject classes in Rust, please use
the [subclass mechanism in glib-rs][subclass] instead!

This project is no longer paying attention to issues or merge
requests; it is just here for posterity.

[subclass]: https://gtk-rs.org/gtk4-rs/stable/latest/book/gobject_subclassing.html

## A post-mortem of gnome-class, more or less

Gnome-class started during the first [Rust/GNOME hackfest][hackfest]
in Mexico City in 2017.  At that time, an early version of gtk-rs
already existed, but it was just a C->Rust binding.  It did not
contemplate implementing GObject subclasses in Rust.

At that time, a convenient way to write GObject-derived code in a
language that looked vaguely like C, but at a higher level, was by
using [Vala][Vala]:

```vala
class Demo.HelloWorld : GLib.Object {
    public static int main(string[] args) {
        stdout.printf("Hello, World\n");
        return 0;
    }
}

class Person : GLib.Object {
    private int _age = 32;

    /* Property */
    public int age {
        get { return _age; }
        set { _age = value; }
    }
}
```

This syntax is very comfortable/familar to people coming from a C# or
Java background.  So the reasoning was something like this:

* Rust can call C functions just fine.  One defines new GObjects by
  calling C functions.

* A mythical "compiler plugin" could take the Vala-like syntax above,
  and turn it into the boilerplate to register GObject classes, their
  properties, and signals.

* The actual code inside method implementations would be plain Rust,
  and the "compiler plugin" would leave them untouched.  Maybe it
  would just write appropriate function signatures, and have some
  magic trampolines to convert C and GObject types into Rust types.

This became `gnome-class`, a Rust procedural macro with more or less
the following syntax for the `gobject_gen!` macro:

```rust
gobject_gen! {
    class MyClass: GObject {
        foo: Cell<i32>,
        bar: RefCell<String>,
    }

    impl MyClass {
        virtual fn my_virtual_method(&self, x: i32) {
            ... do something with x ...
        }
    }
}
```

The idea was that `class MyClass` would be turned into a struct with
the appropriate fields for deriving from `GObject`, and with private
data to hold the `foo` and `bar` fields.  The proc macro would create
a `GObjectClass`-derived struct with a vtable slot for
`my_virtual_method`, and create an appropriate `#[no_mangle] pub fn
my_class_my_virtual_method(zelf: , x: i32)` callable from C code.

### Growing pains

In 2017 and 2018, Rust's procedural macros were still in heavy
development, as well as the surrounding infrastructure like the `syn`
and `quote` crates.  In particular, `syn` often made incompatible API
changes as it was developed.

I (Federico) was not working full-time on gnome-class, so those kinds
of changes gave me a lot of trouble — every time `syn` changed
its API, *all* of gnome-class would break and I would have no idea of
how to fix it.

Also, I'm not a compilers person.  So, I was trying to learn how to
write a compiler, how to use early iterations of proc-macros, and how
to do GObject in Rust all at the same time.  I was just overwhelmed.

### Incorrect assumptions in the architecture of gnome-class

Once we had code generation more or less working, it became desirable
to also output C header files for the generated API and GObject
Introspection `.gir` data.  You can see the latter in
[`src/gen/gir.rs`](src/gen/gir.rs).  However, doing this from the
procedural macro, i.e. *when the Rust code is compiled* is hugely
problematic.

It seems super-fishy to allow the Rust compiler to write to arbitrary
files just determined by the proc-macro.  Also, it is hard for the
macro to know the environment's conventions about where to place
generated files.

You know what does this sort of thing much better?
[Wasm-bindgen][wasm-bindgen].  It also wants to generate some magic
wrappers around Rust code, and then emit some metadata for them —
analogous to the GObject boilerplate that the proc-macro generates in
gnome-class, plus the GIR information.

Apart from the trampolines and other boilerplate, Wasm-bindgen
essentially emits its metadata by creating specially-named functions
like this:

```rust
#[no_mangle]
pub extern "C" fn _specially_named_function_that_outputs_metadata() -> Metadata {
    ...
}
```

Later, the `wasm-bindgen` tool opens the binaries, calls these
functions to extract the metadata, and generates the wrapper
Javascript code and anything else that may be needed.

That is, it puts the "generate non-Rust code" part in a totally
separate binary from the Rust compiler, and leaves it under the user's
control.  This is much friendlier to build systems, and is a better
architecture than burdening rustc with random tasks.

There are some [notes on how wasm-bindgen
works](gobject-notes/notes.txt), but they are probably very outdaded —
by this time you should better check wasm-bindgen's actual
documentation or code.

[hackfest]: https://wiki.gnome.org/Hackfests/Rust2017
[Vala]: https://wiki.gnome.org/Projects/Vala
[wasm-bindgen]: https://github.com/rustwasm/wasm-bindgen

### Problems that I don't quite remember if we solved

***Types:*** Not all of gobject's fundamental types have a 1:1 mapping to Rust
primitive types, but things like GObject signals really need accurate
function prototypes.  It also needs proper translation of incompatible
types that look like they should be compatible.  If you write this in
the `gobject_gen` syntax:

```
impl MyObject {
    signal fn a_signal_that_carries_a_string(&self, s: &str);
}
```

Should the signal trampoline create a `CString` and pass its pointer
to the other side?  Or should you be constrained to passing a special
`CString`-like object from the Rust code?

***C parent classes, Rust derived classes:*** If you have a C struct like this with bitfields:

```c
typedef struct {
    GObject parent;
    int my_field;
}
```

Then gtk-rs's `gir` needs to be able to reconstruct an equivalent
`#[repr(C)]` struct so that the Rust compiler will know the size of
the struct on the C side.  We had some problems with C structs with
bitfields, as `gir` couldn't handle them.  I don't know if these were
removed/fixed in GTK4 or if `gir` was made to support them.



# Postscript: the initial README.md

What follows is the old contents of `README.md`.  Maybe you can get
something useful out of it, but it's mostly outdated now.


# Gnome-class: implement GObjects in Rust with no boilerplate

[GObject][gobject] is the C-based object system for [GTK+][gtk] and
[GNOME][gnome] programs.  While C does not have objects or classes by
itself, GObject makes it possible to write object-oriented C programs.

GObject in C normally requires that you write an uncomfortable amount
of [boilerplate code][boilerplate] to do things like register a new
class, define its methods, register object signals and properties,
etc.  Due to the nature of C, many operations are not type-safe and
depend on correct pointer casts, or on knowing the types that you
should really be passing to varargs functions, which are not checked
by the compiler.

The goal of this Gnome-class crate is to let you write GObject
implementations in Rust with minimal or no boilerplate, and with
compile-time type safety all along.

The library API reference can be found **[here][class-docs]**.

## Requirements

Gnome-class is a Rust procedural macro.  We make use of some features
in the `proc-macro2` crate which, as of March 2018, require the **Rust
nightly** toolchain.  This is probably easiest to set up with
[`rustup`], which will install the Rust/Cargo toolchain in your home
directory, without overwriting your system's installation.

Once you are in the `rustup` environment, you can select the nightly
toolchain as the default like this:

```sh
rustup default nightly
```

Or if you don't want to make that the default, but still work on
gnome-class, you can be careful to type

```sh
cargo +nightly test
```

instead of the usual "`cargo test`" you would use for development.

Gnome-class also depends on the `rustfmt` utility which can be easily
installed with [`rustup`].

```sh
rustup component add rustfmt-preview
```


[`rustup`]: https://www.rustup.rs/

## How gnome-class works

Gnome-class is a procedural macro for Rust.  Within the macro, we
define a mini-language which looks as Rust-y as possible, and that has
extensions to let you define GObject subclasses, their properties,
signals, interface implementations, and the rest of GObject's
features.  The goal is to require no `unsafe` code on your part.

The procedural macro generates a bunch of code which makes `unsafe`
calls to GObject's C API.  For example, when you write this:

```rust
gobject_gen! {
    class MyClass: GObject {
        foo: Cell<i32>,
        bar: RefCell<String>,
    }

    impl MyClass {
        virtual fn my_virtual_method(&self, x: i32) {
            ... do something with x ...
        }
    }
}
```

Then the `gobject_gen!` procedural macro will generate a bunch of code
which both defines a GObject implementation that is callable from
other languages using the [GObject Introspection][gi] machinery, and
which also exports a Rust API using the same conventions
as [glib-rs][glib-rs].

# Goals

* Let users write new GObject classes completely in Rust, with no
  unsafe code, and no boilerplate.

* Generate GObject implementations that look exactly like C GObjects
  from the outside.  The generated GObjects should be callable from C
  or other languages in exactly the same way as traditional GTK+/GNOME
  libraries.

* Automatically emit GObject Introspection information so that the
  generated objects can be consumed by language bindings.

* In the end, we aim to make it compelling for users to *not* write
  new GObject libraries in C, but rather to give them an "obvious" way
  to it in Rust.  This should ensure higher-quality, safer code for
  GNOME's general-purpose libraries, while maintaining backwards
  compatibility with all the GObject-based infrastructure we have.

# Contributing to gnome-class

There is a code of conduct for contributors to gnome-class; please see
the file [`code-of-conduct.md`][coc].

As of November 2018, gnome-class is under heavy development, and is not
ready yet for general consumption.  We are not finished implementing
the desired syntax (see the [syntax document][syntax] for details).
Also, we don't have proposed syntax yet for all the useful features of
GObject.

Please read the file [`CONTRIBUTING.md`][contributing] for information
on how to report bugs, how to request features, and for how to work on
gnome-class itself.

## Missing features

We track missing features in the [issue
tracker](https://gitlab.gnome.org/federico/gnome-class/issues).
Please take a look there to see what's missing.


# Maintainers
-----------

The maintainer of gnome-class is [Federico Mena Quintero][federico].  Feel
free to contact me for any questions you may have about gnome-class, both
its usage and its development.  You can contact me in the following
ways:

* [Mail me][mail] at federico@gnome.org.

* IRC: I am `federico` on `irc.gnome.org` in the `#rust` or
  `#gnome-hackers` channels.  I'm there most weekdays (Mon-Fri)
  starting at about UTC 14:00 (that's 08:00 my time; I am in the UTC-6
  timezone).  If this is not a convenient time for you, feel free to
  [mail me][mail] and we can arrange a time.

[gobject]: https://developer.gnome.org/platform-overview/unstable/tech-gobject.html.en
[boilerplate]: https://developer.gnome.org/SubclassGObject/
[gtk]: https://www.gtk.org/
[gnome]: https://www.gnome.org/
[gi]: https://wiki.gnome.org/Projects/GObjectIntrospection
[glib-rs]: http://gtk-rs.org/docs/glib/
[syntax]: gobject-notes/syntax.md
[syn]: https://github.com/dtolnay/syn/
[federico]: https://people.gnome.org/~federico/
[mail]: mailto:federico@gnome.org
[coc]: code-of-conduct.md
[contributing]: CONTRIBUTING.md
[class-docs]: https://federico.pages.gitlab.gnome.org/gnome-class/doc/gobject_gen/
