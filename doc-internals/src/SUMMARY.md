# Summary

- [Introduction](./introduction.md)
- [Overview](./overview.md)
- [Parsing into the Abstract Syntax Tree](./parsing.md)
- [High-level Internal Representation](./hir.md)
- [Type conversions between Rust and Glib](./types.md)
- [Error and Diagnostic Handling](./errors.md)
- [Testing strategy](./testing.md)
- [Glossary](./glossary.md)
