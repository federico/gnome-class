# 2. Overview

Gnome-class extends the Rust language to support a very particular
kind of classes with inheritance, for the GObject type system.
Gnome-class is implemented as a procedural macro in Rust; this means
that it runs as part of the Rust compiler itself, and parses the
user's Gnome-like code and generates Rust code for it.

## Stages

Gnome-class operates in various stages, similar to a compiler:

1. **Parsing into an Abstract Syntax Tree.** We parse the code that
   the user put inside the `gobject_gen!` invocation using a
   [syn][syn]-based parser.  The parser generates an **Abstract Syntax
   Tree (AST)**, which closely matches the structure of the user's
   code.  At the end of this process, the code will be fully parsed
   into an AST (or it will have failed with a syntax error), but the
   AST may not be semantically valid.  The AST is defined in
   `src/ast.rs`.
   
2. We check the AST for semantic errors.  For example, there cannot be
   two classes defined with the same name.
   
3. We create a **High-level Internal Representation (HIR)** from the
   AST.  The HIR matches GObject concepts more closely.  For example,
   while the AST may contain separate items for `class Foo` and `impl
   Foo`, the HIR has a single `Class` struct who knows which methods
   are defined for it, which virtual methods have default
   implementations, etc. This is also
   where we ensure that the user's code is semantically valid.  For
   example, we check that the same signal name is not being declared
   twice for a class.  The HIR is defined in [`src/hir`](src/hir).

4. **Code generation.** We generate code based on the HIR.  For each
   class defined in the HIR, we emit the necessary GObject boilerplate
   to register that class, its methods, signals, properties, etc.  We
   emit the actual code for methods and signal handlers, and the
   necessary trampolines to call Rust methods and signal handlers from
   C.  The code generator is defined in [`src/gen`](src/gen).  In
   there, the one-time, per-class GObject boilerplate is in
   [`src/gen/boilerplate.rs`](src/gen/boilerplate.rs).  The other
   files in the `src/gen` directory are used for things that require extra
   code generation like signals and traits for method trampolines.
   
## Code structure

The entry point for gnome-class is the `gobject_gen!` procedural
macro.  It is defined in `src/lib.rs`.

The AST structures are defined in `src/ast.rs`.

The parser is in `src/parser/mod.rs`.

Some of the AST validation code is in `src/checking.rs`.  Other checks
happen in the HIR.

The HIR is in `src/hir/mod.rs`.

Finally, code generation is in `src/gen/*.rs`.

[TokenStream]: https://doc.rust-lang.org/proc_macro/struct.TokenStream.html
[syn]: https://github.com/dtolnay/syn/
