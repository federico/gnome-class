// use lalrpop_intern::InternedString;
use crate::parser::keywords;
use proc_macro2::Ident;
use quote::ToTokens;
use syn::punctuated::Punctuated;
use syn::token;
use syn::{Attribute, Lit, Token};
use syn::{Block, FieldsNamed, FnArg, ImplItemType, Path, ReturnType, Type};

pub struct Program {
    pub items: Vec<Item>,
}

impl Program {
    pub fn classes<'a>(&'a self) -> impl Iterator<Item = &'a Class> + 'a {
        self.items.iter().filter_map(|item| match *item {
            Item::Class(ref c) => Some(c),
            _ => None,
        })
    }

    pub fn impls<'a>(&'a self) -> impl Iterator<Item = &'a Impl> + 'a {
        self.items.iter().filter_map(|item| match *item {
            Item::Impl(ref i) => Some(i),
            _ => None,
        })
    }

    pub fn interfaces<'a>(&'a self) -> impl Iterator<Item = &'a Interface> + 'a {
        self.items.iter().filter_map(|item| match *item {
            Item::Interface(ref i) => Some(i),
            _ => None,
        })
    }
}

pub enum Item {
    Class(Class),
    Impl(Impl),
    Interface(Interface),
}

pub fn get_program_classes<'a>(program: &'a Program) -> Vec<&'a Class> {
    program
        .items
        .iter()
        .filter_map(|item| {
            if let Item::Class(ref c) = *item {
                Some(c)
            } else {
                None
            }
        })
        .collect()
}

pub struct Class {
    pub attrs: Vec<Attribute>,
    pub class: keywords::class,
    pub name: Ident,
    pub colon: Option<Token![:]>,
    pub extends: Option<Path>,
    pub fields: FieldsNamed,
}

// similar to syn::ItemImpl
pub struct Impl {
    pub trait_: Option<Path>,
    pub self_path: Ident,
    pub items: Vec<ImplItem>,
}

pub struct Interface {
    pub attrs: Vec<Attribute>,
    pub interface: keywords::interface,
    pub name: Ident,
    // FIXME: required class and interfaces
    pub brace: token::Brace,
    pub items: Vec<ImplItem>,
}

pub struct ImplItem {
    pub attrs: Vec<Attribute>,
    pub node: ImplItemKind,
}

pub enum ImplItemKind {
    Method(ImplItemMethod),
    Prop(ImplProp),
    ReserveSlots(token::Paren, Lit),
    TraitType(ImplItemType),
}

impl ImplItemKind {
    pub fn tokens(&self) -> &dyn ToTokens {
        match self {
            ImplItemKind::Method(m) => &m.name,
            ImplItemKind::Prop(ImplProp { name, .. }) => name,
            ImplItemKind::ReserveSlots(_, lit) => lit,
            ImplItemKind::TraitType(l) => l,
        }
    }
}

pub struct ImplItemMethod {
    pub public: Option<Token![pub]>,       // requires body
    pub virtual_: Option<Token![virtual]>, // implies public, doesn't need body
    pub signal: Option<keywords::signal>,  // ignore
    pub fn_: Token![fn],
    pub name: Ident,
    pub parem_token: token::Paren,
    pub inputs: Punctuated<FnArg, Token!(,)>, // must start with &self
    pub output: ReturnType,
    pub body: ImplItemMethodBlock,
}

pub enum ImplItemMethodBlock {
    Block(Block),
    Empty(Token![;]),
}

impl ImplItemMethodBlock {
    pub fn as_ref(&self) -> Option<&Block> {
        match self {
            ImplItemMethodBlock::Block(ref b) => Some(b),
            ImplItemMethodBlock::Empty(_) => None,
        }
    }
}

pub struct ImplProp {
    pub attributes: Vec<Attribute>,
    pub property: keywords::property,
    pub name: Ident,
    pub colon1: Token![:],
    pub t1: keywords::T,
    pub where_: Token![where],
    pub t2: keywords::T,
    pub colon2: Token![:],
    pub type_: Type,
    pub braces: token::Brace,
    pub items: Vec<ImplPropBlock>,
}

impl ImplProp {
    pub fn getter(&self) -> Option<&ImplPropBlock> {
        self.items.iter().find(|item| match *item {
            ImplPropBlock::Getter(_) => true,
            _ => false,
        })
    }

    pub fn setter(&self) -> Option<&ImplPropBlock> {
        self.items.iter().find(|item| match *item {
            ImplPropBlock::Setter(_) => true,
            _ => false,
        })
    }
}

pub enum ImplPropBlock {
    Getter(ImplPropGetter),
    Setter(ImplPropSetter),
    Default(ImplPropDefault),
}

// get(&self) -> T {}
pub struct ImplPropGetter {
    pub get: keywords::get,
    pub paren: token::Paren,
    pub and: Token![&],
    pub self_: Token![self],
    pub rarrow: Token![->],
    pub t: keywords::T,
    pub block: Block,
}

// set(&mut self, ident: T)
pub struct ImplPropSetter {
    pub set: keywords::set,
    pub paren: token::Paren,
    pub amp: Token![&],
    pub self_: Token![self],
    pub comma: Token![,],
    pub param: Ident,
    pub colon: Token![:],
    pub t: keywords::T,
    pub block: Block,
}

// default() -> T { }
pub struct ImplPropDefault {
    pub default: Token![default],
    pub paren: token::Paren,
    pub rarrow: Token![->],
    pub self_: keywords::T,
    pub block: Block,
}

// Mostly copied from syn's ImplItemType
pub struct InstancePrivateItem {
    pub type_token: Token!(type),
    pub eq_token: Token!(=),
    pub path: Path,
    pub semi_token: Token!(;),
}
