#![allow(non_snake_case)]

use crate::ast;
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};

pub fn tokens_GObject() -> TokenStream {
    quote! { glib::Object }
}

pub fn tokens_GObjectFfi() -> TokenStream {
    quote! { gobject_ffi::GObject }
}

pub fn tokens_GObjectClassFfi() -> TokenStream {
    quote! { gobject_ffi::GObjectClass }
}

pub fn tokens_glib_type(gtype: &::glib::Type) -> TokenStream {
    match gtype {
        ::glib::Type::Invalid => quote! { ::gobject_class::glib::Type::Invalid },
        ::glib::Type::Unit => quote! { ::gobject_class::glib::Type::Unit },
        ::glib::Type::BaseInterface => quote! { ::gobject_class::glib::Type::BaseInterface },
        ::glib::Type::I8 => quote! { ::gobject_class::glib::Type::I8 },
        ::glib::Type::U8 => quote! { ::gobject_class::glib::Type::U8 },
        ::glib::Type::Bool => quote! { ::gobject_class::glib::Type::Bool},
        ::glib::Type::I32 => quote! { ::gobject_class::glib::Type::I32 },
        ::glib::Type::U32 => quote! { ::gobject_class::glib::Type::U32 },
        ::glib::Type::ILong => quote! { ::gobject_class::glib::Type::ILong },
        ::glib::Type::ULong => quote! { ::gobject_class::glib::Type::ULong },
        ::glib::Type::I64 => quote! { ::gobject_class::glib::Type::I64 },
        ::glib::Type::U64 => quote! { ::gobject_class::glib::Type::U64 },
        ::glib::Type::BaseEnum => quote! { ::gobject_class::glib::Type::BaseEnum },
        ::glib::Type::BaseFlags => quote! { ::gobject_class::glib::Type::BaseFlags },
        ::glib::Type::F32 => quote! { ::gobject_class::glib::Type::F32 },
        ::glib::Type::F64 => quote! { ::gobject_class::glib::Type::F64 },
        ::glib::Type::String => quote! { ::gobject_class::glib::Type::String },
        ::glib::Type::Pointer => quote! { ::gobject_class::glib::Type::Pointer },
        ::glib::Type::BaseBoxed => quote! { ::gobject_class::glib::Type::BaseBoxed },
        ::glib::Type::BaseParamSpec => quote! { ::gobject_class::glib::Type::BaseParamSpec },
        ::glib::Type::BaseObject => quote! { ::gobject_class::glib::Type::BaseObject },
        ::glib::Type::Variant => quote! { ::gobject_class::glib::Type::Variant },
        ::glib::Type::Other(x) => quote! { ::gobject_class::glib::Type::Other(#x) },
    }
}

pub fn tokens_glib_ffi_type(gtype: &::glib::Type) -> TokenStream {
    match gtype {
        ::glib::Type::Invalid => quote! { ::gobject_class::gobject_sys::G_TYPE_INVALID },
        ::glib::Type::Unit => quote! { ::gobject_class::gobject_sys::G_TYPE_NONE },
        ::glib::Type::BaseInterface => quote! { ::gobject_class::gobject_sys::G_TYPE_INTERFACE },
        ::glib::Type::I8 => quote! { ::gobject_class::gobject_sys::G_TYPE_CHAR },
        ::glib::Type::U8 => quote! { ::gobject_class::gobject_sys::G_TYPE_UCHAR },
        ::glib::Type::Bool => quote! { ::gobject_class::gobject_sys::G_TYPE_BOOLEAN },
        ::glib::Type::I32 => quote! { ::gobject_class::gobject_sys::G_TYPE_INT },
        ::glib::Type::U32 => quote! { ::gobject_class::gobject_sys::G_TYPE_UINT },
        ::glib::Type::ILong => quote! { ::gobject_class::gobject_sys::G_TYPE_LONG },
        ::glib::Type::ULong => quote! { ::gobject_class::gobject_sys::G_TYPE_ULONG },
        ::glib::Type::I64 => quote! { ::gobject_class::gobject_sys::G_TYPE_INT64 },
        ::glib::Type::U64 => quote! { ::gobject_class::gobject_sys::G_TYPE_UINT64 },
        ::glib::Type::BaseEnum => quote! { ::gobject_class::gobject_sys::G_TYPE_ENUM },
        ::glib::Type::BaseFlags => quote! { ::gobject_class::gobject_sys::G_TYPE_FLAGS },
        ::glib::Type::F32 => quote! { ::gobject_class::gobject_sys::G_TYPE_FLOAT },
        ::glib::Type::F64 => quote! { ::gobject_class::gobject_sys::G_TYPE_DOUBLE },
        ::glib::Type::String => quote! { ::gobject_class::gobject_sys::G_TYPE_STRING },
        ::glib::Type::Pointer => quote! { ::gobject_class::gobject_sys::G_TYPE_POINTER },
        ::glib::Type::BaseBoxed => quote! { ::gobject_class::gobject_sys::G_TYPE_BOXED },
        ::glib::Type::BaseParamSpec => quote! { ::gobject_class::gobject_sys::G_TYPE_PARAM },
        ::glib::Type::BaseObject => quote! { ::gobject_class::gobject_sys::G_TYPE_OBJECT },
        ::glib::Type::Variant => quote! { ::gobject_class::gobject_sys::G_TYPE_VARIANT },
        ::glib::Type::Other(_) => panic!("Cannot know type of non-fundamental gtype"),
    }
}

pub fn tokens_ParentInstance(class: &ast::Class) -> TokenStream {
    class
        .extends
        .as_ref()
        .map(|path| {
            let mut tokens = TokenStream::new();
            path.to_tokens(&mut tokens);
            tokens
        })
        .unwrap_or_else(tokens_GObject)
}

pub fn tokens_ParentInstanceFfi(class: &ast::Class) -> TokenStream {
    let ParentInstance = tokens_ParentInstance(class);
    quote! {
        <#ParentInstance as ::gobject_class::glib::wrapper::Wrapper>::GlibType
    }
}

pub fn tokens_ParentClassFfi(class: &ast::Class) -> TokenStream {
    let ParentInstance = tokens_ParentInstance(class);
    quote! {
        <#ParentInstance as ::gobject_class::glib::wrapper::Wrapper>::GlibClassType
    }
}

pub fn glib_callback_guard() -> TokenStream {
    // FIXME: remove this function if we formally declare that
    // gnome-class will require Rust 1.24 or later?  That version made
    // glib::CallbackGuard obsolete.
    quote! {
        #[allow(deprecated)]
        let _guard = ::gobject_class::glib::CallbackGuard::new();
    }
}

pub fn lower_case_instance_name(instance_name: &str) -> String {
    let mut char_iterator = instance_name.char_indices();
    let mut start_index = match char_iterator.next() {
        Some((index, _)) => index,
        None => return String::new(),
    };

    let mut parts = Vec::new();
    'outer: loop {
        // If the next bit starts with a sequence of uppercase characters, we include them
        // all. This is done in order to have a better default behavior with class names that
        // contains acronyms. See the `lower_cases_with_sequential_uppercase_characters` test
        // below.
        let mut found_non_uppercase_character = false;

        for (end_index, character) in &mut char_iterator {
            let character_is_uppercase = character.is_uppercase();
            if found_non_uppercase_character && character_is_uppercase {
                parts.push(instance_name[start_index..end_index].to_lowercase());
                start_index = end_index;
                continue 'outer;
            } else {
                found_non_uppercase_character |= !character_is_uppercase;
            }
        }

        parts.push(instance_name[start_index..].to_lowercase());
        break 'outer;
    }

    parts.join("_")
}

pub mod tests {
    use super::*;

    pub fn run() {
        lower_cases_simple_names();
        lower_cases_non_ascii_names();
        lower_cases_with_sequential_uppercase_characters();
    }

    fn lower_cases_simple_names() {
        assert_eq!("foo", lower_case_instance_name("Foo"));
        assert_eq!(
            "snake_case_sliding_through_the_grass",
            lower_case_instance_name("SnakeCaseSlidingThroughTheGrass")
        );
        assert_eq!("", lower_case_instance_name(""));
        assert_eq!(
            "ifyoureallywantto",
            lower_case_instance_name("ifyoureallywantto")
        );
        assert_eq!(
            "if_you_really_want_to",
            lower_case_instance_name("if_you_really_want_to")
        );
    }

    fn lower_cases_non_ascii_names() {
        assert_eq!("y̆es", lower_case_instance_name("Y̆es"));
        assert_eq!(
            "trying_this_y̆es_y̆es",
            lower_case_instance_name("TryingThisY̆esY̆es")
        );
        assert_eq!(
            "y̆es_y̆es_trying_this",
            lower_case_instance_name("Y̆esY̆esTryingThis")
        );
    }

    fn lower_cases_with_sequential_uppercase_characters() {
        assert_eq!("gtk_rbtree", lower_case_instance_name("GtkRBTree"));
        assert_eq!(
            "rbtree_internals",
            lower_case_instance_name("RBTreeInternals")
        );

        // This may or may not be what we want, but for now this is the behavior that we expect.
        assert_eq!("gtkrbtree", lower_case_instance_name("GTKRBTree"));

        assert_eq!(
            "thisisaterribleclassname",
            lower_case_instance_name("THISISATERRIBLECLASSNAME")
        );
    }
}
