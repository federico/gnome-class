use super::boilerplate;
use super::instance_ext;
use super::properties;
use super::SlotExt;
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use std::marker::PhantomData;

/// This class can generate the public part of the boilerplate code for objects
pub struct Public<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> {
    boilerplate: &'lt Boilerplate,
    phantom: PhantomData<&'ast ()>,
}

/// This class can generate the public part of the boilerplate code for objects
/// The code is expected to be inserted into the "imp"-mod
pub struct PublicImp<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> {
    boilerplate: &'lt Boilerplate,
    phantom: PhantomData<&'ast ()>,
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> ToTokens
    for Public<'lt, 'ast, Boilerplate>
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        tokens.extend(self.gen_public_new());
        // tokens.extend(self.gen_def_trait_ext());
        // tokens.extend(self.gen_impl_trait_ext());
    }
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> ToTokens
    for PublicImp<'lt, 'ast, Boilerplate>
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        tokens.extend(self.gen_external_methods());
        tokens.extend(self.gen_trait_ext());
    }
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> Public<'lt, 'ast, Boilerplate> {
    pub fn new(boilerplate: &'lt Boilerplate) -> Self {
        let phantom = PhantomData;
        Public {
            boilerplate,
            phantom,
        }
    }

    fn gen_public_new(&self) -> TokenStream {
        if self.boilerplate.fundamental_type() == glib::Type::BaseInterface {
            // interfaces cannot be instantiated
            return TokenStream::new();
        }

        let instance = self.boilerplate.names().instance();
        let imp_new_fn_name = self.boilerplate.names().exported_fn_from_str("new");

        quote! {
            impl #instance {
                // FIXME: we should take construct-only arguments and other convenient args to new()
                pub fn new() -> #instance {
                    unsafe { from_glib_full(imp::#imp_new_fn_name(/* FIXME: args */)) }
                }
            }
        }
    }

    fn gen_def_trait_ext(&self) -> TokenStream {
        let instance_ext = self.boilerplate.names().instance_ext();
        let slot_trait_fns = instance_ext::slot_trait_fns(&self.boilerplate.slots());

        let set_properties_fn = properties::set_properties_fn(self.boilerplate.properties().iter());
        let get_properties_fn = properties::get_properties_fn(self.boilerplate.properties().iter());

        quote! {
            pub trait #instance_ext {
                #(#slot_trait_fns)*

                #(#get_properties_fn)*

                #(#set_properties_fn)*
            }
        }
    }

    fn gen_impl_trait_ext(&self) -> TokenStream {
        let instance = self.boilerplate.names().instance();
        let instance_ext = self.boilerplate.names().instance_ext();
        let slot_trait_impls =
            instance_ext::slot_trait_impls(&self.boilerplate.names(), &self.boilerplate.slots());
        let set_properties = properties::set_properties(self.boilerplate.properties().iter());
        let get_properties = properties::get_properties(self.boilerplate.properties().iter());

        quote! {
            impl<O: IsA<#instance> + IsA<glib::object::Object> + glib::object::ObjectExt> #instance_ext for O {
                #(#slot_trait_impls)*

                #(#get_properties)*

                #(#set_properties)*
            }
        }
    }
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>>
    PublicImp<'lt, 'ast, Boilerplate>
{
    pub fn new(boilerplate: &'lt Boilerplate) -> Self {
        let phantom = PhantomData;
        PublicImp {
            boilerplate,
            phantom,
        }
    }

    fn gen_external_methods(&self) -> TokenStream {
        let imp_extern_methods =
            crate::gen::imp::extern_methods(&self.boilerplate.names(), &self.boilerplate.slots());
        quote! { #(#imp_extern_methods)* }
    }
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>>
    PublicImp<'lt, 'ast, Boilerplate>
{
    fn gen_trait_ext(&self) -> TokenStream {
        let name = self.boilerplate.names().instance();
        let ancestors = self
            .boilerplate
            .ancestors()
            .iter()
            .filter(|anc| !anc.is_fundamental())
            .map(|anc| anc.instance());
        let instance_ext = self.boilerplate.names().instance_ext();
        let slot_def = self
            .boilerplate
            .slots()
            .iter()
            .filter(|slot| slot.public())
            .map(|slot| slot.trait_definition());
        let slot_impl = self
            .boilerplate
            .slots()
            .iter()
            .filter(|slot| slot.public())
            .map(|slot| slot.trait_implementation(self.boilerplate.names()));

        let set_properties_fn = properties::set_properties_fn(self.boilerplate.properties().iter());
        let get_properties_fn = properties::get_properties_fn(self.boilerplate.properties().iter());
        let set_properties = properties::set_properties(self.boilerplate.properties().iter());
        let get_properties = properties::get_properties(self.boilerplate.properties().iter());

        quote! {
            pub trait #instance_ext {
                #(#slot_def)*

                #(#get_properties_fn)*
                #(#set_properties_fn)*
            }

            impl<T: IsA<#name> #(+ IsA<#ancestors>)* + IsA<::gobject_class::glib::Object> + ::gobject_class::glib::value::SetValue> #instance_ext for T
            {
                #(#slot_impl)*

                #(#get_properties)*
                #(#set_properties)*
            }
        }
    }
}
