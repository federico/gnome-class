use crate::errors::{Errors, Result};
use crate::hir::generatorattrs::{Generator, GeneratorAttributes};
use difference::{Changeset, Difference};
use proc_macro2::{Span, TokenStream};
use quote::ToTokens;
use std::fs::{self, File};
use std::io::{Read, Result as IoResult, Write};
use std::iter::FromIterator;
use std::process::{Command, Stdio};
use std::rc::Rc;
use syn::LitStr;

/// This struct contains a receiver for information about a file which can be generated.
/// The impl can write these content to disk or compare it to an existing file
pub struct GeneratorWrite {
    generator: Option<File>,
    comparer: Option<(Vec<u8>, LitStr)>,
    lit: LitStr,
}

impl GeneratorWrite {
    /// Initialize from a Generator. The generator is normally constructed as part of an attribute
    pub fn from(gen: &Generator) -> Result<Self> {
        let lit = gen
            .generate
            .as_ref()
            .or(gen.compare.as_ref())
            .cloned()
            .unwrap_or(LitStr::new("foo", Span::call_site()));
        let generator = match &gen.generate {
            Some(path) => match File::create(path.value()) {
                Ok(f) => Some(f),
                Err(e) => bail!(lit, "{}", e),
            },
            None => None,
        };
        let comparer = gen.compare.as_ref().map(|path| (Vec::new(), path.clone()));

        Ok(GeneratorWrite {
            generator,
            comparer,
            lit,
        })
    }

    /// Interpretet the difference and write tje diff-file to String.
    fn collect_difference(diffs: Vec<Difference>) -> String {
        const SURROUNDING_LINES: usize = 5;

        let mut res = Vec::new();
        res.push("Files do not match");

        for current in 0..diffs.len() {
            match diffs[current] {
                Difference::Same(ref x) => {
                    let lines: Vec<&str> = x.lines().collect();
                    if lines.len() < SURROUNDING_LINES {
                        for line in lines {
                            res.push("\n ");
                            res.push(line);
                        }
                    } else if lines.len() < 2 * SURROUNDING_LINES
                        && (current == 0 || current == diffs.len() - 1)
                    {
                        // Not the first or the last, so 2*SURROUNDING_LINES are possible
                        for line in lines {
                            res.push("\n ");
                            res.push(line);
                        }
                    } else {
                        if current > 0 {
                            for line in lines.iter().take(5) {
                                res.push("\n ");
                                res.push(line);
                            }
                            if current + 1 < diffs.len() {
                                res.push("\n...");
                            }
                        }
                        if current + 1 < diffs.len() {
                            for line in lines.iter().skip(lines.len() - 5) {
                                res.push("\n ");
                                res.push(line);
                            }
                        }
                    }
                    if x.len() == 0 || x.ends_with("\n") {
                        res.push("\n "); // Adds an additional same line if last line is empty
                    }
                }
                Difference::Add(ref x) => {
                    for line in x.lines().into_iter() {
                        res.push("\n+");
                        res.push(line);
                    }
                    if x.len() == 0 || x.ends_with("\n") {
                        res.push("\n+");
                    }
                }
                Difference::Rem(ref x) => {
                    for line in x.lines().into_iter() {
                        res.push("\n-");
                        res.push(line);
                    }
                    if x.len() == 0 || x.ends_with("\n") {
                        res.push("\n-");
                    }
                }
            }
        }

        res.concat()
    }

    /// Until here, the content is prepared to be compared.
    /// This code actually compares the code.
    pub fn collect_output(self) -> Result<()> {
        let lit = self.lit;
        let (data, path) = match self.comparer {
            Some(p) => p,
            None => return Ok(()),
        };
        let file_str = fs::read_to_string(path.value()).map_err(|e| format_err!(lit, "{}", e))?;
        if file_str.as_bytes() == &data[..] {
            return Ok(());
        }
        let data_str = String::from_utf8(data).map_err(|e| format_err!(lit, "{}", e))?;
        let Changeset { diffs, .. } = Changeset::new(&file_str, &data_str, "\n");
        let diff = GeneratorWrite::collect_difference(diffs);
        bail!(lit, "{}", diff)
    }
}

/// Append data to the file or to the content to be compared
impl Write for GeneratorWrite {
    fn write(&mut self, mut buf: &[u8]) -> IoResult<usize> {
        if let Some(generator) = &mut self.generator {
            let amt = generator.write(buf)?;
            buf = &buf[..amt];
        }
        if let Some((data, _)) = &mut self.comparer {
            data.extend(buf);
        }
        Ok(buf.len())
    }

    fn flush(&mut self) -> IoResult<()> {
        if let Some(generator) = &mut self.generator {
            generator.flush()?;
        }
        Ok(())
    }
}

/// Struct used to write tokens to files. It is also used to generate
/// the final TokenStream
pub struct RustWriter {
    output: Vec<Rc<TokenStream>>,
    errors: Vec<Errors>,
    generate_files: Vec<RustWriterEntry>,
    compare_files: Vec<RustWriterEntry>,
}

struct RustWriterEntry {
    content: Vec<Rc<TokenStream>>,
    span: LitStr,
}

/// A class or interface can contain different generating-information.
/// This class applies them
pub struct RustWriterFile<'parent> {
    parent_output: Option<&'parent mut Vec<Rc<TokenStream>>>,
    write_entry: Option<&'parent mut RustWriterEntry>,
    compare_entry: Option<&'parent mut RustWriterEntry>,
}

impl RustWriter {
    /// Create a new RustWriter
    pub fn new() -> Self {
        RustWriter {
            output: Vec::new(),
            errors: Vec::new(),
            compare_files: Vec::new(),
            generate_files: Vec::new(),
        }
    }

    /// This function creates a writer for a class. This writer can write to both a file and to
    /// output. It also supports comparing the output with another file.
    pub fn class_writer<'this, 'parent: 'this>(
        &'parent mut self,
        class_attrs: &'this GeneratorAttributes,
    ) -> RustWriterFile<'parent> {
        // Find the index in self.compare_files already containing this file (and add it sorted if
        // it doesn't exists) Errors if the file is part of the generated-vector, because
        // we cannot generate and compare the same file.
        let cmp_index: Option<usize> = class_attrs.rust.compare.as_ref().and_then(|path| {
            let pathval = path.value();
            let index = self
                .compare_files
                .binary_search_by(|other| other.span.value().cmp(&pathval));
            match index {
                Ok(index) => Some(index),
                Err(index) => {
                    if self
                        .generate_files
                        .binary_search_by(|other| other.span.value().cmp(&pathval))
                        .is_err()
                    {
                        self.compare_files.insert(
                            index,
                            RustWriterEntry {
                                content: Vec::new(),
                                span: path.clone(),
                            },
                        );
                        Some(index)
                    } else {
                        self.errors.push(
                            format_err!(path, "File cannot be used for both compare and generate")
                                .into(),
                        );
                        None
                    }
                }
            }
        });

        // Find the index in self.generate_files already containing this file (and add it sorted if
        // it doesn't exists) Errors if the file is part of the compare-vector, because we
        // cannot generate and compare the same file.
        let gen_index: Option<usize> = class_attrs.rust.generate.as_ref().and_then(|path| {
            let pathval = path.value();
            let index = self
                .generate_files
                .binary_search_by(|other| other.span.value().cmp(&pathval));
            match index {
                Ok(index) => Some(index),
                Err(index) => {
                    if self
                        .compare_files
                        .binary_search_by(|other| other.span.value().cmp(&pathval))
                        .is_err()
                    {
                        self.generate_files.insert(
                            index,
                            RustWriterEntry {
                                content: Vec::new(),
                                span: path.clone(),
                            },
                        );
                        Some(index)
                    } else {
                        self.errors.push(
                            format_err!(path, "File cannot be used for both compare and generate")
                                .into(),
                        );
                        None
                    }
                }
            }
        });

        let compare_files = &mut self.compare_files;
        let generate_files = &mut self.generate_files;
        RustWriterFile {
            compare_entry: cmp_index.map(move |index| &mut compare_files[index]),
            write_entry: gen_index.map(move |index| &mut generate_files[index]),
            parent_output: if class_attrs.rust.generate_only {
                None
            } else {
                Some(&mut self.output)
            },
        }
    }

    /// Apply rustfmt on a TokenStream
    fn format_stream(err_span: Span, tokens: Vec<Rc<TokenStream>>) -> Result<Vec<u8>> {
        let stream: String = tokens.iter().map(|token| token.to_string()).collect();
        let mut child = Command::new("rustfmt")
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::inherit())
            .spawn()
            .map_err(|e| format_err_span!(err_span, "{}", e))?;
        child
            .stdin
            .take()
            .unwrap()
            .write_all(stream.as_bytes())
            .map_err(|e| format_err_span!(err_span, "{}", e))?;
        let mut out = Vec::new();
        child
            .stdout
            .take()
            .unwrap()
            .read_to_end(&mut out)
            .map_err(|e| format_err_span!(err_span, "{}", e))?;
        Ok(out)
    }

    /// Generate a file from the TokenStream
    fn generate(path: LitStr, tokens: Vec<Rc<TokenStream>>) -> Result<()> {
        let data = Self::format_stream(path.span(), tokens)?;
        fs::write(path.value(), data).map_err(|e| format_err!(path, "{}", e))?;
        Ok(())
    }

    /// Compare with a file from the TokenStream
    fn compare(path: LitStr, tokens: Vec<Rc<TokenStream>>) -> Result<()> {
        let gen_data = Self::format_stream(path.span(), tokens)?;

        let comparer = GeneratorWrite {
            generator: None,
            lit: path.clone(),
            comparer: Some((gen_data, path)),
        };
        comparer.collect_output()
    }

    fn compare_all(entries: Vec<RustWriterEntry>) -> Result<()> {
        let mut errors = Vec::new();
        for entry in entries {
            if let Err(e) = Self::compare(entry.span, entry.content) {
                errors.push(e);
            }
        }
        if errors.len() == 0 {
            Ok(())
        } else {
            Err(errors.into())
        }
    }

    fn generate_all(entries: Vec<RustWriterEntry>) -> Result<()> {
        let mut errors = Vec::new();
        for entry in entries {
            if let Err(e) = Self::generate(entry.span, entry.content) {
                errors.push(e);
            }
        }
        if errors.len() == 0 {
            Ok(())
        } else {
            Err(errors.into())
        }
    }

    /// Create a tokenstream of an error
    fn into_tokenstream_inner(self) -> Result<TokenStream> {
        // Check if there are earlier errors. If so, do not write and add the errors to the token
        // stream
        if self.errors.len() > 0 {
            return Err(self.errors.into());
        }
        // Then, compare all the files. Do not generate anything if one compare fails
        Self::compare_all(self.compare_files)?;
        Self::generate_all(self.generate_files)?;
        Ok(TokenStream::from_iter(self.output.into_iter().map(
            |tokens| Rc::try_unwrap(tokens).expect("Expecting destructing last rc"),
        )))
    }

    /// Create the tokenstream
    pub fn into_tokenstream(self) -> TokenStream {
        self.into_tokenstream_inner()
            .unwrap_or_else(|err| err.into_token_stream())
    }
}

impl<'parent> RustWriterFile<'parent> {
    /// Append a tokenstream to the writer
    pub fn append(&mut self, stream: TokenStream) {
        let rc = Rc::new(stream);
        if let Some(ref mut parent_output) = self.parent_output {
            parent_output.push(rc.clone());
        }
        if let Some(ref mut entry) = self.write_entry {
            entry.content.push(rc.clone());
        }
        if let Some(ref mut entry) = self.compare_entry {
            entry.content.push(rc.clone());
        }
    }
}
