use crate::hir::{Property, Ty};
use crate::ident_ext::IdentExt;
use proc_macro2::TokenStream;
use quote::quote;
use syn::Lit;

pub trait PropertyGenExt<'ast> {
    fn gen_paramspec(&self) -> TokenStream;
    fn gen_register_override(index: usize, parent: &Property<'ast>) -> TokenStream;
    // fn gen_register_interface(&self) -> TokenStream;
}

impl<'ast> PropertyGenExt<'ast> for Property<'ast> {
    fn gen_paramspec(&self) -> TokenStream {
        let name = self.name.to_owned_string();

        // FIXME: Don't unwrap() here; instead, store a hir::Ty in the property instead of
        // a syn::Type, i.e. something that we already checked that can be represented by
        // Glib.
        let ty = Ty::extract_from_type(&self.type_).unwrap();

        let (para_spec, default) = (
            ty.to_gparam_spec_constructor(),
            ty.to_gparam_spec_min_max_default(),
        );

        let flags = self.attrs.flags_tokens();
        let nick = if let Some(Lit::Str(litstr)) = self.attrs.nick() {
            litstr.value()
        } else {
            // If no value is given, use name
            name.clone()
        };
        let blurb = if let Some(Lit::Str(litstr)) = self.attrs.blurb() {
            litstr.value()
        } else {
            // If no value is given, use name
            name.clone()
        };

        quote! {
            #para_spec(
                // We're using here the same value for name, nick and blurb because with
                // the current property declaration syntax we only have the property name.
                // In the future we can provide a way to declare this or we can compose
                // this names using the property name and appending a prefix or suffix.
                CString::new(#name).unwrap().as_ptr(),
                CString::new(#nick).unwrap().as_ptr(),
                CString::new(#blurb).unwrap().as_ptr(),

                // default value
                #default,

                #flags

                // FIXME: the construct only param will be used when we parse the
                // declaration attributes
                //| gobject_ffi::G_PARAM_CONSTRUCT_ONLY,
            )
        }
    }

    fn gen_register_override(index: usize, property: &Property<'ast>) -> TokenStream {
        let name = property.name.to_string();
        let index = index + 1;
        quote! { gobject_ffi::g_object_class_override_property(gobject_class, #index, ::std::ffi::CString::new(#name).unwrap().as_ptr()) }
    }
}
