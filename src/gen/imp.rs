use proc_macro2::{Ident, TokenStream};
use quote::quote;
use syn::Block;

use crate::glib_utils::*;
use crate::hir::names::{Names, ParentNames};
use crate::hir::{FnSig, Method, Override, OverrideItem, Signal, Slot, VirtualMethod};

pub fn slot_assignments<'ast>(names: &Names, slots: &[Slot<'ast>]) -> Vec<TokenStream> {
    let InstanceNameFfi = names.instance_ffi();

    slots
        .iter()
        .filter_map(|slot| match *slot {
            Slot::Method(_) => None,

            Slot::VirtualMethod(VirtualMethod { ref sig, .. }) => {
                let name = &sig.name;
                let trampoline_name = names.slot_trampoline(&name);

                Some(quote! {
                    vtable.#name = Some(#InstanceNameFfi::#trampoline_name);
                })
            }

            Slot::Signal(ref signal) => {
                let signalname = &signal.sig.name;
                let trampoline_name = names.slot_trampoline(&signalname);

                Some(quote! {
                    vtable.#signalname = Some(#InstanceNameFfi::#trampoline_name);
                })
            }
        })
        .collect::<Vec<_>>()
}

// If you rename this function, or move stuff out of it, please
// update doc-internals/src/types.md
pub fn instance_slot_trampolines<'this, 'ast: 'this>(
    names: &'this Names,
    slots: &'this [Slot<'ast>],
    overrides: impl Iterator<Item = &'this OverrideItem<'ast>>,
) -> Vec<TokenStream> {
    let callback_guard = glib_callback_guard();
    let InstanceName = names.instance();
    let InstanceNameFfi = names.instance_ffi();
    let tokens = |sig: &FnSig<'_>, parent_names: Option<&ParentNames>| {
        let trampoline_name = names.slot_trampoline(&sig.name);
        let method_impl_name = names.slot_impl(&sig.name);
        let inputs = sig.input_args_with_glib_types();
        let arg_names = sig.input_args_from_glib_types();

        let ret = quote! { instance.#method_impl_name(#arg_names) };
        let ret = sig.ret_to_glib(ret);
        let output = sig.output_glib_return_type();
        let receiver_instance = match parent_names {
            Some(parent_names) => {
                let ffi = parent_names.instance_ffi();
                quote! { #ffi }
            }
            None => {
                let ffi = names.instance_ffi();
                quote! { #ffi }
            }
        };
        quote! {
            unsafe extern "C" fn #trampoline_name(
                this: *mut #receiver_instance,
                #inputs
            ) #output
            {
                #callback_guard

                let this = this as *mut #InstanceNameFfi;
                let instance: &super::#InstanceName = &from_glib_borrow(this);
                #ret
            }
        }
    };
    let mut ret = slots
        .iter()
        .filter_map(|slot| match *slot {
            Slot::Method(_) => None,

            Slot::VirtualMethod(VirtualMethod { ref sig, .. }) => Some(tokens(sig, None)),

            Slot::Signal(ref signal) => Some(tokens(&signal.sig, None)),
        })
        .collect::<Vec<_>>();

    ret.extend(
        overrides
            .filter_map(|overrideitem| {
                if let OverrideItem::Method(method) = overrideitem {
                    Some(method)
                } else {
                    None
                }
            })
            .map(
                |Override {
                     ancestor,
                     inner: method,
                 }| {
                    // TODO: does the name here need mangling with the parent class?
                    tokens(&method.sig, Some(ancestor))
                },
            ),
    );

    ret
}

pub fn slot_default_handlers<'this, 'ast: 'this>(
    names: &'this Names,
    slots: &'this [Slot<'ast>],
    overrides: impl Iterator<Item = &'this OverrideItem<'ast>>,
) -> Vec<TokenStream> {
    let method = |sig: &FnSig<'_>, body: &Block, name: Option<&Ident>| {
        let name = if let Some(name) = name {
            name.clone()
        } else {
            names.slot_impl(&sig.name)
        };

        let inputs = &sig.inputs;
        let output = &sig.output;
        quote! {
            fn #name(#(#inputs),*) -> #output #body
        }
    };
    let mut ret = slots
        .iter()
        .map(|slot| match *slot {
            Slot::Method(Method {
                public: false,
                ref sig,
                body,
            }) => method(sig, body, Some(&sig.name)),
            Slot::Method(Method { ref sig, body, .. })
            | Slot::VirtualMethod(VirtualMethod {
                ref sig,
                body: Some(body),
                ..
            }) => method(sig, body, None),

            Slot::VirtualMethod(VirtualMethod {
                ref sig,
                body: None,
                ..
            }) => {
                let name = names.slot_impl(&sig.name);
                let inputs = &sig.inputs;
                let output = &sig.output;
                quote! {
                    fn #name(#(#inputs),*) -> #output {
                        panic!("Called abstract method {} with no implementation", stringify!(#name));
                    }
                }
            }

            Slot::Signal(Signal {
                ref sig,
                body: Some(body),
            }) => method(sig, body, None),

            Slot::Signal(Signal {
                ref sig,
                body: None,
            }) => {
                let name = names.slot_impl(&sig.name);
                let inputs = &sig.inputs;
                let output = &sig.output;
                quote! {
                    #[allow(unused_variables)] // since none of the inputs will be used
                    fn #name(#(#inputs),*) -> #output {
                        panic!("Called default signal handler {} with no implementation", stringify!(#name));
                    }
                }
            }
        })
        .collect::<Vec<_>>();

    ret.extend(
        overrides
            .filter_map(|overrideitem| {
                if let OverrideItem::Method(Override {
                    ancestor: _,
                    inner: method,
                }) = overrideitem
                {
                    Some(method)
                } else {
                    None
                }
            })
            .map(|m| method(&m.sig, m.body, None)),
    );

    ret
}

// If you rename this function, or move stuff out of it, please
// update doc-internals/src/types.md
pub fn extern_methods<'ast>(names: &Names, slots: &[Slot<'ast>]) -> Vec<TokenStream> {
    let InstanceName = names.instance();
    let InstanceNameFfi = names.instance_ffi();
    let callback_guard = glib_callback_guard();
    slots
        .iter()
        .filter_map(|slot| {
            match *slot {
                Slot::Method(Method { public: false, .. }) => None, // these don't get exposed
                // in the C API
                Slot::Method(Method {
                    public: true,
                    ref sig,
                    ..
                }) => {
                    let ffi_name = names.exported_fn(&sig.name);
                    let method_impl_name = names.slot_impl(&sig.name);
                    let inputs = sig.input_args_with_glib_types();
                    let args = sig.input_args_from_glib_types();
                    let ret = quote! { instance.#method_impl_name(#args) };
                    let ret = sig.ret_to_glib(ret);
                    let output = sig.output_glib_return_type();
                    Some(quote! {
                        #[no_mangle]
                        pub unsafe extern "C" fn #ffi_name(this: *mut #InstanceNameFfi,
                                                           #inputs) #output
                        {
                            #callback_guard

                            let instance: &#InstanceName = &from_glib_borrow(this);
                            #ret
                        }
                    })
                }

                Slot::VirtualMethod(VirtualMethod { ref sig, .. }) => {
                    let name = &sig.name;
                    let ffi_name = names.exported_fn(&sig.name);
                    let inputs = sig.input_args_with_glib_types();
                    let args = sig.input_arg_names();
                    let output = sig.output_glib_return_type();
                    Some(quote! {
                        #[no_mangle]
                        pub unsafe extern "C" fn #ffi_name(this: *mut #InstanceNameFfi,
                                                           #inputs) #output
                        {
                            #callback_guard

                            let vtable = (*this).get_vtable();
                            // We unwrap() because vtable.method_name is always set to a method_trampoline
                            (vtable.#name.as_ref().unwrap())(this, #args)
                        }
                    })
                }

                Slot::Signal(_) => None, // these don't get exposed in the C API
            }
        })
        .collect()
}
