use crate::gen::slots::{ReservedSlotDeclarations, SlotDeclarations};
use crate::hir::names::{Names, ParentNames};
use crate::hir::{OverrideItem, Property, Slot};
use syn::Field;

pub trait Boilerplate<'ast> {
    fn names(&self) -> &Names;
    fn parent_names(&self) -> &ParentNames;
    fn ancestors(&self) -> &[ParentNames];
    fn reserved_slot_declarations(&self) -> ReservedSlotDeclarations;
    fn slot_declarations(&self) -> SlotDeclarations<'_>;
    fn slots(&self) -> &[Slot<'ast>];
    fn fundamental_type(&self) -> ::glib::Type;
    fn properties(&self) -> &[Property<'_>];
    fn overrides(&self) -> Option<&[OverrideItem<'ast>]>;
    fn has_private(&self) -> bool;
    fn private_fields(&self) -> Option<&[&'ast Field]>;
}
