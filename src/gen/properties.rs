use proc_macro2::{Ident, TokenStream};
use quote::quote;

use crate::gen::PropertyGenExt;
use crate::hir::{Property, Ty};
use crate::ident_ext::IdentExt;

use std::iter::Map;

// pub fn properties_enum<'ast>(properties: impl Iterator<Item = Property<'ast>>) -> TokenStream {
// let mut is_empty = true;
// {
// let is_empty_mut = &mut is_empty;
//
// let properties: Vec<TokenStream> = properties
// .iter()
// .enumerate()
// .map(|(i, prop)| {
// is_empty = false;
// let name = &prop.name;
// let index = (i as u32) + 1;
// quote! { #name = #index }
// })
// .collect();
// }
//
// quote! {
// #[repr(u32)]
// enum Properties {
// #(#properties, )*
// }
// }
// }

pub fn property_setter<'this, 'ast: 'this>(
    properties: impl Iterator<Item = &'this Property<'ast>>,
) -> TokenStream {
    let props = properties.enumerate().map(|(i, prop)| {
        let index = (i as u32) + 1;
        let param = &prop.setter.param;
        let type_ = prop.type_;
        let body = prop.setter.body;

        quote! {
            #index => {
                let v: glib::Value = unsafe {FromGlibPtrNone::from_glib_none(value)};
                if let Some(#param) = v.get::<#type_>()
                    #body
            }
        }
    });

    quote! {
        #[allow(dead_code, unused_variables)]
        fn set_property_impl(&self, property_id: u32, value: *mut gobject_ffi::GValue) {
            match property_id {
                #(#props)*
                _ => {
                    // G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
                }
            }
        }
    }
}

pub fn property_getter<'this, 'ast: 'this>(
    properties: impl Iterator<Item = &'this Property<'ast>>,
) -> TokenStream {
    let props = properties.enumerate().map(|(i, prop)| {
        let index = (i as u32) + 1;
        let type_ = prop.type_;
        let body = prop.getter;

        // FIXME: Don't unwrap() here; instead, store a hir::Ty in the property instead of
        // a syn::Type, i.e. something that we already checked that can be represented by
        // Glib.
        let ty = Ty::extract_from_type(&prop.type_).unwrap();
        let g_value_set = ty.to_gvalue_setter();

        quote! {
            #index => {
                let ret: #type_ = (|| #body)();
                unsafe {
                    #g_value_set(value, ret);
                }
            }
        }
    });

    quote! {
        #[allow(dead_code, unused_variables)]
        fn get_property_impl(&self, property_id: u32, value: *mut gobject_ffi::GValue) {
            match property_id {
                #(#props)*
                _ => {
                    // G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
                }
            }
        }
    }
}

pub fn set_properties<'this, 'ast: 'this, Prop>(
    properties: Prop,
) -> Map<Prop, fn(&'this Property<'ast>) -> TokenStream>
where
    Prop: Iterator<Item = &'this Property<'ast>> + 'this,
{
    properties.map(|prop| {
        let name = Ident::from_str(format!(
            "set_property_{}",
            prop.name.to_owned_string().to_lowercase()
        ));
        let name_str = prop.name.to_owned_string();
        let type_ = prop.type_;

        quote! {
            fn #name(&self, v: #type_) -> Result<(), ::gobject_class::glib::error::BoolError> {
                ::glib::ObjectExt::set_property(self, #name_str, &v)
            }
        }
    })
}

pub fn get_properties<'this, 'ast: 'this, Prop>(
    properties: Prop,
) -> Map<Prop, fn(&'this Property<'ast>) -> TokenStream>
where
    Prop: Iterator<Item = &'this Property<'ast>> + 'this,
{
    properties.map(|prop| {
        let name = Ident::from_str(format!(
            "get_property_{}",
            prop.name.to_owned_string().to_lowercase()
        ));
        let name_str = prop.name.to_owned_string();
        let type_ = prop.type_;

        quote! {
            fn #name(&self) -> #type_ {
                ::gobject_class::glib::ObjectExt::get_property(self, #name_str).unwrap().get::<#type_>().unwrap()
            }
        }
    })
}

pub fn set_properties_fn<'this, 'ast: 'this, Prop>(
    properties: Prop,
) -> Map<Prop, fn(&'this Property<'ast>) -> TokenStream>
where
    Prop: Iterator<Item = &'this Property<'ast>> + 'this,
{
    properties.map(|prop: &'this Property<'ast>| {
        let name = Ident::from_str(format!(
            "set_property_{}",
            prop.name.to_owned_string().to_lowercase()
        ));
        let type_ = prop.type_;

        quote! {
            fn #name(&self, v: #type_) -> Result<(), ::gobject_class::glib::error::BoolError>;
        }
    })
}

pub fn get_properties_fn<'this, 'ast: 'this, Prop>(
    properties: Prop,
) -> Map<Prop, fn(&'this Property<'ast>) -> TokenStream>
where
    Prop: Iterator<Item = &'this Property<'ast>> + 'this,
{
    properties.map(|prop| {
        let name = Ident::from_str(format!(
            "get_property_{}",
            prop.name.to_owned_string().to_lowercase()
        ));
        let type_ = prop.type_;

        quote! {
            fn #name(&self) -> #type_;
        }
    })
}

fn prop_registration<'this, 'ast: 'this, Prop>(
    properties: Prop,
) -> Map<Prop, fn(&'this Property<'ast>) -> TokenStream>
where
    Prop: Iterator<Item = &'this Property<'ast>> + 'this,
{
    properties.map(|prop| {
        let register_prop = prop.gen_paramspec();

        quote! {
            properties.push(#register_prop);
        }
    })
}

/// The generated function registered all properties from `properies`
/// to the gobject-system. The paramspecs are saved in the private static class data.
pub fn properties_registration<'this, 'ast: 'this>(properties: &[Property<'ast>]) -> TokenStream {
    let first_override = properties
        .iter()
        .position(|prop| prop.is_override)
        .unwrap_or(properties.len());
    let virtual_props = properties[..first_override]
        .iter()
        .map(Property::gen_paramspec);
    let override_props = properties[first_override..]
        .iter()
        .enumerate()
        .map(|(index, item)| (index + first_override, item))
        .map(|(index, item)| Property::gen_register_override(index, item));

    quote! {
        {
            let gobject_class = &mut *(vtable as *mut gobject_ffi::GObjectClass);
            PRIV.private.properties = [ptr::null(), #(#virtual_props),*];

            // g_object_class_install_properties() skips the first one, as
            // it would have id 0, so we check for the number of
            // properties plus our dummy item from above
            ::gobject_class::gobject_sys::g_object_class_install_properties(
                gobject_class,
                PRIV.private.properties.len() as u32,
                PRIV.private.properties.as_mut_ptr() as *mut *mut _,
            );

            #(#override_props)*
        }
    }
}

fn prop_interface_registration<'this, 'ast: 'this, Prop>(
    properties: Prop,
) -> Map<Prop, fn(&'this Property<'ast>) -> TokenStream>
where
    Prop: Iterator<Item = &'this Property<'ast>> + 'this,
{
    properties.map(|prop| {
        let register_prop = prop.gen_paramspec();
        quote! {
            ::gobject_class::gobject_sys::g_object_interface_install_property(gobject_class, #register_prop);
        }
    })
}

/// This function registered the interface properties to the gobject-system.
/// It uses g_object_interface_property for every property in properties.
pub fn properties_interface_registration<'this, 'ast: 'this>(
    properties: impl Iterator<Item = &'this Property<'ast>> + 'this,
) -> TokenStream {
    let prop = prop_registration(properties);

    quote! {
        #(#prop)*
    }
}
