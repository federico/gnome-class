use proc_macro2::{Literal, TokenStream, TokenTree};
use quote::{quote, ToTokens, TokenStreamExt};

/// Wraps a `&str` so it can be tokenized as a C-friendly string literal.
///
/// For example, if the string slice holds the name Foo, then this
/// will generate "`b"Foo\0" as *const u8 as *const i8`", which is
/// suitable for calling C APIs.
pub struct CStringLiteral<'a>(pub &'a str);

impl<'a> ToTokens for CStringLiteral<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        // Make a b"Foo\0" byte literal

        let mut v = Vec::from(self.0.as_bytes());
        v.push(0u8);
        tokens.append(TokenTree::Literal(Literal::byte_string(&v)));
        (quote! { as *const u8 as *const i8 }).to_tokens(tokens);
    }
}
