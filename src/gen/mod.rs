// We give `ClassName` variables an identifier that uses upper-case.
#![allow(non_snake_case)]

use proc_macro2::TokenStream;

mod ancestorext;
mod boilerplate;
mod boilerplateext;
mod class;
mod cstringliteral;
mod fnsig_ext;
pub mod gir;
mod imp;
mod instance;
mod instance_ext;
mod interface;
mod private;
mod privatestatic;
mod properties;
mod property_ext;
mod public;
mod signals;
mod signatures;
mod slot_ext;
mod slots;
mod test;
mod writer;

pub use self::boilerplate::Boilerplate;
pub use self::boilerplateext::BoilerplateExt;
pub use self::class::Class;
pub use self::instance::Instance;
pub use self::interface::Interface;
pub use self::private::Private;

use self::test::Test;
use self::writer::RustWriter;
use crate::hir::Program;

use self::fnsig_ext::FnSigExt;
use self::property_ext::PropertyGenExt;
use self::slot_ext::SlotExt;

pub fn codegen(program: &Program<'_>) -> TokenStream {
    let mut writer = RustWriter::new();

    program.classes.iter().for_each(|class| {
        let mut class_writer = writer.class_writer(&class.attrs);
        class_writer.append(Class::new(program, class).to_token_stream());
    });

    program.interfaces.iter().for_each(|iface| {
        let mut class_writer = writer.class_writer(&iface.attrs);
        class_writer.append(Interface::new(program, iface).to_token_stream());
    });

    writer.into_tokenstream()
}

pub mod tests {
    use super::*;

    pub fn run() {
        signals::tests::run();
    }
}
