use proc_macro2::{Span, TokenStream};
use quote::{quote, ToTokens};
use syn::Token;

use crate::hir::{FnArg, FnSig, Ty};

impl<'ast> FnSig<'ast> {
    /// Generates `-> GlibTypeName` for the function's return value
    ///
    /// For example, if the `FnSig` represents a `fn foo(...) ->
    /// bool`, then this function will generate something that
    /// resolves to `-> glib_sys::gboolean` (including the arrow).
    ///
    /// For functions that return nothing, i.e. the `FnSig` has an `output: Ty::Unit`,
    /// then no `-> Foo` is generated at all.  This is so that `extern "C"` functions will look like
    ///
    /// ```rust,ignore
    /// unsafe extern "C" fn foo(...);
    /// ```
    ///
    /// instead of
    ///
    /// ```rust,ignore
    /// unsafe extern fn "C" foo(...) -> ();    // we don't want a unit type there
    /// ```
    pub fn output_glib_return_type<'a>(&'a self) -> impl ToTokens + 'a {
        ToGlibReturnType(&self.output, self)
    }

    /// Generates an argument list just with Rust types, suitable for `Fn` signatures, without
    /// `&Self`
    ///
    /// For example, if the `FnSig` represents a `fn foo(&self, a: bool, b: i32)`, then this
    /// function will generate tokens for `bool, i32`.  This is useful when generating
    /// an `Fn(&Self, bool, i32)` signature.
    ///
    /// Note that the first parameter `&Self` is omitted.  This is so that callers can
    /// emit it themselves.
    pub fn input_arg_types<'a>(&'a self) -> impl ToTokens + 'a {
        ArgTypes(self)
    }

    /// Generates an argument list with Glib types suitable for function prototypes, without the
    /// `&self`
    ///
    /// For example, if the `FnSig` represents a `fn foo(&self, a: bool, b: i32)`, then this
    /// function will generate tokens for `a: glib_sys::boolean, b: i32,`.  This is useful when
    /// generating a prototype for an `unsafe extern "C" fn` callable from C.
    ///
    /// Note that the first parameter `&self` is omitted.  This is so that callers
    /// can emit a suitable C pointer instead of a Rust `&self`.
    pub fn input_args_with_glib_types<'a>(&'a self) -> impl ToTokens + 'a {
        FnArgsWithGlibTypes(self)
    }

    /// Generates an argument list with values converted from Glib types, without the `&self`
    ///
    /// For example, if the `FnSig` represents a `fn foo(&self, a:
    /// bool, b: i32)`, then this function will generate tokens for
    /// `<bool as FromGlib<_>>::from_glib(a), b,`.  Presumably the
    /// generated tokens are being used in a function call from C to
    /// Rust.
    ///
    /// Note that the first parameter `&self` is omitted.  This is so that the caller
    /// can emit the tokens for the first argument as appropriate.
    pub fn input_args_from_glib_types<'a>(&'a self) -> impl ToTokens + 'a {
        ArgNamesFromGlib(&self.inputs[1..])
    }

    /// Generates an argument list with values converted to Glib types, without the `&self`
    ///
    /// For example, if the `FnSig` represents a `fn foo(&self, a:
    /// bool, b: i32)`, then this function will generate tokens for
    /// `<bool as ToGlib>::to_glib(&a), b,`.  Presumably the generated
    /// tokens are being used in a function call from Rust to C.
    ///
    /// Note that the first parameter `&self` is omitted.  This is so that the caller
    /// can emit the tokens for the first argument as appropriate.
    pub fn input_args_to_glib_types<'a>(&'a self) -> impl ToTokens + 'a {
        ArgNamesToGlib(&self.inputs[1..])
    }

    /// Generates an argument list with values converted to Glib values
    ///
    /// For example, if the `FnSig` represents a `fn foo(&self, a:
    /// bool, b: i32)`, then this function will generate tokens for
    /// `<self as &dyn glib::ToValue>.to_value(), <&a as &dyn glib::ToValue>::to_value(), <&b as
    /// &dyn glib::ToValue>::to_value(),`.  The generated tokens are suitable for
    /// generating a GValue argument list to be passed to `g_signal_emitv()`.
    pub fn input_args_to_glib_values<'a>(&'a self) -> impl ToTokens + 'a {
        ArgNamesToGlibValues(&self.inputs)
    }

    /// Generates a list of argument names with no type conversions, without the `&self`
    ///
    /// For example, if the `FnSig` represents a `fn foo(&self, a:
    /// bool, b: i32)`, then this function will generate tokens for
    /// `a, b,`.  This is just to pass through arguments from inside a
    /// wrapper function.
    ///
    /// Note that the first parameter `&self` is omitted.  This is so that the caller
    /// can emit the tokens for the first argument as appropriate.
    pub fn input_arg_names<'a>(&'a self) -> impl ToTokens + 'a {
        ArgNames(&self.inputs[1..])
    }

    /// Generates the conversion from a Rust return value into a Glib value
    ///
    /// For example, if the `FnSig` has an `output` type of `bool`,
    /// and the `tokens` correspond to `true`, this function will
    /// generate `<bool as ToGlib>::to_glib(&true)`.  This can be used
    /// by code which generates a function callable from C that wraps
    /// Rust code.
    pub fn ret_to_glib<'a, T: ToTokens + 'a>(&'a self, tokens: T) -> impl ToTokens + 'a {
        ToGlib(&self.output, tokens)
    }

    pub fn ret_from_glib_fn<'a, V: ToTokens>(&'a self, v: &'a V) -> impl ToTokens + 'a {
        let mut tokens = TokenStream::new();
        v.to_tokens(&mut tokens);
        FromGlib(&self.output, tokens)
    }
}

struct ToGlibReturnType<'ast>(&'ast Ty<'ast>, &'ast FnSig<'ast>);

impl<'ast> ToTokens for ToGlibReturnType<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        // Only output the "->" for non-unit return types
        match *self.0 {
            Ty::Unit => (),
            _ => {
                (quote! {
                    ->
                })
                .to_tokens(tokens);

                ToGlibType(self.0, self.1).to_tokens(tokens)
            }
        }
    }
}

struct ToGlibType<'ast>(&'ast Ty<'ast>, &'ast FnSig<'ast>);

impl<'ast> ToTokens for ToGlibType<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match *self.0 {
            Ty::Unit => self.0.to_tokens(tokens),
            Ty::Char(ref i) | Ty::Bool(ref i) => {
                (quote! {
                    <#i as ToGlib>::GlibType
                })
                .to_tokens(tokens);
            }
            Ty::Borrowed(ref t) => {
                (quote! {
                    <#t as GlibPtrDefault>::GlibType
                })
                .to_tokens(tokens);
            }
            Ty::Integer(ref i) => i.to_tokens(tokens),
            Ty::Owned(ref t) => {
                (quote! {
                    <#t as GlibPtrDefault>::GlibType
                })
                .to_tokens(tokens);
            }
        }
    }
}

struct ToGlib<'ast, T>(&'ast Ty<'ast>, T);

impl<'ast, T: ToTokens> ToTokens for ToGlib<'ast, T> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let expr = &self.1;
        match *self.0 {
            // no conversion necessary
            Ty::Unit | Ty::Integer(_) => self.1.to_tokens(tokens),

            Ty::Char(ref i) | Ty::Bool(ref i) => {
                (quote! {
                    <#i as ToGlib>::to_glib(&#expr)
                })
                .to_tokens(tokens);
            }
            Ty::Borrowed(ref t) => {
                (quote! {
                    <#t as ToGlibPtr<_>>::to_glib_none(#expr).0
                })
                .to_tokens(tokens);
            }
            Ty::Owned(ref t) => {
                (quote! {
                    <#t as ToGlibPtr<_>>::to_glib_full(&#expr)
                })
                .to_tokens(tokens);
            }
        }
    }
}

struct FromGlib<'ast>(&'ast Ty<'ast>, TokenStream);

impl<'ast> ToTokens for FromGlib<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self {
            FromGlib(Ty::Unit, inner) => {
                inner.to_tokens(tokens);
            } // no conversion necessary
            FromGlib(Ty::Char(ref i), inner) | FromGlib(Ty::Bool(ref i), inner) => {
                tokens.extend(quote! {
                    <#i as FromGlib<_>>::from_glib(#inner)
                })
            }
            FromGlib(Ty::Borrowed(ref t), inner) => tokens.extend(quote! {
                &<#t as FromGlibPtrBorrow<_>>::from_glib_borrow(#inner)
            }),
            FromGlib(Ty::Integer(_), inner) => inner.to_tokens(tokens), // no conversion necessary
            FromGlib(Ty::Owned(t), inner) => {
                tokens.extend(quote! {
                    #[allow(unused_unsafe)] unsafe { <#t as FromGlibPtrFull<_>>::from_glib_full(#inner) }
                });
            }
        }
    }
}

struct ArgTypes<'ast>(&'ast FnSig<'ast>);

impl<'ast> ToTokens for ArgTypes<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        for arg in self.0.inputs[1..].iter() {
            match *arg {
                FnArg::Arg { ref ty, .. } => {
                    ty.to_tokens(tokens);
                    Token!(,)([Span::call_site()]).to_tokens(tokens);
                }
                FnArg::SelfRef(..) => unreachable!(),
            }
        }
    }
}

struct FnArgsWithGlibTypes<'ast>(&'ast FnSig<'ast>);

impl<'ast> ToTokens for FnArgsWithGlibTypes<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        for arg in self.0.inputs[1..].iter() {
            match *arg {
                FnArg::Arg {
                    ref name, ref ty, ..
                } => {
                    name.to_tokens(tokens);
                    Token!(:)([Span::call_site()]).to_tokens(tokens);
                    ToGlibType(ty, self.0).to_tokens(tokens);
                    Token!(,)([Span::call_site()]).to_tokens(tokens);
                }
                FnArg::SelfRef(..) => unreachable!(),
            }
        }
    }
}

struct ArgNamesFromGlib<'ast>(&'ast [FnArg<'ast>]);

impl<'ast> ToTokens for ArgNamesFromGlib<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        for arg in self.0 {
            match *arg {
                FnArg::Arg {
                    ref name, ref ty, ..
                } => {
                    let mut name_tokens = TokenStream::new();
                    name.to_tokens(&mut name_tokens);
                    FromGlib(ty, name_tokens).to_tokens(tokens);
                    Token!(,)([Span::call_site()]).to_tokens(tokens);
                }
                FnArg::SelfRef(..) => unreachable!(),
            }
        }
    }
}

struct ArgNamesToGlib<'ast>(&'ast [FnArg<'ast>]);

impl<'ast> ToTokens for ArgNamesToGlib<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        for arg in self.0 {
            match *arg {
                FnArg::Arg {
                    ref ty, ref name, ..
                } => {
                    ToGlib(ty, name).to_tokens(tokens);
                    Token!(,)([Span::call_site()]).to_tokens(tokens);
                }
                FnArg::SelfRef(..) => unreachable!(),
            }
        }
    }
}

struct ArgNamesToGlibValues<'ast>(&'ast [FnArg<'ast>]);

impl<'ast> ToTokens for ArgNamesToGlibValues<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        for arg in self.0 {
            match *arg {
                FnArg::SelfRef(..) => {
                    let code = quote! {
                        (self as &dyn glib::ToValue).to_value(),
                    };

                    code.to_tokens(tokens);
                }

                FnArg::Arg { ref name, .. } => {
                    let code = quote! {
                        (&#name as &dyn glib::ToValue).to_value(),
                    };

                    code.to_tokens(tokens);
                }
            }
        }
    }
}

struct ArgNames<'ast>(&'ast [FnArg<'ast>]);

impl<'ast> ToTokens for ArgNames<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        for arg in self.0 {
            match *arg {
                FnArg::Arg { ref name, .. } => {
                    name.to_tokens(tokens);
                    Token!(,)([Span::call_site()]).to_tokens(tokens);
                }
                FnArg::SelfRef(..) => unreachable!(),
            }
        }
    }
}
