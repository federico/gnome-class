use crate::hir::FnSig;
use proc_macro2::TokenStream;
use quote::quote;

pub trait FnSigExt {
    fn signal_definition(&self) -> TokenStream;
}

impl<'ast> FnSigExt for FnSig<'ast> {
    fn signal_definition(&self) -> TokenStream {
        let name = self.name.clone();
        let inputs = &self.inputs;
        let output = &self.output;
        quote! {
            fn #name(#(#inputs),*) -> #output
        }
    }
}
