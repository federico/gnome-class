/// ! This file has code to generate a test to test the generated gobject type.
/// ! The test can find missing parents or other user error which
/// ! can't be collected during codegen
use crate::gen::boilerplate;
use proc_macro2::{Ident, TokenStream};
use quote::{quote, ToTokens};
use std::marker::PhantomData;

pub struct Test<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast> + 'lt> {
    boilerplate: &'lt Boilerplate,
    phantom: PhantomData<&'ast ()>,
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> ToTokens
    for Test<'lt, 'ast, Boilerplate>
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        tokens.extend(self.gen_test());
    }
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> Test<'lt, 'ast, Boilerplate> {
    pub fn new(boilerplate: &'lt Boilerplate) -> Self {
        let phantom = PhantomData;
        Test {
            boilerplate,
            phantom,
        }
    }

    fn gen_test(&self) -> TokenStream {
        let test_get_type_fn_name = Ident::new(
            ("test_".to_owned() + self.boilerplate.names().get_type_fn().to_string().as_str())
                .as_str(),
            ::proc_macro2::Span::call_site(),
        );
        let ancestors = self.boilerplate.ancestors().iter().filter_map(|anc| {
            if anc.is_fundamental() {
                None
            } else {
                Some(anc.instance())
            }
        });
        let name_instance = &self.boilerplate.names().instance();

        let create_type = if self.boilerplate.fundamental_type() == ::glib::Type::BaseObject {
            quote! { #name_instance::new(); }
        } else {
            TokenStream::new()
        };

        quote! {
            #[test]
            fn #test_get_type_fn_name()
            {
                let _panic_on_message = ::gobject_class::testhelper::PanicOnMessage::new();

                use ::gobject_class::glib::Type;

                //Test
                let mut parents = vec![ Type::BaseObject #(, <#ancestors as ::glib::types::StaticType>::static_type())* ];
                let mut interfaces = vec![];
                let mut current = <#name_instance as ::gobject_class::glib::types::StaticType>::static_type();

                while let Some(parent) = current.parent() {
                    current = parent;
                    if let Some(pos) = parents.iter().position(|x| x == &current) {
                        parents.swap_remove(pos);
                    } else if current != Type::BaseObject && current != Type::BaseInterface {
                        panic!("Unregistered parent \"{}\"", current.name());
                    }
                }

                current = <#name_instance as ::gobject_class::glib::types::StaticType>::static_type();
                for interface in current.interfaces() {
                    if let Some(pos) = interfaces.iter().position(|x: &::gobject_class::glib::Type| x == &interface) {
                        interfaces.swap_remove(pos);
                    } else if interface != ::gobject_class::glib::Type::BaseInterface {
                        panic!("Unregistered interface \"{}\"", current.name());
                    }
                }

                #create_type
            }
        }
    }
}
