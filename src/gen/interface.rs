// We give `ClassName` variables an identifier that uses upper-case.
#![allow(non_snake_case)]

use crate::hir;
use crate::hir::{OverrideItem, Program, Property, Slot};

use crate::gen::boilerplate::Boilerplate;
use crate::gen::slots::{ReservedSlotDeclarations, SlotDeclarations};
use crate::hir::names::{Names, ParentNames};

use syn::Field;

pub struct Interface<'ast> {
    pub program: &'ast Program<'ast>,
    pub iface: &'ast hir::Interface<'ast>,
    pub parent_names: ParentNames,
}

impl<'ast> Boilerplate<'ast> for Interface<'ast> {
    fn names(&self) -> &Names {
        &self.iface.names
    }

    fn parent_names(&self) -> &ParentNames {
        &self.parent_names
    }

    fn reserved_slot_declarations(&self) -> ReservedSlotDeclarations {
        // Interface vtables don't have reserved slots, as they are
        // allocated by gtype, not by the user.
        ReservedSlotDeclarations(0)
    }

    fn slot_declarations(&self) -> SlotDeclarations<'_> {
        SlotDeclarations {
            InstanceNameFfi: self.iface.names.instance_ffi(),
            slots: &self.iface.slots,
        }
    }

    fn slots(&self) -> &[Slot<'ast>] {
        &self.iface.slots
    }

    fn fundamental_type(&self) -> ::glib::Type {
        ::glib::Type::BaseInterface
    }

    fn properties(&self) -> &[Property<'_>] {
        &self.iface.properties
    }

    fn overrides(&self) -> Option<&[OverrideItem<'ast>]> {
        None
    }

    fn has_private(&self) -> bool {
        false
    }

    fn private_fields(&self) -> Option<&[&'ast Field]> {
        None
    }

    fn ancestors(&self) -> &[ParentNames] {
        &self.iface.ancestors
    }
}

impl<'ast> Interface<'ast> {
    pub fn new(program: &'ast Program<'_>, iface: &'ast hir::Interface<'_>) -> Self {
        let parent_names = ParentNames::new_from_glib(::glib::Type::BaseInterface);

        Interface {
            program,
            iface,
            parent_names,
        }
    }
}
