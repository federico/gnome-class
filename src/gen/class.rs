// We give `ClassName` variables an identifier that uses upper-case.
#![allow(non_snake_case)]

use crate::gen::boilerplate::Boilerplate;
use crate::gen::slots::{ReservedSlotDeclarations, SlotDeclarations};
use crate::hir;
use crate::hir::names::{Names, ParentNames};
use crate::hir::{OverrideItem, Program, Property, Slot};
use syn::Field;

pub struct Class<'ast> {
    pub program: &'ast Program<'ast>,
    pub class: &'ast hir::Class<'ast>,
}

impl<'ast> Boilerplate<'ast> for Class<'ast> {
    fn names(&self) -> &Names {
        &self.class.names
    }

    fn parent_names(&self) -> &ParentNames {
        &self.class.parent
    }

    fn reserved_slot_declarations(&self) -> ReservedSlotDeclarations {
        ReservedSlotDeclarations(self.class.n_reserved_slots)
    }

    fn slot_declarations(&self) -> SlotDeclarations<'_> {
        SlotDeclarations {
            InstanceNameFfi: self.class.names.instance_ffi().clone(),
            slots: &self.class.slots,
        }
    }

    fn slots(&self) -> &[Slot<'ast>] {
        &self.class.slots
    }

    fn fundamental_type(&self) -> ::glib::Type {
        ::glib::Type::BaseObject
    }

    fn properties(&self) -> &[Property<'_>] {
        &self.class.properties
    }

    fn overrides(&self) -> Option<&[OverrideItem<'ast>]> {
        Some(&self.class.overrides)
    }

    fn has_private(&self) -> bool {
        self.class.private_fields.len() > 0
    }

    fn private_fields(&self) -> Option<&[&'ast Field]> {
        Some(&self.class.private_fields)
    }

    fn ancestors(&self) -> &[ParentNames] {
        &self.class.ancestors
    }
}

impl<'ast> Class<'ast> {
    #[cfg_attr(rustfmt, rustfmt_skip)]
    pub fn new(program: &'ast Program<'_>, class: &'ast hir::Class<'_>) -> Self {
        // This function creates a Class by generating the
        // commonly-used symbol names for the class in question, for
        // example, "FooClass" out of "Foo".

        Class {
            program,
            class,
        }
    }
}
