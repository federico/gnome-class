use super::boilerplate;
use crate::gen::boilerplateext::BoilerplateExt;
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use std::marker::PhantomData;

pub struct Instance<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> {
    boilerplate: &'lt Boilerplate,
    phantom: PhantomData<&'ast ()>,
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> ToTokens
    for Instance<'lt, 'ast, Boilerplate>
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let instance_struct = self.gen_instance_struct();
        let impl_super_instance = self.gen_impl_super_instance();
        let impl_instance_ffi = self.gen_impl_instance_ffi();
        let private_struct = self.gen_private_struct();
        let new_instance = self.gen_new_instance();

        tokens.extend(quote! {
            #instance_struct
            #impl_super_instance
            #impl_instance_ffi
            #private_struct
            #new_instance
        })
    }
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> Instance<'lt, 'ast, Boilerplate> {
    pub fn new(boilerplate: &'lt Boilerplate) -> Self {
        let phantom = PhantomData;
        Instance {
            boilerplate,
            phantom,
        }
    }

    fn gen_instance_struct(&self) -> TokenStream {
        let instance_name_ffi = self.boilerplate.names().instance_ffi();
        let parent_ffi = self.boilerplate.parent_names().instance_ffi();

        quote! {
            #[repr(C)]
            pub struct #instance_name_ffi { parent: #parent_ffi }
        }
    }

    fn gen_impl_super_instance(&self) -> TokenStream {
        let instance = self.boilerplate.names().instance();
        let slot_default_handlers = crate::gen::imp::slot_default_handlers(
            &self.boilerplate.names(),
            &self.boilerplate.slots(),
            self.boilerplate.overrides().iter().flat_map(|x| *x),
        );
        let signal_emit_methods =
            crate::gen::signals::signal_emit_methods(self.boilerplate.signals());
        let get_priv_fn = self.gen_get_priv_fn();

        let property_getters_setters = self.gen_impl_super_instance_properties();
        quote! {
            // We are inside the "mod imp".  We will create function
            // implementations for the default handlers for methods and
            // signals as "impl super::Foo { ... }", so that the &self in
            // those functions will refer to the Rust wrapper object that
            // corresponds to the GObject-ABI structs within "mod imp".
            impl super::#instance {
                #get_priv_fn

                #(#slot_default_handlers)*

                #signal_emit_methods

                #property_getters_setters
            }
        }
    }

    fn gen_impl_super_instance_properties(&self) -> TokenStream {
        if self.boilerplate.fundamental_type() != glib::Type::BaseObject {
            return TokenStream::new();
        }

        let property_getter =
            crate::gen::properties::property_getter(self.boilerplate.properties().iter());
        let property_setter =
            crate::gen::properties::property_setter(self.boilerplate.properties().iter());
        quote! {
            #property_setter
            #property_getter
        }
    }

    fn gen_impl_instance_ffi(&self) -> TokenStream {
        let instance_ffi = self.boilerplate.names().instance_ffi();
        let vtable = self.boilerplate.names().vtable();
        let instance_slot_trampolines = crate::gen::imp::instance_slot_trampolines(
            self.boilerplate.names(),
            self.boilerplate.slots(),
            self.boilerplate
                .overrides()
                .iter()
                .flat_map(::std::ops::Deref::deref),
        );
        let instance_init_finalize = self.get_impl_instance_init_finalize();
        let property_getset = self.gen_impl_getset_property();

        quote! {
            impl #instance_ffi {
                #[allow(dead_code)] // not used if no virtual methods
                pub(super) fn get_vtable(&self) -> &#vtable {
                    unsafe {
                        let klass = (*(self as *const _ as *const gobject_ffi::GTypeInstance)).g_class;
                        &*(klass as *const #vtable)
                    }
                }

                #instance_init_finalize
                #property_getset

                #(#instance_slot_trampolines)*
            }
        }
    }

    fn gen_get_priv_fn(&self) -> TokenStream {
        if !self.boilerplate.has_private() {
            return TokenStream::new();
        }

        let instance_private = self.boilerplate.names().instance_private();
        let instance_ffi = self.boilerplate.names().instance_ffi();

        quote! {
            #[allow(dead_code)]
            fn get_priv(&self) -> &#instance_private {
                let glib : *const #instance_ffi = self.as_ptr() as *mut _;
                unsafe {
                    &*((glib as *const u8).wrapping_offset(PRIV.private_offset as isize) as *const #instance_private)
                }
            }
        }
    }

    fn gen_init_priv_with_default(&self) -> TokenStream {
        // Private does only need to be initialized if it exists.
        if !self.boilerplate.has_private() {
            return TokenStream::new();
        }

        let private = self.boilerplate.names().instance_private();

        quote! {
            // Private initialization is only neccesairy if there is there actually exists private data
            if ::std::mem::size_of::<#private>() > 0 {
                // Get the private data from the current offset
                let private = &mut *((obj as *const u8).wrapping_offset(PRIV.private_offset as isize) as *mut #private);

                // Here we initialize the private data.  GObject gives it to us all zero-initialized
                // but we don't really want to have any Drop impls run here so just overwrite the
                // data.
                ptr::write(private, <#private as Default>::default());
            }
        }
    }

    fn gen_free_instance_private(&self) -> TokenStream {
        if !self.boilerplate.has_private() {
            return TokenStream::new();
        }

        let instance_private = self.boilerplate.names().instance_private();
        let get_type_fn = self.boilerplate.names().get_type_fn();

        quote! {
            let _private = gobject_ffi::g_type_instance_get_private(
                obj as *mut gobject_ffi::GTypeInstance,
                #get_type_fn(),
            ) as *mut #instance_private;

            // Drop contents of private data by replacing its
            // content with uninitialized data
            let uninitialized = std::mem::uninitialized();
            std::mem::replace(&mut *_private, uninitialized);
        }
    }

    fn get_impl_instance_init_finalize(&self) -> TokenStream {
        if self.boilerplate.fundamental_type() != glib::Type::BaseObject {
            return TokenStream::new();
        }

        let callback_guard = crate::glib_utils::glib_callback_guard();
        let init_priv_with_default = self.gen_init_priv_with_default();
        let free_instance_private = self.gen_free_instance_private();

        quote! {
            // Instance struct and private data initialization, called from GObject
            unsafe extern "C" fn init(obj: *mut gobject_ffi::GTypeInstance, _klass: glib_ffi::gpointer) {
                #[allow(unused_variables)] // not used if no private
                let obj = obj;
                #callback_guard

                #init_priv_with_default
            }

            // Not in interface
            unsafe extern "C" fn finalize(obj: *mut gobject_ffi::GObject) {
                #callback_guard

                #free_instance_private

                (*(PRIV.private.parent_vtable as *mut gobject_ffi::GObjectClass)).finalize.map(|f| f(obj));
            }
        }
    }

    fn gen_impl_getset_property(&self) -> TokenStream {
        if self.boilerplate.fundamental_type() != glib::Type::BaseObject {
            return TokenStream::new();
        }

        let instance = self.boilerplate.names().instance();
        let instance_ffi = self.boilerplate.names().instance_ffi();
        let callback_guard = crate::glib_utils::glib_callback_guard();

        quote! {
            unsafe extern "C" fn set_property(obj: *mut gobject_ffi::GObject,
                                              property_id: u32,
                                              value: *mut gobject_ffi::GValue,
                                              _pspec: *mut gobject_ffi::GParamSpec) {
                #callback_guard

                let this: &#instance = & #instance::from_glib_borrow(obj as * mut #instance_ffi);
                this.set_property_impl(property_id, value);
            }

            unsafe extern "C" fn get_property(obj: *mut gobject_ffi::GObject,
                                              property_id: u32,
                                              value: *mut gobject_ffi::GValue,
                                              _pspec: *mut gobject_ffi::GParamSpec) {
                # callback_guard

                let this: &#instance = &#instance::from_glib_borrow(obj as * mut #instance_ffi);
                this.get_property_impl(property_id, value);
            }
        }
    }

    fn gen_private_struct(&self) -> TokenStream {
        if !self.boilerplate.has_private() {
            return TokenStream::new();
        }
        let private = self.boilerplate.names().instance_private();
        let private_fields_source = self.boilerplate.private_fields();
        let private_fields = private_fields_source
            .iter()
            .flat_map(std::ops::Deref::deref);

        quote! {
            #[derive(Default)]
            struct #private {
                #(#private_fields,)*
            }
        }
    }

    fn gen_new_instance(&self) -> TokenStream {
        let callback_guard = crate::glib_utils::glib_callback_guard();
        let instance_ffi = self.boilerplate.names().instance_ffi();
        let imp_new_fn_name = self.boilerplate.names().exported_fn_from_str("new");
        let get_type_fn_name = self.boilerplate.names().get_type_fn();

        // This function should only be generated for types which can be constructed
        if self.boilerplate.fundamental_type() != glib::Type::BaseObject {
            return TokenStream::new();
        }

        quote! {
            #[no_mangle]
            pub unsafe extern "C" fn #imp_new_fn_name(/* FIXME: args */) -> *mut #instance_ffi {
                #callback_guard

                let this = gobject_ffi::g_object_newv(
                    #get_type_fn_name(),
                    0,              // FIXME: num_arguments
                    ptr::null_mut() // FIXME: args
                );

                this as *mut #instance_ffi
            }
        }
    }
}
