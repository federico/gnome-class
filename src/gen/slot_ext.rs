use super::FnSigExt;
use crate::gen::cstringliteral::CStringLiteral;
use crate::gen::signals;
use crate::hir::names::Names;
use crate::hir::{FnSig, Method, Signal, Slot, VirtualMethod};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};

pub trait SlotExt {
    fn trait_definition(&self) -> TokenStream;
    fn trait_implementation(&self, names: &Names) -> TokenStream;
    fn public(&self) -> bool;
}

trait SlotInnerExt {
    fn sig(&self) -> &FnSig;
    fn body(&self, names: &Names) -> TokenStream;
}

trait SignalExt {
    fn connect_def(&self) -> TokenStream;
    fn connect_impl(&self, names: &Names) -> TokenStream;
}

impl<'ast> SlotExt for Slot<'ast> {
    fn trait_definition(&self) -> TokenStream {
        let sig = match self {
            Slot::VirtualMethod(ref method) => method.sig(),
            Slot::Method(ref method) => method.sig(),
            Slot::Signal(ref method) => {
                return method.connect_def();
            }
        }
        .signal_definition();

        quote! {
            #sig;
        }
    }

    fn trait_implementation(&self, names: &Names) -> TokenStream {
        let (sig, body) = match self {
            Slot::VirtualMethod(ref method) => (method.sig(), method.body(names)),
            Slot::Method(ref method) => (method.sig(), method.body(names)),
            Slot::Signal(ref method) => {
                return method.connect_impl(names);
            }
        };
        let sig = sig.signal_definition();

        quote! {
            #sig #body
        }
    }

    fn public(&self) -> bool {
        match self {
            Slot::Method(ref method) => method.public,
            _ => true,
        }
    }
}

impl<'ast> SlotInnerExt for Method<'ast> {
    fn sig(&self) -> &FnSig {
        &self.sig
    }

    fn body(&self, _names: &Names) -> TokenStream {
        self.body.clone().into_token_stream()
    }
}

impl<'ast> SlotInnerExt for VirtualMethod<'ast> {
    fn sig(&self) -> &FnSig {
        &self.sig
    }

    fn body(&self, names: &Names) -> TokenStream {
        if self.body.is_some() {
            let name = &self.sig.name;
            let args = self.sig.input_args_to_glib_types();
            let instance_ffi = names.instance_ffi();
            let ret = quote! {ret};
            let ret_from_glib_fn = self.sig.ret_from_glib_fn(&ret);
            quote! { {
                let ffi: *mut #instance_ffi = <Self as glib::translate::ToGlibPtr<'_, *mut #instance_ffi>>::to_glib_none(self).0;
                let vtable = unsafe { (*ffi).get_vtable() };
                // We unwrap() because vtable.method_name is always set to a method_trampoline
                let ret = unsafe { (vtable.#name.as_ref().unwrap())(ffi, #args) };
                #ret_from_glib_fn
            } }
        } else {
            let panic_msg = format!(
                "Called abstract method {} with no implementation",
                &self.sig.name
            );
            quote! { { panic!(#panic_msg) } }
        }
    }
}

impl<'ast> SignalExt for Signal<'ast> {
    fn connect_def(&self) -> TokenStream {
        let connect_signalname = signals::connect_signalname(self);
        let sig = &self.sig;
        let input_types = self.sig.input_arg_types();
        let output = &sig.output;

        quote! {
            fn #connect_signalname<F: Fn(&Self, #input_types) -> #output + 'static>(&self, f: F) ->
                glib::SignalHandlerId;
        }
    }

    fn connect_impl(&self, names: &Names) -> TokenStream {
        let connect_signalname = signals::connect_signalname(self);
        let sig = &self.sig;
        let input_types = self.sig.input_arg_types();
        let output = &sig.output;

        let signalname = sig.name.to_string();
        let signalname_cstr = CStringLiteral(&signalname);
        let signalname_trampoline = signals::signal_trampoline_name(self);
        let arg_names = sig.input_args_from_glib_types();
        let c_inputs = sig.input_args_with_glib_types();
        let glib_return_type = sig.output_glib_return_type();
        let InstanceName = names.instance();
        let InstanceNameFfi = names.instance_ffi();

        let ret = quote! {
            f(&#InstanceName::from_glib_borrow(this).unsafe_cast(), #arg_names)
        };
        let ret = sig.ret_to_glib(ret);

        quote! {
            fn #connect_signalname<F: Fn(&Self, #input_types) -> #output + 'static>(&self, f: F) ->
                glib::SignalHandlerId
            {
                unsafe extern "C" fn #signalname_trampoline<
                    P,
                    F: Fn(&P, #input_types) -> #output + 'static,
                >(
                    this: *mut imp::#InstanceNameFfi,
                    #c_inputs
                    f: glib_ffi::gpointer,
                ) #glib_return_type
                where
                    P: IsA<#InstanceName>,
                {
                    let f: &F = &*(f as *const F);
                    #ret
                }

                unsafe {
                    let f: Box<F> = Box::new(f);

                    glib::signal::connect_raw(
                        self.as_ptr() as *mut _,
                        #signalname_cstr,
                        Some(mem::transmute(#signalname_trampoline::<Self, F> as usize)),
                        Box::into_raw(f),
                    )
                }
            }
        }
    }
}
