use proc_macro2::TokenStream;
use quote::{quote, ToTokens};

use crate::hir::{Method, Slot, Ty, VirtualMethod};

use super::signals;
use crate::hir::names::Names;

/// Generates function prototypes for use within the generated FooExt trait.
///
/// Returns, for each method, something like
///
/// ```notest
/// fn foo(&self, arg: u32);
/// ```
///
/// Or for signals, something like
///
/// ```notest
/// fn connect_foo<F: Fn(&Self, arg: u32) + 'static>(&self, f: F) -> glib::SignalHandlerId
/// ```
pub fn slot_trait_fns<'ast>(slots: &[Slot<'ast>]) -> Vec<TokenStream> {
    slots
        .iter()
        .filter_map(|slot| match *slot {
            Slot::Method(Method { public: false, .. }) => None,

            Slot::Method(Method {
                public: true,
                ref sig,
                ..
            })
            | Slot::VirtualMethod(VirtualMethod { ref sig, .. }) => {
                let name = &sig.name;
                let inputs = &sig.inputs;
                let output = ToReturnType(&sig.output);
                Some(quote! {
                    fn #name(#(#inputs),*) #output;
                })
            }

            Slot::Signal(ref signal) => {
                let connect_signalname = signals::connect_signalname(signal);
                let sig = &signal.sig;
                let input_types = signal.sig.input_arg_types();
                let output = ToReturnType(&sig.output);
                Some(quote! {
                    fn #connect_signalname<F: Fn(&Self, #input_types) #output + 'static>(&self, f: F) ->
                        glib::SignalHandlerId;
                })
            }
        })
        .collect()
}

/// Generates the implementations of the trait functions defined
/// in `slot_trait_fns()`.
pub fn slot_trait_impls<'ast>(names: &Names, slots: &[Slot<'ast>]) -> Vec<TokenStream> {
    slots
        .iter()
        .filter_map(|slot| match *slot {
            Slot::Method(Method { public: false, .. }) => None,

            Slot::Method(Method {
                public: true,
                ref sig,
                ..
            })
            | Slot::VirtualMethod(VirtualMethod { ref sig, .. }) => {
                let name = &sig.name;
                let ffi_name = names.exported_fn(&name);
                let arg_names = sig.input_args_to_glib_types();
                let value = quote! {
                    unsafe {
                        imp::#ffi_name(self.to_glib_none().0
                                       #arg_names)
                    }
                };
                let output_from = sig.ret_from_glib_fn(&value);
                let inputs = &sig.inputs;
                let output = ToReturnType(&sig.output);
                Some(quote! {
                    fn #name(#(#inputs),*) #output {
                        #output_from
                    }
                })
            }

            Slot::Signal(ref signal) => {
                let connect_signalname = signals::connect_signalname(signal);
                let sig = &signal.sig;
                let signalname_str = signals::canonicalize_signal_name(&sig.name.to_string())
                    .expect("rust identifiers should always be canonicalizable");
                let input_types = signal.sig.input_arg_types();
                let output = ToReturnType(&sig.output);

                Some(quote! {
                    fn #connect_signalname<F: Fn(&Self, #input_types) #output + 'static>(&self, f: F) ->
                        glib::SignalHandlerId
                    {
                        glib::ObjectExt::connect(self,
                                                 #signalname_str,
                                                 false,
                                                 f).unwrap()
                    }
                })
            }
        })
        .collect()
}

struct ToReturnType<'a>(&'a Ty<'a>);

impl<'a> ToTokens for ToReturnType<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self.0 {
            Ty::Unit => {}
            other => (quote! { -> #other }).to_tokens(tokens),
        }
    }
}
