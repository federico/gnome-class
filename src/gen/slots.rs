use proc_macro2::{Ident, TokenStream};

use quote::{quote, ToTokens};

use crate::hir::{Slot, VirtualMethod};

struct SlotDeclaration<'ast> {
    InstanceNameFfi: &'ast Ident,
    slot: &'ast Slot<'ast>,
}

impl<'ast> ToTokens for SlotDeclaration<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        // ABI: we are generating the imp::FooClass with the parent_class, and the slots to
        // signals/methods. This defines the C ABI for the class structure.

        let InstanceNameFfi = self.InstanceNameFfi;

        match *self.slot {
            Slot::Method(_) => (),

            Slot::VirtualMethod(VirtualMethod { ref sig, .. }) => {
                let output = sig.output_glib_return_type();
                let inputs = sig.input_args_with_glib_types();
                let name = &sig.name;
                (quote! {
                    pub #name: Option<unsafe extern "C" fn(
                        this: *mut #InstanceNameFfi,
                        #inputs
                    ) #output>,
                })
                .to_tokens(tokens);
            }

            Slot::Signal(ref signal) => {
                let output = signal.sig.output_glib_return_type();
                let inputs = signal.sig.input_args_with_glib_types();
                let signalname = &signal.sig.name;

                (quote! {
                    pub #signalname: Option<unsafe extern "C" fn(
                        this: *mut #InstanceNameFfi,
                        #inputs
                    ) #output>,
                })
                .to_tokens(tokens);
            }
        }
    }
}

pub struct SlotDeclarations<'ast> {
    pub InstanceNameFfi: Ident,
    pub slots: &'ast [Slot<'ast>],
}

impl<'ast> ToTokens for SlotDeclarations<'ast> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        // ABI: we are generating the imp::FooClass with the parent_class, and the slots to
        // signals/methods. This defines the C ABI for the class structure.

        for slot in self.slots {
            let decl = SlotDeclaration {
                InstanceNameFfi: &self.InstanceNameFfi,
                slot,
            };

            decl.to_tokens(tokens);
        }
    }
}

pub struct ReservedSlotDeclarations(pub usize);

impl ToTokens for ReservedSlotDeclarations {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let num_reserved_slots = self.0;

        (quote! {
            pub reserved_slots: [*const libc::c_void; #num_reserved_slots],
        })
        .to_tokens(tokens);
    }
}
