//! This file contains the instantion of all the data that is instantiated once
//! and stored in a static variable

use crate::gen::boilerplate;
use crate::gen::boilerplateext::BoilerplateExt;
use crate::gen::Test;
use crate::hir::{Override, OverrideItem};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};

use std::marker::PhantomData;

pub struct PrivateStatic<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> {
    boilerplate: &'lt Boilerplate,
    phantom: PhantomData<&'ast ()>,
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> ToTokens
    for PrivateStatic<'lt, 'ast, Boilerplate>
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let gen_struct_def = self.gen_struct_def();
        let gen_priv_def = self.gen_priv_def();
        let gen_vtable_init_parent = self.gen_vtable_init_parent();
        let gen_vtable_init_callbacks = self.gen_vtable_init_callbacks(); // Must be called before properties
        let gen_vtable_init_properties = self.gen_vtable_init_properties();
        let gen_vtable_init_signals = self.gen_vtable_init_signals();
        let gen_vtable_init_offset = self.gen_vtable_init_offset();
        let gen_vtable_init_slots = self.gen_vtable_init_slots();
        let gen_impl_vtable = self.gen_impl_vtable();
        let gen_get_type = self.gen_get_type();
        let gen_test_get_type = Test::new(self.boilerplate);

        tokens.extend(quote! {
            #gen_struct_def

            #gen_priv_def

            #gen_impl_vtable

            impl PrivateStatic
            {
                unsafe fn vtable_init(vtable: glib_ffi::gpointer, _klass_data: glib_ffi::gpointer)
                {
                    #gen_vtable_init_parent
                    #gen_vtable_init_callbacks
                    #gen_vtable_init_properties
                    #gen_vtable_init_signals
                    #gen_vtable_init_offset
                    #gen_vtable_init_slots
                }
            }

            #gen_get_type

            #gen_test_get_type
        })
    }
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>>
    PrivateStatic<'lt, 'ast, Boilerplate>
{
    pub fn new(boilerplate: &'lt Boilerplate) -> Self {
        let phantom = PhantomData;
        PrivateStatic {
            boilerplate,
            phantom,
        }
    }

    /// Generated the code for the private static struct
    pub fn gen_struct_def(&self) -> TokenStream {
        let structname = self.boilerplate.names().class_private();
        let vtable = self.boilerplate.names().vtable();
        let parent_vtable = self.boilerplate.parent_names().vtable();
        let properties_field_len = self.boilerplate.number_new_properties() + 1;
        let properties = if properties_field_len > 1 {
            quote! { properties: [*const gobject_ffi::GParamSpec; #properties_field_len], }
        } else {
            quote! {}
        };
        let names = crate::gen::signals::signal_id_names(self.boilerplate.signals());
        let signal_id_names = &names;
        let slot_declarations = self.boilerplate.slot_declarations();
        let reserved_slot_declarations = self.boilerplate.reserved_slot_declarations();

        quote! {
            #[repr(C)]
            pub struct #vtable {
                pub parent_vtable: #parent_vtable,

                #slot_declarations
                #reserved_slot_declarations
            }

            struct #structname
            {
                parent_vtable: *const #parent_vtable,
                #properties
                #(#signal_id_names: u32),*
            }

            struct PrivateStatic
            {
                // See the comment in get_type_slow() to see why we use an AtomicUsize here
                gtype: AtomicUsize,
                private_offset: i32,
                private: #structname,
            }

        }
    }

    /// Generates the static private variables
    fn gen_priv_def(&self) -> TokenStream {
        let structname = self.boilerplate.names().class_private();
        let names = crate::gen::signals::signal_id_names(self.boilerplate.signals());
        let signal_id_names = &names;
        let properties_field_len = self.boilerplate.number_new_properties() + 1;
        let properties = if properties_field_len > 1 {
            quote! { properties: [0 as *const gobject_ffi::GParamSpec; #properties_field_len], }
        } else {
            quote! {}
        };
        quote! {
            static mut PRIV: PrivateStatic = PrivateStatic {
                gtype: ::std::sync::atomic::AtomicUsize::new(0),
                private_offset: 0i32,
                private: {
                    #structname {
                        #(#signal_id_names: 0u32 ,)*
                        #properties
                        #[allow(unused)]
                        parent_vtable: 0 as _
                } } };
        }
    }

    /// Initialized PRIV.parent_vtable
    fn gen_vtable_init_parent(&self) -> TokenStream {
        let parent_vtable = self.boilerplate.parent_names().vtable();
        quote! {
            {
                PRIV.private.parent_vtable = ::gobject_class::gobject_sys::g_type_class_peek_parent(vtable) as *const #parent_vtable;
            }
        }
    }

    fn gen_vtable_init_callbacks(&self) -> TokenStream {
        let instance_ffi = self.boilerplate.names().instance_ffi();
        match self.boilerplate.parent_names().glib_type() {
            ::glib::Type::Other(_) /* Parent type is a custom object */ |
            ::glib::Type::BaseObject /* Parent type is GObject */ =>
                quote! {
                    {
                        let gobject_class = &mut *(vtable as *mut ::gobject_class::gobject_sys::GObjectClass);
                        gobject_class.finalize = Some(#instance_ffi::finalize);
                        gobject_class.set_property = Some(#instance_ffi::set_property);
                        gobject_class.get_property = Some(#instance_ffi::get_property);
                    }
                },
            _ => quote!{}
        }
    }

    fn gen_vtable_init_offset(&self) -> TokenStream {
        quote! {
            {
                if PRIV.private_offset > 0 {
                    ::gobject_class::gobject_sys::g_type_class_adjust_private_offset(vtable, &mut PRIV.private_offset);
                }
            }
        }
    }

    fn gen_vtable_init_properties(&self) -> TokenStream {
        let properties = self.boilerplate.new_properties();
        // If there are no properties, then there is nothing to install
        if properties.len() == 0 {
            return quote! {};
        }
        match self.boilerplate.fundamental_type() {
            ::glib::Type::BaseObject => crate::gen::properties::properties_registration(properties),
            ::glib::Type::BaseInterface => {
                crate::gen::properties::properties_interface_registration(properties.iter())
            }
            ::glib::Type::BaseParamSpec
            | ::glib::Type::BaseFlags
            | ::glib::Type::BaseEnum
            | ::glib::Type::BaseBoxed => {
                unimplemented!("Properties for this fundamental type is not implemented")
            }
            _ => panic!("Fundamental type is no base type"),
        }
    }

    fn gen_vtable_init_signals(&self) -> TokenStream {
        let signal_declaration = crate::gen::signals::signal_declarations(
            self.boilerplate.names(),
            self.boilerplate.signals(),
        );

        quote! {
            {
                #( #signal_declaration)*
            }
        }
    }

    fn gen_vtable_init_slots(&self) -> TokenStream {
        let vtable = self.boilerplate.names().vtable();
        let slot_assignments =
            crate::gen::imp::slot_assignments(&self.boilerplate.names(), &self.boilerplate.slots());
        let slot_overrides = self.gen_vtable_slot_assignments();

        quote! {
            {
                #[allow(unused)]
                let vtable = &mut *(vtable as *mut #vtable);

                #(#slot_assignments)*
                #(#slot_overrides)*
            }
        }
    }

    fn gen_vtable_slot_assignments(&self) -> Vec<TokenStream> {
        let InstanceNameFfi = self.boilerplate.names().instance_ffi();

        let ret: Vec<TokenStream> = self
            .boilerplate
            .overrides()
            .iter()
            .flat_map(|array| array.iter())
            .filter_map(|overrideitem| {
                if let OverrideItem::Method(Override {
                    ancestor,
                    inner: method,
                }) = overrideitem
                {
                    let name = &method.sig.name;
                    let trampoline_name =
                        self.boilerplate.names().slot_trampoline(&method.sig.name);
                    let ancestor_vtable = ancestor.vtable();

                    Some(quote! {
                        (
                            *(vtable as *mut _ as *mut #ancestor_vtable)
                        ).#name = Some(#InstanceNameFfi::#trampoline_name);
                    })
                } else {
                    None
                }
            })
            .collect();
        ret
    }

    fn gen_impl_vtable(&self) -> TokenStream {
        let vtable = self.boilerplate.names().vtable();
        let callback_guard = crate::glib_utils::glib_callback_guard();

        quote! {
            impl #vtable {
                unsafe extern "C" fn init(vtable: ::gobject_class::glib_sys::gpointer, klass_data: ::gobject_class::glib_sys::gpointer) {
                    #callback_guard

                    PrivateStatic::vtable_init(vtable, klass_data);
                }
            }
        }
    }

    fn gen_get_type_inner(&self) -> TokenStream {
        match self.boilerplate.parent_names().glib_type() {
            ::glib::Type::Other(_) | ::glib::Type::BaseObject => self.gen_get_type_inner_class(),
            ::glib::Type::BaseInterface => self.gen_get_type_inner_interface(),
            _ => unimplemented!("get_type is only implemented for objects and interfaces"),
        }
    }

    fn gen_add_instance_private(&self) -> TokenStream {
        // Only adjust offset if there are private fields
        if !self.boilerplate.has_private() {
            return TokenStream::new();
        }
        let private = self.boilerplate.names().instance_private();

        quote! {
            if mem::size_of::<#private>() > 0 {
                PRIV.private_offset = gobject_ffi::g_type_add_instance_private(
                    g_object_id,
                    mem::size_of::<#private>(),
                );
            }
        }
    }

    fn gen_get_type_inner_class(&self) -> TokenStream {
        let vtable = self.boilerplate.names().vtable();
        let instance = self.boilerplate.names().instance();
        let instance_ffi = self.boilerplate.names().instance_ffi();
        let instance_owned = instance.to_string();
        let instance_name_literal =
            crate::gen::cstringliteral::CStringLiteral(instance_owned.as_str());
        let parent_instance = self.boilerplate.parent_names().instance();
        let add_instance_private = self.gen_add_instance_private();

        quote! {
            fn get_type_inner() -> usize
            {
                let class_size = mem::size_of::<#vtable>();
                assert!(class_size <= u16::MAX as usize);

                let instance_size = mem::size_of::<#instance_ffi>();
                assert!(instance_size <= u16::MAX as usize);

                unsafe {
                    let g_object_id = ::gobject_class::gobject_sys::g_type_register_static_simple(
                        <#parent_instance as ::gobject_class::glib::StaticType>::static_type().to_glib(),
                    #instance_name_literal,
                    class_size as u32,
                    Some(#vtable::init),
                    instance_size as u32,
                    Some(#instance_ffi::init),
                    0
                    );
                    #add_instance_private

                    g_object_id
                }

                // FIXME: add interfaces
            }
        }
    }

    fn gen_get_type_inner_interface(&self) -> TokenStream {
        let vtable = self.boilerplate.names().vtable();
        let instance = self.boilerplate.names().instance();
        let instance_owned = instance.to_string();
        let instance_name_literal =
            crate::gen::cstringliteral::CStringLiteral(instance_owned.as_str());
        quote! {
            fn get_type_inner() -> usize
            {
                let class_size = mem::size_of::<#vtable>();
                assert!(class_size <= u16::MAX as usize);

                unsafe {
                    ::gobject_class::gobject_sys::g_type_register_static_simple(
                        ::gobject_class::gobject_sys::G_TYPE_INTERFACE,
                        #instance_name_literal,
                        class_size as u32,
                        Some(#vtable::init),
                        0, // instance_size
                        None, // instance_init
                        0
                    )
                }
            }
        }
    }

    fn gen_get_type(&self) -> TokenStream {
        let callback_guard = crate::glib_utils::glib_callback_guard();
        let get_type_fn_name = self.boilerplate.names().get_type_fn();
        let get_type_inner = self.gen_get_type_inner();
        quote! {
            #[no_mangle]
            pub unsafe extern "C" fn #get_type_fn_name() -> glib_ffi::GType {
                #callback_guard

                use std::u16;

                fn get_type_slow(atype: &AtomicUsize) -> usize
                {
                    // We want to call g_type_register_static_simple() only once (this is done in
                    // get_type_inner()).  To guarantee this, we avoid expensive locking, or use of
                    // a Once, in the following fashion:
                    //
                    // A GType can have the following values:
                    //   * 0: invalid
                    //   * small integers: Glib's built-in fundamental types
                    //   * large integers: types registered at runtime
                    //
                    // We implement a spinlock with an AtomicUsize that can have the following
                    // values:
                    //
                    //   0 = unlocked, uninitialized
                    //   1 = locked while the GType is being computed
                    //   anything else = unlocked, actual value of the GType
                    return loop {
                        match atype.compare_and_swap(0, 1, Ordering::SeqCst) {
                            0 => {
                                let mut unlock = Unlock(true, atype);
                                let ty = get_type_inner();
                                assert!(ty != gobject_ffi::G_TYPE_INVALID);
                                unlock.0 = false;
                                atype.store(ty, Ordering::SeqCst);
                                break ty
                            },
                            1 => ::std::thread::yield_now(),
                            res => { break res }
                        }
                    };

                    struct Unlock<'a>(bool, &'a AtomicUsize);

                    impl<'a> Drop for Unlock<'a> {
                        fn drop(&mut self) {
                            if self.0 {
                                self.1.store(0, Ordering::SeqCst);
                            }
                        }
                    }
                }

                #get_type_inner

                let res = PRIV.gtype.load(Ordering::SeqCst);
                if res > 1 {
                    res
                } else {
                    get_type_slow(&PRIV.gtype)
                }
            }
        }
    }
}
