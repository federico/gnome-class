use super::boilerplate;
use super::SlotExt;
use crate::gen::boilerplateext::BoilerplateExt;
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use std::marker::PhantomData;

/// This class can generate the public part of the boilerplate code for objects
pub struct Private<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast> + 'lt> {
    boilerplate: &'lt Boilerplate,
    phantom: PhantomData<&'ast ()>,
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> Private<'lt, 'ast, Boilerplate> {
    pub fn new(boilerplate: &'lt Boilerplate) -> Self {
        let phantom = PhantomData;
        Private {
            boilerplate,
            phantom,
        }
    }
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> ToTokens
    for Private<'lt, 'ast, Boilerplate>
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        tokens.extend(self.gen_def_trait_ext_priv());
    }
}

impl<'lt, 'ast: 'lt, Boilerplate: boilerplate::Boilerplate<'ast>> Private<'lt, 'ast, Boilerplate> {
    fn gen_def_trait_ext_priv(&self) -> TokenStream {
        let instance_ext_priv = self.boilerplate.names().instance_ext_priv();
        let name = self.boilerplate.names().instance();
        let ancestors = self
            .boilerplate
            .ancestors()
            .iter()
            .filter(|anc| !anc.is_fundamental())
            .map(|anc| anc.instance());
        let get_priv_def = self.gen_get_priv_def();
        let get_priv_impl = self.gen_get_priv_impl();
        let slot_def = self
            .boilerplate
            .slots()
            .iter()
            .filter(|slot| !slot.public())
            .map(|slot| slot.trait_definition());
        let slot_impl = self
            .boilerplate
            .slots()
            .iter()
            .filter(|slot| !slot.public())
            .map(|slot| slot.trait_implementation(self.boilerplate.names()));
        let signal_emit_methods =
            crate::gen::signals::signal_emit_methods(self.boilerplate.signals());
        let signal_emit_methods_declarations =
            crate::gen::signals::signal_emit_methods_declaration(self.boilerplate.signals());

        quote! {
            trait #instance_ext_priv {
                #get_priv_def

                #(#slot_def)*

                #signal_emit_methods_declarations
            }

            impl<T: IsA<#name> #(+ IsA<#ancestors>)* + IsA<::gobject_class::glib::Object> + ::gobject_class::glib::value::SetValue> #instance_ext_priv for T
            {
                #get_priv_impl

                #(#slot_impl)*

                #signal_emit_methods
            }
        }
    }

    fn gen_get_priv_def(&self) -> TokenStream {
        if !self.boilerplate.has_private() {
            return TokenStream::new();
        }

        let instance_private = self.boilerplate.names().instance_private();

        quote! {
            fn get_priv(&self) -> &#instance_private;
        }
    }

    fn gen_get_priv_impl(&self) -> TokenStream {
        if !self.boilerplate.has_private() {
            return TokenStream::new();
        }

        let instance_private = self.boilerplate.names().instance_private();
        let instance_ffi = self.boilerplate.names().instance_ffi();

        quote! {
            #[allow(dead_code)]
            fn get_priv(&self) -> &#instance_private {
                let glib : *mut #instance_ffi = self.as_ptr() as *mut _;
                unsafe {
                    &*((glib as *const u8).wrapping_offset(PRIV.private_offset as isize) as *const #instance_private)
                }
            }
        }
    }
}
