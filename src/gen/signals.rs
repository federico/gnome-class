use proc_macro2::{Ident, TokenStream};
use quote::quote;
use std::char;
use syn::Path;

use super::super::ident_ext::IdentExt;
use super::cstringliteral::CStringLiteral;
use crate::hir::names::Names;
use crate::hir::{FnArg, Signal, Ty};

pub fn signal_emit_methods_declaration<'ast>(
    signals: impl Iterator<Item = &'ast Signal<'ast>>,
) -> TokenStream {
    let signals = signals.map(|signal| {
        let emit_name = emit_signalname(signal);
        let rust_params = &signal.sig.inputs;
        let rust_return_ty = &signal.sig.output;

        quote! {
            fn #emit_name(#(#rust_params),*) -> #rust_return_ty;
        }
    });
    quote! { #(#signals)* }
}

pub fn signal_emit_methods<'ast>(signals: impl Iterator<Item = &'ast Signal<'ast>>) -> TokenStream {
    let signals = signals.map(|signal| {
        let emit_name = emit_signalname(signal);
        let signal_id_name = signal_id_name(&signal);
        let rust_params = &signal.sig.inputs;
        let rust_return_ty = &signal.sig.output;
        let signal_params = signal.sig.input_args_to_glib_values();
        let return_gtype = signal.sig.output.to_gtype_path();

        let (initialize_return_value, convert_return_value_to_rust) = match rust_return_ty {
            Ty::Unit => (quote! {}, quote! { () }),

            _ => (
                quote! {
                    gobject_sys::g_value_init(ret.to_glib_none_mut().0, #return_gtype);
                },
                quote! {
                    if ret.type_() == glib::Type::Invalid {
                        unreachable!();
                    } else {
                        ret.get().unwrap()
                    }
                },
            ),
        };

        quote! {
            #[allow(unused)]
            fn #emit_name(#(#rust_params),*) -> #rust_return_ty {
                // foo/imp.rs: increment()
                let params: &[glib::Value] = &[
                    #signal_params
                ];

                unsafe {
                    let mut ret = glib::Value::uninitialized();

                    #initialize_return_value

                    gobject_sys::g_signal_emitv(
                        mut_override(params.as_ptr()) as *mut gobject_sys::GValue,
                        PRIV.private.#signal_id_name,
                        0, // detail
                        ret.to_glib_none_mut().0,
                    );

                    #convert_return_value_to_rust
                }
            }
        }
    });
    quote! { #(#signals)* }
}

pub fn signal_id_names<'ast>(signals: impl Iterator<Item = &'ast Signal<'ast>>) -> Vec<Ident> {
    signals.map(|signal| signal_id_name(signal)).collect()
}

pub fn signal_declarations<'ast>(
    names: &Names,
    signals: impl Iterator<Item = &'ast Signal<'ast>>,
) -> Vec<TokenStream> {
    signals
        .map(|signal| {
            // FIXME: we are not passing signal flags
            //
            // FIXME: We are not passing a class_closure, marshaller, etc.

            let get_type_fn_name = names.get_type_fn();
            let signal_id_name = signal_id_name(&signal);

            // FIXME: here we unwrap(); we should check that the
            // signal name is valid during the program checking phase
            let signal_name_str = signal.sig.name.to_owned_string();
            let canonical_signal_name = canonicalize_signal_name(&signal_name_str).unwrap();
            let signal_name_literal = CStringLiteral(&canonical_signal_name);

            assert!(!signal.sig.inputs.is_empty());
            let n_params = (signal.sig.inputs.len() - 1) as u32;

            let param_gtypes: Vec<Path> = signal
                .sig
                .inputs
                .iter()
                .skip(1) // skip &self
                .map(|arg| {
                    if let FnArg::Arg { ref ty, .. } = arg {
                        ty.to_gtype_path()
                    } else {
                        unreachable!();
                    }
                })
                .collect();

            let return_gtype = signal.sig.output.to_gtype_path();

            quote! {
                let param_gtypes = [#(#param_gtypes),*];

                // FIXME: we are using G_SIGNAL_RUN_LAST all the time so that signals
                // with return values will work.  We should do this automatically only
                // for signals with return values, or let the user specify the flags and
                // check that they match gobject's requirements.
                PRIV.private.#signal_id_name =
                    gobject_sys::g_signal_newv (#signal_name_literal,
                                                #get_type_fn_name(),
                                                gobject_sys::G_SIGNAL_RUN_LAST,    // flags
                                                ptr::null_mut(),                   // class_closure
                                                None,                              // accumulator
                                                ptr::null_mut(),                   // accu_data
                                                None,                              // c_marshaller,
                                                #return_gtype,                     // return_type
                                                #n_params,                         // n_params,
                                                mut_override(param_gtypes.as_ptr())
                    );
            }
        })
        .collect()
}

/// From a signal called `foo`, generate `foo_signal_id`.  This is used to
/// store the signal ids from g_signal_newv() in the Class structure.
fn signal_id_name<'ast>(signal: &'ast Signal<'_>) -> Ident {
    Ident::from_str(format!("{}_signal_id", signal.sig.name))
}

/// From a signal called `foo` generate a `foo_trampoline` identifier.  This is used
/// for the functions that get passed to g_signal_connect().
pub fn signal_trampoline_name(signal: &Signal<'_>) -> Ident {
    Ident::from_str(format!("{}_signal_handler_trampoline", signal.sig.name))
}

/// From a signal called `foo` generate a `connect_foo` identifier.  This is used
/// for the public methods in the InstanceExt trait.
pub fn connect_signalname(signal: &Signal<'_>) -> Ident {
    Ident::from_str(format!("connect_{}", signal.sig.name))
}

/// From a signal called `foo` generate a `emit_foo` identifier.  This is used
/// for the user's implementations of methods.
fn emit_signalname(signal: &Signal<'_>) -> Ident {
    Ident::from_str(format!("emit_{}", signal.sig.name))
}

/// Validates a GObject signal name and returns its canonical version
///
/// Per GObject's documentation, a signal name consists of segments
/// consisting of ASCII letters and digits, separated by either the
/// '-' or '_' character.  The first character of a signal name must be
/// a letter.
///
/// This function ensures that the provided signal name is valid,
/// and upon success returns the name with only '-', no '_'; dashes
/// are GObject's preferred internal representation.
pub fn canonicalize_signal_name(name: &str) -> Result<String, ()> {
    if name.len() == 0 {
        return Err(());
    }

    let mut canon = String::with_capacity(name.len());

    for c in name.chars() {
        canon.push(match c {
            'a'..='z' => c,
            'A'..='Z' => char::from_u32(u32::from(c) + u32::from('a') - u32::from('A')).unwrap(),
            _ => return Err(()),
        });

        break;
    }

    for c in name.chars().skip(1) {
        canon.push(match c {
            '0'..='9' => c,
            'a'..='z' => c,
            'A'..='Z' => char::from_u32(u32::from(c) + u32::from('a') - u32::from('A')).unwrap(),
            '_' | '-' => '-',
            _ => return Err(()),
        });
    }

    Ok(canon)
}

pub mod tests {
    use super::*;

    pub fn run() {
        empty_signal_name_is_invalid();
        signal_name_can_only_start_with_a_letter();
        catches_invalid_chars_in_signal_names();
        validates_signal_names();
    }

    fn empty_signal_name_is_invalid() {
        assert!(canonicalize_signal_name("").is_err());
    }

    fn signal_name_can_only_start_with_a_letter() {
        assert!(canonicalize_signal_name("42abc").is_err());
        assert!(canonicalize_signal_name("_abc").is_err());
        assert!(canonicalize_signal_name("-abc").is_err());
    }

    fn catches_invalid_chars_in_signal_names() {
        assert!(canonicalize_signal_name("abc.def").is_err());
        assert!(canonicalize_signal_name("abc$").is_err());
    }

    fn validates_signal_names() {
        assert_eq!(canonicalize_signal_name("foo").unwrap(), "foo");
        assert_eq!(canonicalize_signal_name("foo42").unwrap(), "foo42");
        assert_eq!(canonicalize_signal_name("foo_42").unwrap(), "foo-42");
        assert_eq!(canonicalize_signal_name("foo-42").unwrap(), "foo-42");
    }
}
