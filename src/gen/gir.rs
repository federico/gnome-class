use quote::ToTokens;
use xml::writer::{EmitterConfig, XmlEvent};
use xml::EventWriter;

use crate::gen::writer::GeneratorWrite;
use crate::glib_utils::*;
use crate::hir::names::Names;
use crate::hir::{Class, FnArg, Method, Program, Slot, Ty};

use crate::errors::*;

use xml::writer::Result as EmitterResult;

pub fn generate(program: &Program<'_>) -> Result<()> {
    for class in program.classes.iter() {
        generate_gir(class)?;
    }

    Ok(())
}

pub fn generate_gir(class: &Class<'_>) -> Result<()> {
    let mut writer = GeneratorWrite::from(&class.attrs.gir)?;
    if let Err(err) = generate_gir_xml(class, &mut writer) {
        bail!(class.names.instance(), "{}", err)
    }
    writer.collect_output()
}

fn generate_gir_xml(class: &Class<'_>, f: &mut GeneratorWrite) -> EmitterResult<()> {
    let mut w = EmitterConfig::new().perform_indent(true).create_writer(f);

    let name = class.names.instance().to_string();
    let lower_name = lower_case_instance_name(&name).to_string();
    let names = &class.names;

    // TODO: Get the version and shared-library from Cargo.toml?
    let version = "1.0.0";
    let slib = format!("lib{}.so", lower_name);

    // TODO: Find the parent namespace and class
    let parent_name = "GObject.Object";

    // TODO: include deps like GLib, etc
    // the dep should be `("GLib", "2.0")` lib and version
    let deps: Vec<(&str, &str)> = vec![];

    w.write(
        XmlEvent::start_element("repository")
            .attr("version", "1.2")
            .attr("xmlns", "http://www.gtk.org/introspection/core/1.0")
            .attr("xmlns:c", "http://www.gtk.org/introspection/c/1.0")
            .attr("xmlns:glib", "http://www.gtk.org/introspection/glib/1.0"),
    )?;

    // <include name="GLib" version="2.0"/>
    for (name, version) in deps {
        w.write(
            XmlEvent::start_element("include")
                .attr("name", name)
                .attr("version", version),
        )?;
        w.write(XmlEvent::end_element())?;
    }

    w.write(
        XmlEvent::start_element("namespace")
            .attr("name", &name)
            .attr("c:identifier-prefixes", &name)
            .attr("c:symbol-prefixes", &lower_name)
            .attr("version", &version)
            .attr("shared-library", &slib),
    )?;

    w.write(
        XmlEvent::start_element("class")
            .attr("name", &name)
            .attr("c:type", &name)
            .attr("glib:type-name", &name)
            .attr("c:symbol-prefix", &lower_name)
            .attr("glib:type-struct", &names.vtable().to_string())
            .attr("glib:get-type", &names.get_type_fn().to_string())
            .attr("parent", &parent_name),
    )?;

    gen_constructor_xml(&mut w, &names)?;

    for slot in class.slots.iter() {
        gen_slot_xml(&mut w, &names, slot)?;
    }

    // closing class
    w.write(XmlEvent::end_element())?;

    gen_record_xml(&mut w, &names)?;

    // closing namespace
    w.write(XmlEvent::end_element())?;
    // closing repository
    w.write(XmlEvent::end_element())?;

    Ok(())
}

fn gen_slot_xml(
    w: &mut EventWriter<&mut GeneratorWrite>,
    names: &Names,
    slot: &Slot<'_>,
) -> EmitterResult<()> {
    // <method name="add" c:identifier="counter_add">
    //   <return-value transfer-ownership="none">
    //     <type name="gint" c:type="gint"/>
    //   </return-value>
    //   <parameters>
    //     <parameter name="x" transfer-ownership="none">
    //       <type name="gint" c:type="gint"/>
    //     </parameter>
    //   </parameters>
    // </method>
    // <method name="get" c:identifier="counter_get">
    //   <return-value transfer-ownership="none">
    //     <type name="gint" c:type="gint"/>
    //   </return-value>
    // </method>

    match slot {
        Slot::Method(Method {
            public: true,
            ref sig,
            ..
        }) => {
            let name = sig.name.to_string();
            let identifier = names.exported_fn(&sig.name).to_string();
            let output_ctype = type_to_ctype(&sig.output);
            w.write(
                XmlEvent::start_element("method")
                    .attr("name", &name)
                    .attr("c:identifier", &identifier),
            )?;

            // FIXME, calculate the ownership transfer for pointer types
            w.write(XmlEvent::start_element("return-value").attr("transfer-ownership", "none"))?;
            w.write(
                XmlEvent::start_element("type")
                    .attr("name", &output_ctype)
                    .attr("c:type", &output_ctype),
            )?;
            w.write(XmlEvent::end_element())?; // closing type
            w.write(XmlEvent::end_element())?; // closing return-value

            // TODO: add parameters here
            w.write(XmlEvent::start_element("parameters"))?;
            for param in sig.inputs.iter() {
                if let FnArg::Arg {
                    ref name, ref ty, ..
                } = param
                {
                    let ownership = "none";
                    let ctype = type_to_ctype(ty);

                    w.write(
                        XmlEvent::start_element("parameter")
                            .attr("name", &name.to_string())
                            .attr("transfer-ownership", ownership),
                    )?;

                    w.write(
                        XmlEvent::start_element("type")
                            .attr("name", &ctype)
                            .attr("c:type", &ctype),
                    )?;
                    w.write(XmlEvent::end_element())?; // closing type

                    w.write(XmlEvent::end_element())?; // closing parameter
                }
            }
            w.write(XmlEvent::end_element())?; // closing parameters

            // TODO: add doc here?

            w.write(XmlEvent::end_element())?;
        }
        _ => {
            // VirtualMethod or Signal
        }
    };

    Ok(())
}

fn gen_constructor_xml(
    w: &mut EventWriter<&mut GeneratorWrite>,
    names: &Names,
) -> EmitterResult<()> {
    // <constructor name="new" c:identifier="counter_new">
    //   <return-value transfer-ownership="full">
    //     <type name="Counter" c:type="Counter*"/>
    //   </return-value>
    // </constructor>

    let identifier = names.exported_fn_from_str("new").to_string();
    let name = names.instance().to_string();
    let ctype = format!("{}*", name);
    w.write(
        XmlEvent::start_element("constructor")
            .attr("name", "new")
            .attr("c:identifier", &identifier),
    )?;

    w.write(XmlEvent::start_element("return-value").attr("transfer-ownership", "full"))?;
    w.write(
        XmlEvent::start_element("type")
            .attr("name", &name)
            .attr("c:type", &ctype),
    )?;
    w.write(XmlEvent::end_element())?; // closing type
    w.write(XmlEvent::end_element())?; // closing return-value

    // closing constructor
    w.write(XmlEvent::end_element())?;

    Ok(())
}

fn type_to_ctype(type_: &Ty<'_>) -> String {
    match *type_ {
        Ty::Integer(_) => "gint".to_string(),
        Ty::Unit => "void".to_string(),
        Ty::Char(_) => "gchar*".to_string(),
        Ty::Bool(_) => "gboolean".to_string(),
        // FIXME: These two need to be correctly implemented when support
        Ty::Borrowed(ref t) => format!("{}*", &type_to_ctype(t.as_ref())),
        Ty::Owned(ref t) => t.into_token_stream().to_string(),
    }
}

fn gen_record_xml(w: &mut EventWriter<&mut GeneratorWrite>, names: &Names) -> EmitterResult<()> {
    // <record name="CounterClass"
    //         c:type="CounterClass"
    //         disguised="1"
    //         glib:is-gtype-struct-for="Counter">
    // </record>
    w.write(
        XmlEvent::start_element("record")
            .attr("name", &names.vtable().to_string())
            .attr("disguised", "1")
            .attr("glib:is-gtype-struct-for", &names.instance().to_string()),
    )?;
    w.write(XmlEvent::end_element())?;

    Ok(())
}
