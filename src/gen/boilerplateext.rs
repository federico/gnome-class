use super::boilerplate::Boilerplate;
use crate::gen::ancestorext::AncestorRefExt;
use crate::hir::Property;
use crate::hir::{Method, Signal, Slot, VirtualMethod};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use std::iter::FilterMap;
use std::slice::Iter;

pub trait BoilerplateExt<'ast> {
    fn gen_glib_wrapper(&self) -> TokenStream;
    fn signals<'this>(
        &'this self,
    ) -> FilterMap<Iter<'this, Slot<'ast>>, fn(&'this Slot<'ast>) -> Option<&'this Signal<'ast>>>;
    fn methods<'this>(
        &'this self,
    ) -> FilterMap<Iter<'this, Slot<'ast>>, fn(&'this Slot<'ast>) -> Option<&'this Method<'ast>>>;
    fn virtual_methods<'this>(
        &'this self,
    ) -> FilterMap<
        Iter<'this, Slot<'ast>>,
        fn(&'this Slot<'ast>) -> Option<&'this VirtualMethod<'ast>>,
    >;
    fn number_new_properties(&self) -> usize;
    fn new_properties<'this>(&'this self) -> &'this [Property<'this>]
    where
        'ast: 'this;
    fn override_properties<'this>(&'this self) -> &'this [Property<'this>]
    where
        'ast: 'this;

    fn to_token_stream(&self) -> TokenStream;
}

impl<'ast, T: Boilerplate<'ast>> BoilerplateExt<'ast> for T {
    fn signals<'this>(
        &'this self,
    ) -> FilterMap<Iter<'this, Slot<'ast>>, fn(&'this Slot<'ast>) -> Option<&'this Signal<'ast>>>
    {
        self.slots().iter().filter_map(|slot| match *slot {
            Slot::Signal(ref s) => Some(s),
            _ => None,
        })
    }

    fn methods<'this>(
        &'this self,
    ) -> FilterMap<Iter<'this, Slot<'ast>>, fn(&'this Slot<'ast>) -> Option<&'this Method<'ast>>>
    {
        self.slots().iter().filter_map(|slot| match *slot {
            Slot::Method(ref s) => Some(s),
            _ => None,
        })
    }

    fn virtual_methods<'this>(
        &'this self,
    ) -> FilterMap<
        Iter<'this, Slot<'ast>>,
        fn(&'this Slot<'ast>) -> Option<&'this VirtualMethod<'ast>>,
    > {
        self.slots().iter().filter_map(|slot| match *slot {
            Slot::VirtualMethod(ref s) => Some(s),
            _ => None,
        })
    }

    fn gen_glib_wrapper(&self) -> TokenStream {
        let instance_name = self.names().instance();
        let class_name = self.names().class();
        let instance_name_ffi = self.names().instance_ffi();
        let class_name_ffi = self.names().vtable();
        let get_type_fn_name = self.names().get_type_fn();
        let parent_instance_tokens = if self.parent_names().is_fundamental() {
            TokenStream::new()
        } else if self.fundamental_type() == glib::Type::BaseInterface {
            unimplemented!("deriving from interfaces")
        } else {
            let mut has_ancestors = false;
            let ancestors = self.ancestors().iter().map(move |anc| {
                has_ancestors = true;
                anc.to_glib_wrapper_tokens().into_token_stream()
            });
            if self.ancestors().len() > 0 {
                quote! { @extends #(#ancestors),* }
            } else {
                quote! {}
            }
        };

        quote! {
            use ::gobject_class::glib_sys as glib_ffi;
            use ::gobject_class::gobject_sys as gobject_ffi;

            use ::gobject_class::glib;
            use ::gobject_class::glib::glib_wrapper;
            use ::gobject_class::glib::glib_object_wrapper;

            use ::gobject_class::libc;

            #[allow(unused_imports)]
            use ::gobject_class::glib::IsA;

            use ::gobject_class::glib::translate::*;
            use std::mem;
            use std::ptr;

            #[allow(unused_imports)]
            use super::*;

            glib_wrapper! {
                pub struct #instance_name(Object<imp::#instance_name_ffi, imp::#class_name_ffi, #class_name>)
                    #parent_instance_tokens;

                match fn {
                    get_type => || imp::#get_type_fn_name(),
                }
            }
        }
    }

    /// Generates the complete module and code for a class or interface
    fn to_token_stream(&self) -> TokenStream {
        let ModuleName = self.names().module();

        // FIXME normalize this function names, I'm sure we can find better names for this
        // variables and functions
        // let properties_enum = properties::properties_enum(&self.iface.properties);

        let glib_wrapper = self.gen_glib_wrapper();
        let private_static = crate::gen::privatestatic::PrivateStatic::new(self);
        let instance = crate::gen::instance::Instance::new(self);
        let public = crate::gen::public::Public::new(self);
        let public_imp = crate::gen::public::PublicImp::new(self);
        let instance_ext = self.names().instance_ext();
        let private = crate::gen::private::Private::new(self);

        quote! {
            #[allow(non_snake_case)] // "oddly" named module above
            pub mod #ModuleName {
                #glib_wrapper

                pub mod imp {
                    // Bring in our grandparent's stuff so the user's
                    // implementation can use what they had already defined
                    // there. Note that this isn't guaranteed to get used though
                    // so stick an #[allow] on it
                    #[allow(unused_imports)]
                    use super::super::*;

                    use super::glib_ffi;
                    use super::gobject_ffi;
                    use std::sync::atomic::{AtomicUsize,Ordering};

                    #[allow(unused_imports)]
                    use super::libc;

                    #[allow(unused_imports)]
                    use ::gobject_class::glib::translate::*;
                    use ::gobject_class::glib::object::ObjectExt;
                    #[allow(unused_imports)]
                    use ::gobject_class::glib::IsA;

                    #[allow(unused_imports)]
                    use std::ffi::CString;

                    use std::mem;
                    use std::ptr;

                    #instance

                    #private_static

                    #public_imp

                    #private
                }

                #public


            }

            pub use self::#ModuleName::*;
            pub use self::#ModuleName::imp::#instance_ext;
        }
    }

    /// This function returns the number of new properties
    fn number_new_properties(&self) -> usize {
        let properties = self.properties();
        properties
            .iter()
            .position(|prop| prop.is_override)
            .unwrap_or(properties.len())
    }

    fn new_properties<'this>(&'this self) -> &'this [Property<'this>]
    where
        'ast: 'this,
    {
        &self.properties()[..self.number_new_properties()]
    }

    fn override_properties<'this>(&'this self) -> &'this [Property<'this>]
    where
        'ast: 'this,
    {
        &self.properties()[self.number_new_properties()..]
    }
    // fn gen_boilerplate(&self) -> TokenStream
    // {
    // class
    // quote! {
    // #[allow(non_snake_case)] // "oddly" named module above
    // pub mod #ModuleName {
    // #glib_wrapper
    //
    // pub mod imp {
    // Bring in our grandparent's stuff so the user's
    // implementation can use what they had already defined
    // there. Note that this isn't guaranteed to get used though
    // so stick an #[allow] on it
    // #[allow(unused_imports)]
    // use super::super::*;
    //
    // use super::glib;
    // use super::glib_ffi;
    // use super::gobject_ffi;
    // use super::libc;
    //
    // use std::mem;
    // use std::ptr;
    //
    // #[allow(unused_imports)]
    // use glib::translate::*;
    //
    // #[allow(unused_imports)]
    // use std::ffi::CString;
    //
    // use std::sync::atomic::{AtomicUsize,Ordering};
    //
    // #instance
    //
    // #struct_vtable
    //
    // properties enum
    // #properties_enum
    //
    // #private_static
    //
    // #(#imp_extern_methods)*
    //
    // }
    //
    // impl #InstanceName {
    // FIXME: we should take construct-only arguments and other convenient args to new()
    // pub fn new() -> #InstanceName {
    // unsafe { from_glib_full(imp::#imp_new_fn_name(/* FIXME: args */)) }
    // }
    // }
    //
    // pub trait #InstanceExt {
    // #(#slot_trait_fns)*
    //
    // #(#get_properties_fn)*
    //
    // #(#set_properties_fn)*
    // }
    //
    // impl<O: IsA<#InstanceName> + IsA<glib::object::Object> + glib::object::ObjectExt>
    // #InstanceExt for O { #(#slot_trait_impls)*
    //
    // #(#get_properties)*
    //
    // #(#set_properties)*
    // }
    //
    // }
    //
    // pub use #ModuleName::*;
    // }
    //
    // interface
    // quote! {
    // #[allow(non_snake_case)] // "oddly" named module above
    // pub mod #ModuleName {
    // #glib_wrapper
    //
    // pub mod imp {
    // use super::glib_ffi;
    // use super::gobject_ffi;
    // use std::sync::atomic::{AtomicUsize,Ordering};
    //
    // use super::libc;
    //
    // #[allow(unused_imports)]
    // use glib::translate::*;
    //
    // use std::mem;
    // use std::ptr;
    //
    // #struct_instance
    //
    // #struct_vtable
    //
    // properties enum
    // #properties_enum
    //
    // #instance
    //
    // #private_static
    //
    // #(#imp_extern_methods)*
    // }
    //
    // pub trait #InstanceExt {
    // #(#slot_trait_fns)*
    //
    // FIXME #(#get_properties_fn)*
    //
    // FIXME #(#set_properties_fn)*
    // }
    //
    // FIXME: some gtk-rs/gtk ifaces have IsA<glib::object::Object> and some don't;
    // is that due to interface prerequisites?
    // impl<O: IsA<#InstanceName> + IsA<glib::object::Object>> #InstanceExt for O {
    // #(#slot_trait_impls)*
    //
    // FIXME #(#get_properties)*
    //
    // FIXME #(#set_properties)*
    // }
    //
    // }
    //
    // pub use #ModuleName::*;
    // }
    // }
}
