use crate::hir::names::ParentNames;
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};

pub trait AncestorRefExt {
    type GlibWrapperTokens: ToTokens;

    fn to_glib_wrapper_tokens(&self) -> Self::GlibWrapperTokens;
}

pub struct AncestorExtGlibWrapperTokens<'this>(&'this ParentNames);

impl<'this> ToTokens for AncestorExtGlibWrapperTokens<'this> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let rust_type = self.0.instance();
        if let Some(ffi_type) = self.0.explicit_ffi() {
            tokens.extend(quote! { #rust_type => #ffi_type });
        } else {
            rust_type.to_tokens(tokens);
        }
    }
}

impl<'this> AncestorRefExt for &'this ParentNames {
    type GlibWrapperTokens = AncestorExtGlibWrapperTokens<'this>;

    fn to_glib_wrapper_tokens(&self) -> AncestorExtGlibWrapperTokens<'this> {
        AncestorExtGlibWrapperTokens(self)
    }
}
