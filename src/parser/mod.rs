use crate::proc_macro;

use proc_macro2::TokenStream;
use syn::parse::{Parse, ParseStream, Result};
use syn::{self, braced, parenthesized, Token};

use crate::ast;

pub mod keywords {
    use syn::custom_keyword;

    custom_keyword!(class);
    custom_keyword!(interface);
    custom_keyword!(property);
    custom_keyword!(signal);
    custom_keyword!(T);
    custom_keyword!(reserve_slots);
    custom_keyword!(get);
    custom_keyword!(set);
}

pub fn parse_program(token_stream: proc_macro::TokenStream) -> Result<ast::Program> {
    syn::parse(token_stream).map_err(|e| e.into())
}

impl Parse for ast::Program {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        Ok(ast::Program {
            items: ast::Item::parse_outer(input)?,
        })
    }
}

impl Parse for ast::Item {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let attrs = syn::Attribute::parse_outer(input)?;
        let lookahead = input.lookahead1();
        if lookahead.peek(keywords::class) {
            input.parse().map(|mut res: ast::Class| {
                res.attrs = attrs;
                ast::Item::Class(res)
            })
        } else if lookahead.peek(Token![impl]) {
            input.parse().map(ast::Item::Impl)
        } else if lookahead.peek(keywords::interface) {
            input.parse().map(|mut res: ast::Interface| {
                res.attrs = attrs;
                ast::Item::Interface(res)
            })
        } else {
            Err(lookahead.error())
        }
    }
}

impl ast::Item {
    fn parse_outer(input: ParseStream<'_>) -> Result<Vec<Self>> {
        let mut res = Vec::new();
        while !input.is_empty() {
            res.push(input.parse()?);
        }
        Ok(res)
    }
}

impl Parse for ast::Class {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let class = input.parse()?;
        let name = input.parse()?;
        let colon: Option<Token![:]> = input.parse()?;
        let extends = if colon.is_some() {
            Some(input.parse()?)
        } else {
            None
        };
        let fields = input.parse()?;
        Ok(ast::Class {
            attrs: Vec::new(),
            class,
            name,
            colon,
            extends,
            fields,
        })
    }
}

impl Parse for ast::Interface {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let content;
        Ok(ast::Interface {
            attrs: Vec::new(),
            interface: input.parse()?,
            name: input.parse()?,
            brace: braced!(content in input),
            items: content.call(ast::ImplItem::parse_outer)?,
        })
    }
}

impl Parse for ast::Impl {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        input.parse::<Token![impl]>()?;
        let trait_ = if input.peek2(Token![for]) {
            let trait_ = input.parse()?;
            input.parse::<Token![for]>()?;
            Some(trait_)
        } else {
            None
        };
        let self_path = input.parse()?;
        let mut items = Vec::new();
        let content;
        braced!(content in input);
        while !content.is_empty() {
            items.push(content.parse()?);
        }

        Ok(ast::Impl {
            trait_,
            self_path,
            items,
        })
    }
}

impl Parse for ast::ImplItem {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        Ok(ast::ImplItem {
            attrs: input.call(syn::Attribute::parse_outer)?,
            node: input.parse()?,
        })
    }
}

impl ast::ImplItem {
    fn parse_outer(input: ParseStream<'_>) -> Result<Vec<Self>> {
        let mut res = Vec::new();
        while !input.is_empty() {
            res.push(input.parse()?);
        }
        Ok(res)
    }
}

impl Parse for ast::ImplItemKind {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let lookahead = input.lookahead1();
        if lookahead.peek(keywords::property) {
            input.parse().map(ast::ImplItemKind::Prop)
        } else if lookahead.peek(keywords::reserve_slots) {
            input.parse::<keywords::reserve_slots>()?;
            let content;
            let paren = parenthesized!(content in input);
            Ok(ast::ImplItemKind::ReserveSlots(paren, content.parse()?))
        } else if lookahead.peek(Token![type]) {
            input.parse().map(ast::ImplItemKind::TraitType)
        } else {
            input.parse().map(ast::ImplItemKind::Method)
        }
    }
}

impl Parse for ast::ImplItemMethodBlock {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        if input.peek(Token![;]) {
            input.parse().map(ast::ImplItemMethodBlock::Empty)
        } else {
            input.parse().map(ast::ImplItemMethodBlock::Block)
        }
    }
}

impl Parse for ast::ImplItemMethod {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let content;
        let public: Option<Token![pub]> = input.parse()?;
        let virtual_ = input.parse()?;
        let signal = input.parse()?;
        let fn_ = input.parse()?;
        let name = input.parse()?;
        let parem_token = parenthesized!(content in input);
        let inputs = content.parse_terminated(syn::FnArg::parse)?;
        let output = input.parse()?;
        let body = input.parse()?;
        Ok(ast::ImplItemMethod {
            public,
            virtual_,
            signal,
            fn_,
            name,
            parem_token,
            inputs,
            output,
            body,
        })
    }
}

impl Parse for ast::ImplProp {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let content;
        Ok(ast::ImplProp {
            attributes: input.call(syn::Attribute::parse_inner)?,
            property: input.parse()?,
            name: input.parse()?,
            colon1: input.parse()?,
            t1: input.parse()?,
            where_: input.parse()?,
            t2: input.parse()?,
            colon2: input.parse()?,
            type_: input.parse()?,
            braces: braced!(content in input),
            items: content.call(ast::ImplPropBlock::parse_outer)?,
        })
    }
}

impl Parse for ast::ImplPropBlock {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let lookahead = input.lookahead1();
        if lookahead.peek(keywords::get) {
            input.parse().map(ast::ImplPropBlock::Getter)
        } else if lookahead.peek(keywords::set) {
            input.parse().map(ast::ImplPropBlock::Setter)
        } else if lookahead.peek(keywords::set) {
            input.parse().map(ast::ImplPropBlock::Default)
        } else {
            Err(lookahead.error())
        }
    }
}

impl ast::ImplPropBlock {
    fn parse_outer(input: ParseStream<'_>) -> Result<Vec<Self>> {
        let mut res = Vec::new();
        while !input.is_empty() {
            res.push(input.parse()?);
        }
        Ok(res)
    }
}

impl Parse for ast::ImplPropGetter {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let content;
        Ok(ast::ImplPropGetter {
            get: input.parse()?,
            paren: parenthesized!(content in input),
            and: content.parse()?,
            self_: content.parse()?,
            rarrow: input.parse()?,
            t: input.parse()?,
            block: input.parse()?,
        })
    }
}

impl Parse for ast::ImplPropSetter {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let content;
        Ok(ast::ImplPropSetter {
            set: input.parse()?,
            paren: parenthesized!(content in input),
            amp: content.parse()?,
            self_: content.parse()?,
            comma: content.parse()?,
            param: content.parse()?,
            colon: content.parse()?,
            t: content.parse()?,
            block: input.parse()?,
        })
    }
}

impl Parse for ast::ImplPropDefault {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let _content;
        Ok(ast::ImplPropDefault {
            default: input.parse()?,
            paren: parenthesized!(_content in input),
            rarrow: input.parse()?,
            self_: input.parse()?,
            block: input.parse()?,
        })
    }
}

pub mod tests {
    use super::super::ident_ext::IdentExt;
    use super::*;
    use quote::ToTokens;
    use syn::parse_str;

    pub fn run() -> Result<()> {
        parses_class_with_no_superclass()?;
        parses_class_with_superclass()?;
        parses_class_item()?;
        parses_plain_impl_item()?;
        parses_impl_item_with_trait()?;
        parses_class_with_private_field()?;
        parses_interface()?;
        Ok(())
    }

    fn assert_tokens_equal<T: ToTokens>(x: &T, s: &str) {
        let mut tokens = TokenStream::new();
        x.to_tokens(&mut tokens);
        assert_eq!(tokens.to_string(), s);
    }

    fn parses_class_with_no_superclass() -> Result<()> {
        let raw = "class Foo {}";
        let class = parse_str::<ast::Class>(raw)?;

        assert_eq!(class.name.to_owned_string(), "Foo");
        assert!(class.extends.is_none());
        Ok(())
    }

    fn parses_class_with_private_field() -> Result<()> {
        let raw = "class Foo {
          foo : u32,
          bar : u32,
          baz : u32
        }";
        let class = parse_str::<ast::Class>(raw)?;

        assert_eq!(class.fields.named.len(), 3);
        Ok(())
    }

    fn parses_class_with_superclass() -> Result<()> {
        let raw = "class Foo: Bar {}";
        let class = parse_str::<ast::Class>(raw)?;

        assert_eq!(class.name.to_owned_string(), "Foo");
        assert_tokens_equal(&class.extends, "Bar");
        Ok(())
    }

    fn parses_class_item() -> Result<()> {
        let raw = "class Foo {}";
        let item = parse_str::<ast::Item>(raw)?;

        if let ast::Item::Class(class) = item {
            assert_eq!(class.name.to_owned_string(), "Foo");
            assert!(class.extends.is_none());
        } else {
            unreachable!();
        }
        Ok(())
    }

    fn test_parsing_impl_item(raw: &str, trait_name: Option<&str>, self_name: &str) -> Result<()> {
        let item = parse_str::<ast::Item>(raw)?;

        if let ast::Item::Impl(ref impl_) = item {
            if let Some(ref trait_path) = impl_.trait_ {
                assert_tokens_equal(&trait_path, trait_name.as_ref().unwrap());
            } else {
                assert!(trait_name.is_none());
            }

            assert_tokens_equal(&impl_.self_path, self_name);
        } else {
            unreachable!();
        }
        Ok(())
    }

    fn parses_plain_impl_item() -> Result<()> {
        test_parsing_impl_item("impl Foo {}", None, "Foo")
    }

    fn parses_impl_item_with_trait() -> Result<()> {
        test_parsing_impl_item("impl Foo for Bar {}", Some("Foo"), "Bar")
    }

    fn parses_interface() -> Result<()> {
        let raw = "interface Foo { virtual fn bar(&self); }";
        let iface = parse_str::<ast::Interface>(raw)?;

        assert_eq!(iface.name.to_owned_string(), "Foo");
        Ok(())
    }
}
