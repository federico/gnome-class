use crate::ast::*;
use crate::errors::*;

pub fn check_program(program: &Program) -> Result<()> {
    for class in get_program_classes(program) {
        check_class(class)?;
    }
    Ok(())
}

fn check_class(class: &Class) -> Result<()> {
    Ok(check_class_items(class)?)
}

fn check_class_items(_class: &Class) -> Result<()> {
    Ok(())
}

pub mod tests {
    use super::*;

    use crate::ast;

    pub fn run() -> Result<()> {
        checks_empty_class()
    }

    fn checks_empty_class() -> Result<()> {
        let raw = "class Foo {}";

        let program: ast::Program = ::syn::parse_str(raw)?;

        check_program(&program)
    }
}
