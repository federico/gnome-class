use syn::{Path, Type};

pub struct Ancestor<'ast> {
    rust_type: &'ast Path,
    ffi_type: Option<&'ast Type>,
}

impl<'ast> Ancestor<'ast> {
    pub fn new(rust_type: &'ast Path, ffi_type: Option<&'ast Type>) -> Self {
        Ancestor {
            rust_type,
            ffi_type,
        }
    }

    pub fn rust_type(&self) -> &'ast Path {
        self.rust_type
    }

    pub fn ffi_type(&self) -> Option<&'ast Type> {
        self.ffi_type
    }
}
