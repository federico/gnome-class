use syn::{Ident, Path, Type};

use crate::glib_utils::*;

use super::super::ident_ext::IdentExt;
use std::iter::FromIterator;

macro_rules! type_path {
    { $($ident:ident)::* } => {
        ::syn::Type::Path(
            ::syn::TypePath {
                qself: None,
                path: type_path!(@path $($ident)::*)
            }
        )
    };
    { @path $($ident:ident)::* } => {
        ::syn::Path {
            leading_colon: None,
            segments: ::syn::punctuated::Punctuated::from_iter(vec![
                $(::syn::PathSegment::from(Ident::new(stringify!($ident), ::proc_macro2::Span::call_site()))),*
            ])}
    };
    { @count } => { 0 };
    { @count $a:ident } => { 1 };
    { @count $a:ident::$($b:ident)::*} => { 1 + type_path!{ @count $($b)::* }};
    { @innerpath $a:ident::$($b:ident)::* } => { type_path!( @path $a::$($b)::*) };
    { @innerpath $a:ident } => { $a };
    { <$($path:ident)::* as $($a:ident)::*>::$($b:ident)::* } => {
        ::syn::Type::Path(
            ::syn::TypePath {
                qself: Some(::syn::QSelf {
                    ty: Box::new(::syn::Type::Path(::syn::TypePath { qself: None, path: type_path!( @innerpath $($path)::* ) })),
                    position: type_path!( @count $($a)::* ),
                    lt_token: Default::default(),
                    gt_token: Default::default(),
                    as_token: Default::default(),
                }),
                path: type_path!(@path $($a)::*::$($b)*)
            }
        )
    };
    //<$($inner:tt)*>::$($ident:id)::* => {}
}

/// Utilities to generate names useful for code generation of a GObject
///
/// The user of gnome-class provides a single name for a class or an interface,
/// say `Foo`.  If this is a class and has a method name `bar()`, we need to be able to
/// generate identifier names like the following:
///
/// * `FooClass` - the class structure with the methods/signals vtable
/// * `FooFfi` - the instance structure with `GObject` as its first field
/// * `foo_bar()` - public C-callable function for the `Foo::bar()` method
/// * `foo_bar_impl()` - internal wrapper over the user-supplied implementation of that method
///
/// All these names can be generated by the various functions that `Names` provides.
pub struct Names {
    // User-supplied name of the instance, e.g. `Foo`
    instance: Ident,

    // Suffix to append for `FooClass` or `FooIface`
    vtable_suffix: &'static str,
}

impl<'s> PartialEq<&'s str> for Names {
    fn eq(&self, other: &&'s str) -> bool {
        self.instance == other
    }
}

impl Names {
    /// Creates a new struct full names useful for code generation of a GObject
    ///
    /// `instance` is the user-provided name for the GObject
    ///
    /// `vtable_suffix` can be something like `"Class"` or `"Iface"`; it will get appended
    /// to the `instance` name.
    pub fn new(instance: &Ident, vtable_suffix: &'static str) -> Names {
        Names {
            instance: instance.clone(),
            vtable_suffix,
        }
    }

    /// Returns the name of the public instance struct for a GObject
    pub fn instance(&self) -> Ident {
        self.instance.clone()
    }

    /// Returns the name of the public class struct for a GObject
    pub fn class(&self) -> Ident {
        self.instance.with_suffix("Class")
    }

    /// Returns the name of the class or interface struct
    pub fn vtable(&self) -> Ident {
        self.instance.with_suffix(&self.vtable_suffix)
    }

    /// Returns the name of the module in which the generated code should live
    pub fn module(&self) -> Ident {
        self.instance.with_suffix("Mod")
    }

    /// Returns the name of the Ffi instance struct
    pub fn instance_ffi(&self) -> Ident {
        self.instance.with_suffix("Ffi")
    }

    /// Returns the name of the instance-private struct
    pub fn instance_private(&self) -> Ident {
        self.instance.with_suffix("Priv")
    }

    /// Returns the name of the class-private struct
    pub fn class_private(&self) -> Ident {
        self.instance.with_suffix("ClassPrivate")
    }

    /// Returns the name of the public trait with method names
    pub fn instance_ext(&self) -> Ident {
        self.instance.with_suffix("Ext")
    }

    /// Returns the name of the private trait with method names
    pub fn instance_ext_priv(&self) -> Ident {
        self.instance.with_suffix("ExtPriv")
    }

    /// Returns the name of a C-callable function name for a method
    ///
    /// If the `Names` was created with an instance name of "FooBar",
    /// and `method_name` is `frob`, this generates the identifier
    /// `foo_bar_frob`.
    pub fn exported_fn(&self, method_name: &Ident) -> Ident {
        Ident::from_str(format!(
            "{}_{}",
            lower_case_instance_name(self.instance.to_owned_string().as_str()),
            method_name
        ))
    }

    /// Returns the name of a C-callable function name for a method
    ///
    /// Similar to `exported_fn()`, but takes a `&str` for the method
    /// name instead of an `&Ident`.
    pub fn exported_fn_from_str(&self, method_name: &str) -> Ident {
        Ident::from_str(format!(
            "{}_{}",
            lower_case_instance_name(self.instance.to_owned_string().as_str()),
            method_name
        ))
    }

    /// Returns the name of the `foo_get_type()` function for GType
    pub fn get_type_fn(&self) -> Ident {
        self.exported_fn_from_str("get_type")
    }

    /// Given a slot name (method or signal), returns its trampoline name
    pub fn slot_trampoline(&self, slot_name: &Ident) -> Ident {
        Ident::from_str(format!("{}_slot_trampoline", slot_name))
    }

    /// Given a slot name (method or signal), returns its method implementation name
    pub fn slot_impl(&self, slot_name: &Ident) -> Ident {
        Ident::from_str(format!("{}_impl", slot_name))
    }

    /// Returns a span which can be used to generate an error
    pub fn span(&self) -> ::proc_macro2::Span {
        self.instance.span()
    }
}

pub enum ParentNames {
    Object {
        instance: Path,
        instance_ffi: Option<Type>,
        class_ffi: Option<Path>,
    },
    GType(::glib::Type),
}

impl ParentNames {
    /// Creates a new ParentNames object for a parent name from the same crate as current
    pub fn new(instance: Path) -> Self {
        ParentNames::Object {
            instance,
            instance_ffi: None,
            class_ffi: None,
        }
    }

    /// Creates a new parent with from a rust-name and a ffi-name.
    /// This is neccesairy to avoid conflicting implementations of instance is from an extern crate
    pub fn new_with_explicit_ffi(instance: Path, instance_ffi: Option<Type>) -> Self {
        ParentNames::Object {
            instance,
            instance_ffi,
            class_ffi: None,
        }
    }

    /// Create a ParentNames instance from a gtype.
    /// panics if gtype isn't a base object type
    pub fn new_from_glib(gtype: ::glib::Type) -> Self {
        match gtype {
            ::glib::Type::BaseInterface | ::glib::Type::BaseObject => ParentNames::GType(gtype),
            ::glib::Type::BaseBoxed
            | ::glib::Type::BaseEnum
            | ::glib::Type::BaseFlags
            | ::glib::Type::BaseParamSpec => unimplemented!(
                "Other base types then objects and interfaces are not yet implemented"
            ),
            _ => panic!("Unsupported base type {}", gtype),
        }
    }

    /// Returns the rust-name of the instance of this parent
    pub fn instance(&self) -> Path {
        match self {
            ParentNames::Object { ref instance, .. } => instance.clone(),
            ParentNames::GType(::glib::Type::BaseObject) => {
                type_path! { @path glib::Object }
            }
            ParentNames::GType(gtype) => ::syn::parse2(crate::glib_utils::tokens_glib_type(&gtype))
                .expect("Valid conversion from gtype to type"),
        }
    }

    fn path_to_type(path: Path) -> Type {
        ::syn::Type::Path(::syn::TypePath { qself: None, path })
    }

    /// Gives the C-type of this object. Not all types have a base object, such as interfaces.
    pub fn instance_ffi(&self) -> Type {
        match self {
            ParentNames::Object {
                ref instance,
                ref instance_ffi,
                ..
            } => {
                if let Some(instance_ffi) = instance_ffi {
                    instance_ffi.clone()
                } else {
                    let instance: Path = instance.clone();
                    type_path!(<instance as glib::object::ObjectType>::GlibType)
                }
            }
            ParentNames::GType(::glib::Type::BaseObject) => {
                // If the parent is a BaseObject, then we can use the ::glib::Object type
                type_path!(<glib::Object as glib::object::ObjectType>::GlibType)
            }
            ParentNames::GType(::glib::Type::BaseInterface) => {
                // If the parent is a BaseObject, then we can use the ::glib::Object type
                type_path!(libc::c_void)
            }
            ParentNames::GType(gtype) => {
                unimplemented!("No rust type for fundamental type {}", gtype);
            }
        }
    }

    /// Returns the vtable type of the object
    pub fn vtable(&self) -> Type {
        match self {
            ParentNames::Object {
                ref instance,
                ref class_ffi,
                ..
            } => {
                if let Some(class_ffi) = class_ffi {
                    ::syn::Type::Path(::syn::TypePath {
                        qself: None,
                        path: class_ffi.clone(),
                    })
                } else {
                    let instance: Path = instance.clone();
                    type_path!(<instance as glib::object::ObjectType>::GlibClassType)
                }
            }
            ParentNames::GType(::glib::Type::BaseObject) => {
                // If the parent is a BaseObject, then we can use the ::glib::Object type
                type_path!(<glib::Object as glib::object::ObjectType>::GlibClassType)
            }
            ParentNames::GType(::glib::Type::BaseInterface) => {
                type_path!(gobject_ffi::GTypeInterface)
            }
            ParentNames::GType(gtype) => panic!("No rust type for fundamental type {}", gtype),
        }
    }

    /// Returns true if the type is a fundamental type.
    /// Because is a parent name, it must be base-type
    pub fn is_fundamental(&self) -> bool {
        match self {
            ParentNames::GType(_) => true,
            _ => false,
        }
    }

    /// This function returns the glib-type.
    /// If the type is a custom object, then Other(0) is returned,
    /// because we do know the type is other, but we don't know
    /// the object number at compiler time.
    pub fn glib_type(&self) -> ::glib::Type {
        match self {
            ParentNames::GType(gtype) => gtype.clone(),
            _ => ::glib::Type::Other(0),
        }
    }

    pub fn explicit_ffi(&self) -> Option<&Type> {
        match self {
            ParentNames::Object { instance_ffi, .. } => instance_ffi.as_ref(),
            _ => None,
        }
    }
}

trait WithSuffix: ::std::fmt::Display {
    fn with_suffix(&self, suffix: &str) -> Ident {
        Ident::from_str(format!("{}{}", self, suffix))
    }
}

impl WithSuffix for Ident {}
