// High-level Internal Representation of GObject artifacts
//
// Here we provide a view of the world in terms of what GObject knows:
// classes, interfaces, signals, etc.
//
// We construct this view of the world from the raw Abstract Syntax
// Tree (AST) from the previous stage.

mod ancestor;
mod associated_types;
mod class;
pub mod generatorattrs;
pub mod names;
mod property;

use std::collections::HashMap;
use std::rc::Rc;

use proc_macro2::{Ident, Span, TokenStream};
use quote::quote;
use quote::ToTokens;
use syn::punctuated::Punctuated;
use syn::{self, parse_str, Block, LitStr, Path, ReturnType, Token};

use self::generatorattrs::GeneratorAttributes;
use self::names::{Names, ParentNames};
use super::ast;
use super::checking::*;
use super::errors::*;
use super::ident_ext::IdentExt;

pub use self::ancestor::Ancestor;
pub use self::class::{Class, Classes};
pub use self::property::Property;

pub struct Program<'ast> {
    pub classes: Classes<'ast>,
    pub interfaces: Interfaces<'ast>,
}

pub struct Interfaces<'ast> {
    items: HashMap<Ident, Interface<'ast>>,
}

pub struct GenerateFile {
    path: LitStr,
}

enum GenerateType {
    Gir,
    H,
    Rust,
}

pub struct Interface<'ast> {
    pub attrs: GeneratorAttributes, // Interpreted attributes
    pub names: Names,               // Foo

    // The order of these is important; it's the order of the slots in FooIface
    pub slots: Vec<Slot<'ast>>,
    pub properties: Vec<Property<'ast>>,
    pub ancestors: Vec<ParentNames>,
}

pub enum Slot<'ast> {
    Method(Method<'ast>),
    VirtualMethod(VirtualMethod<'ast>),
    Signal(Signal<'ast>),
}

pub struct Override<T> {
    pub ancestor: Rc<ParentNames>,
    pub inner: T,
}

pub enum OverrideItem<'ast> {
    Method(Override<Method<'ast>>),
    Property(Override<Property<'ast>>),
}

pub struct Method<'ast> {
    pub public: bool,
    pub sig: FnSig<'ast>,
    pub body: &'ast Block,
}

pub struct VirtualMethod<'ast> {
    pub sig: FnSig<'ast>,
    pub body: Option<&'ast Block>,
}

/// Represents a slot signature (method or signal).
///
/// This is different from syn::FnDecl because GObject slots only support a subset
/// of the things that Rust does.  This is encoded in our `FnArg` type for arguments
/// and the `Ty` for the return type.
///
/// `FnSig` has a number of convenience methods that return an `impl
/// ToTokens`, for when you need to emit code for different aspects of
/// the `FnSig`:  the Glib type that corresponds to the function's
/// return value, the input arguments with Glib types, the input
/// arguments converted *from* Glib types, and so on.
pub struct FnSig<'ast> {
    pub name: Ident,
    pub inputs: Vec<FnArg<'ast>>,
    pub output: Ty<'ast>,
}

pub enum FnArg<'ast> {
    SelfRef(Token!(&), Token!(self)),
    Arg {
        mutbl: Option<Token![mut]>,
        name: Ident,
        ty: Ty<'ast>,
    },
}

pub struct Signal<'ast> {
    // FIXME: signal flags
    pub sig: FnSig<'ast>,
    pub body: Option<&'ast Block>,
}

pub enum Ty<'ast> {
    Unit,
    Char(Ident),
    Bool(Ident),
    Borrowed(Box<Ty<'ast>>),
    Integer(Ident),
    Owned(&'ast syn::Path),
}

fn path_from_string(s: &str) -> Path {
    parse_str::<Path>(s).unwrap()
}

impl<'ast> Program<'ast> {
    pub fn from_ast_program(ast: &'ast ast::Program) -> Result<Program<'ast>> {
        check_program(ast)?;

        let mut classes = Classes::new();
        for class in ast.classes() {
            classes.add(class)?;
        }
        for impl_ in ast.impls() {
            classes.add_impl(impl_)?;
        }

        let mut interfaces = Interfaces::new();
        for iface in ast.interfaces() {
            interfaces.add(iface)?;
        }

        Ok(Program {
            classes,
            interfaces,
        })
    }
}

impl<'ast> Slot<'ast> {
    fn translate_from_item(item: &'ast ast::ImplItem) -> Result<Slot<'ast>> {
        assert_eq!(item.attrs.len(), 0); // attributes unimplemented
        match item.node {
            ast::ImplItemKind::Method(ref method) => Slot::translate_from_method(method),
            ast::ImplItemKind::ReserveSlots(..) => unreachable!(),
            ast::ImplItemKind::Prop(_) => unreachable!(),
            ast::ImplItemKind::TraitType(_) => unreachable!(),
        }
    }

    fn translate_from_method(method: &'ast ast::ImplItemMethod) -> Result<Slot<'ast>> {
        if method.signal.is_some() {
            if let Some(spanned) = &method.public {
                bail!(
                    spanned,
                    "function `{}` is a signal so it doesn't need to be public",
                    method.name
                )
            }

            if let Some(spanned) = &method.virtual_ {
                bail!(
                    spanned,
                    "function `{}` is a signal so it doesn't need to be virtual",
                    method.name
                )
            }

            let sig = extract_sig(method)?;
            let body = match method.body {
                ast::ImplItemMethodBlock::Block(ref block) => Some(block),
                _ => None,
            };
            Ok(Slot::Signal(Signal {
                // FIXME: signal flags
                sig,
                body,
            }))
        } else if method.virtual_.is_some() {
            if let Some(spanned) = &method.public {
                bail!(
                    spanned,
                    "function `{}` is virtual so it doesn't need to be public",
                    method.name
                );
            }
            let sig = extract_sig(method)?;
            let body = match method.body {
                ast::ImplItemMethodBlock::Block(ref block) => Some(block),
                _ => None,
            };
            Ok(Slot::VirtualMethod(VirtualMethod { sig, body }))
        } else {
            let sig = extract_sig(method)?;
            Ok(Slot::Method(Method {
                sig,
                public: method.public.is_some(),
                body: match method.body.as_ref() {
                    Some(body) => body,
                    None => bail!(method.name, "function `{}` requires a body", method.name),
                },
            }))
        }
    }
}

fn extract_sig<'ast>(method: &'ast ast::ImplItemMethod) -> Result<FnSig<'ast>> {
    Ok(FnSig {
        output: extract_output(&method.output)?,
        inputs: extract_inputs(&method.inputs)?,
        name: method.name.clone(),
    })
}

fn extract_output<'ast>(output: &'ast ReturnType) -> Result<Ty<'ast>> {
    match *output {
        ReturnType::Type(_, ref boxt) => Ty::extract_from_type(boxt),
        ReturnType::Default => Ok(Ty::Unit),
    }
}

fn extract_inputs<'ast>(punc: &'ast Punctuated<syn::FnArg, Token!(,)>) -> Result<Vec<FnArg<'ast>>> {
    punc.iter()
        .map(|arg| match &*arg {
            // Extract &self
            syn::FnArg::Receiver(receiver) => {
                if !receiver.attrs.is_empty() {
                    bail!(arg, "cannot have attributes in self arguments");
                }

                let and_token;

                match receiver.reference {
                    None => bail!(arg, "by-value self not implemented"),
                    Some((_tok, Some(_))) => bail!(arg, "lifetime arguments on self not implemented"),
                    Some((tok, None)) => and_token = tok,
                }

                if receiver.mutability.is_some() {
                    bail!(arg, "&mut self not implemented");
                }

                let self_token = receiver.self_token;

                Ok(FnArg::SelfRef(and_token, self_token))
            }

            syn::FnArg::Typed(pat_type) => {
                if !pat_type.attrs.is_empty() {
                    bail!(arg, "cannot have attributes in function arguments");
                }

                match &*pat_type.pat {
                    syn::Pat::Ident(pat_ident) => {
                        if !pat_ident.attrs.is_empty() {
                            bail!(arg, "cannot have attributes in function argument identifiers");
                        }

                        if pat_ident.by_ref.is_some() {
                            bail!(arg, "cannot have &by_reference function arguments");
                        }

                        if pat_ident.subpat.is_some() {
                            bail!(arg, "cannot have @ subpatterns in function arguments");
                        }

                        let mutbl = pat_ident.mutability;
                        let ident = &pat_ident.ident;

                        Ok(FnArg::Arg {
                            mutbl,
                            name: ident.clone(),
                            ty: Ty::extract_from_type(&pat_type.ty)?,
                        })
                    }

                    _ => {
                        bail!(
                            arg,
                            "only bare identifiers are allowed as argument patterns"
                        );
                    }
                }
            }
        })
        .collect()

    // FIXME: check that &self is the first argument?
    // FIXME: check that the arguments are not empty?
}

impl<'ast> Ty<'ast> {
    pub fn extract_from_type(t: &'ast syn::Type) -> Result<Ty<'ast>> {
        match *t {
            syn::Type::Reference(syn::TypeReference {
                lifetime: Some(_), ..
            }) => {
                bail!(t, "borrowed types with lifetimes not implemented yet");
            }
            syn::Type::Reference(syn::TypeReference {
                lifetime: None,
                ref elem,
                ref mutability,
                ..
            }) => {
                if mutability.is_some() {
                    bail!(t, "mutable borrowed pointers not implemented");
                }
                let path = match **elem {
                    syn::Type::Path(syn::TypePath {
                        qself: None,
                        ref path,
                    }) => path,
                    _ => {
                        bail!(t, "only borrowed pointers to paths supported");
                    }
                };
                let ty = Ty::extract_from_path(path)?;
                Ok(Ty::Borrowed(Box::new(ty)))
            }
            syn::Type::Tuple(syn::TypeTuple { ref elems, .. }) => {
                if elems.is_empty() {
                    Ok(Ty::Unit)
                } else {
                    bail!(t, "tuple types not implemented yet")
                }
            }
            syn::Type::Path(syn::TypePath { qself: Some(_), .. }) => bail!(
                t,
                "path types with qualified self (`as` syntax) not allowed"
            ),
            syn::Type::Path(syn::TypePath {
                qself: None,
                ref path,
            }) => Ty::extract_from_path(path),

            _ => {
                bail!(t, "this type is not supported");
            }
        }
    }

    fn extract_from_path(t: &'ast syn::Path) -> Result<Ty<'ast>> {
        if t.segments.iter().any(|segment| match segment.arguments {
            syn::PathArguments::None => false,
            _ => true,
        }) {
            bail!(t, "path arguments not allowed in types");
        }

        if t.leading_colon.is_some() || t.segments.len() > 1 {
            return Ok(Ty::Owned(t));
        }

        let ident = &t.segments.first().unwrap().ident;

        match ident.to_owned_string().as_str() {
            "char" => Ok(Ty::Char(ident.clone())),
            "bool" => Ok(Ty::Bool(ident.clone())),
            "i8" | "i16" | "i32" | "i64" | "isize" | "u8" | "u16" | "u32" | "u64" | "usize" => {
                Ok(Ty::Integer(ident.clone()))
            }
            _other => Ok(Ty::Owned(t)),
        }
    }

    pub fn to_gtype_string(&self) -> &'static str {
        match *self {
            Ty::Unit => "gobject_sys::G_TYPE_NONE",
            Ty::Char(_) => "gobject_sys::G_TYPE_UINT", // <char as ToGlib>::GlibType = u32
            Ty::Bool(_) => "gobject_sys::G_TYPE_BOOLEAN",
            Ty::Borrowed(_) => unimplemented!(),

            Ty::Integer(ref ident) => match ident.to_owned_string().as_str() {
                "i8" => "gobject_sys::G_TYPE_CHAR",
                "i16" => unimplemented!("should we promote i16 to i32?"),
                "i32" => "gobject_sys::G_TYPE_INT",
                "i64" => "gobject_sys::G_TYPE_INT64",
                "isize" => unimplemented!(),

                "u8" => "gobject_sys::G_TYPE_UCHAR",
                "u16" => unimplemented!("should we promote u16 to u32?"),
                "u32" => "gobject_sys::G_TYPE_UINT",
                "u64" => "gobject_sys::G_TYPE_UINT64",
                "usize" => unimplemented!(),

                _ => unreachable!(),
            },

            Ty::Owned(_) => unimplemented!(),
        }
    }

    pub fn to_gtype_path(&self) -> Path {
        path_from_string(self.to_gtype_string())
    }

    /// Returns tokens for the name of the `g_param_spec_*()` function that can be
    /// used to create a `GParamSpec` suitable for the `Ty`.
    pub fn to_gparam_spec_constructor(&self) -> TokenStream {
        match *self {
            Ty::Unit => unreachable!("there is no g_param_spec_*() for the Unit type"),
            Ty::Char(_) => quote! { gobject_ffi::g_param_spec_uint }, // <char as ToGlib>::
            // GlibType = u32
            Ty::Bool(_) => quote! { gobject_ffi::g_param_spec_boolean },
            Ty::Borrowed(_) => unimplemented!(),

            Ty::Integer(ref ident) => match ident.to_owned_string().as_str() {
                "i8" | "u8" => quote! { gobject_ffi::g_param_spec_char },
                "i16" => unimplemented!("should we promote i16 to i32?"),
                "i32" => quote! { gobject_ffi::g_param_spec_int },
                "i64" => quote! { gobject_ffi::g_param_spec_int64 },
                "isize" => unimplemented!(),

                "u16" => unimplemented!("should we promote u16 to u32?"),
                "u32" => quote! { gobject_ffi::g_param_spec_uint },
                "u64" => quote! { gobject_ffi::g_param_spec_uint64 },
                "usize" => unimplemented!(),

                _ => unreachable!(),
            },

            Ty::Owned(_) => unimplemented!(),
        }
    }

    /// Returns tokens for the minimum/maximum/default values that are passed to
    /// the `g_param_spec_*()` functions when constructing a `GParamSpec`.
    pub fn to_gparam_spec_min_max_default(&self) -> TokenStream {
        match *self {
            Ty::Unit => unreachable!("there is no g_param_spec_*() for the Unit type"),
            Ty::Char(_) => quote! { std::u32::MIN, std::u32::MAX, 0 }, /* <char as ToGlib>::
                                                                         * GlibType = u32 */

            // g_param_spec_bool() takes no min/max, just a default
            Ty::Bool(_) => quote! { glib_ffi::GFALSE },

            Ty::Borrowed(_) => unimplemented!(),

            Ty::Integer(ref ident) => match ident.to_owned_string().as_str() {
                "i8" | "u8" => quote! { 0, 0xff as i8, 0 },
                "i16" => unimplemented!("should we promote i16 to i32?"),
                "i32" => quote! { i32::MIN, i32::MAX, 0 },
                "i64" => quote! { i64::MIN, i64::MAX, 0 },
                "isize" => unimplemented!(),

                "u16" => unimplemented!("should we promote u16 to u32?"),
                "u32" => quote! { std::u32::MIN, std::u32::MAX, 0 },
                "u64" => quote! { u64::MIN, u64::MAX, 0 },
                "usize" => unimplemented!(),

                _ => unreachable!(),
            },

            Ty::Owned(_) => unimplemented!(),
        }
    }

    pub fn to_gvalue_setter(&self) -> TokenStream {
        match *self {
            Ty::Unit => unreachable!("there is no g_value_set_*() for the Unit type"),
            Ty::Char(_) => quote! { gobject_ffi::g_value_set_uint }, // <char as ToGlib>::
            // GlibType = u32
            Ty::Bool(_) => quote! { gobject_ffi::g_value_set_bool },
            Ty::Borrowed(_) => unimplemented!(),

            Ty::Integer(ref ident) => match ident.to_owned_string().as_str() {
                "i8" | "u8" => quote! { gobject_ffi::g_value_set_char },
                "i16" => unimplemented!("should we promote i16 to i32?"),
                "i32" => quote! { gobject_ffi::g_value_set_int },
                "i64" => quote! { gobject_ffi::g_value_set_int64 },
                "isize" => unimplemented!(),

                "u16" => unimplemented!("should we promote u16 to u32?"),
                "u32" => quote! { gobject_ffi::g_value_set_uint },
                "u64" => quote! { gobject_ffi::g_value_set_uint64 },
                "usize" => unimplemented!(),

                _ => unreachable!(),
            },

            Ty::Owned(_) => unimplemented!(),
        }
    }
}

impl<'ast> Interfaces<'ast> {
    fn new() -> Interfaces<'ast> {
        Interfaces {
            items: HashMap::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.items.len()
    }

    pub fn get(&self, name: &str) -> &Interface<'_> {
        self.items.iter().find(|c| c.1.names == name).unwrap().1
    }

    fn add(&mut self, ast_iface: &'ast ast::Interface) -> Result<()> {
        let slots = self.translate_slots(ast_iface)?;
        let properties = self.translate_properties(ast_iface)?;

        let prev = self.items.insert(
            ast_iface.name.clone(),
            Interface {
                attrs: GeneratorAttributes::try_from(&ast_iface.attrs)?,
                names: Names::new(&ast_iface.name, "Iface"),
                slots,
                properties,
                ancestors: Vec::new(),
            },
        );
        if let Some(_) = prev {
            bail!(ast_iface.name, "redefinition of interface")
        } else {
            Ok(())
        }
    }

    fn translate_slots(&self, ast_iface: &'ast ast::Interface) -> Result<Vec<Slot<'ast>>> {
        let mut slots = Vec::new();

        for item in ast_iface.items.iter() {
            match item.node {
                ast::ImplItemKind::Prop(_) => (),
                ast::ImplItemKind::TraitType(_) => (),
                _ => {
                    let slot = Slot::translate_from_item(item)?;
                    slots.push(slot);
                }
            }
        }

        Ok(slots)
    }

    fn translate_properties(&self, ast_iface: &'ast ast::Interface) -> Result<Vec<Property<'ast>>> {
        let mut properties = Vec::new();

        for item in ast_iface.items.iter() {
            match item.node {
                ast::ImplItemKind::Prop(_) => {
                    let property = Property::translate_from_impl_item(item, false)?;
                    properties.push(property);
                }

                _ => (),
            }
        }

        Ok(properties)
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item = &'a Interface<'_>> + 'a {
        self.items.values()
    }
}

fn make_path_glib_object() -> Path {
    path_from_string("glib::Object")
}

impl<'a> ToTokens for FnArg<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match *self {
            FnArg::SelfRef(and, self_) => {
                and.to_tokens(tokens);
                self_.to_tokens(tokens);
            }
            FnArg::Arg {
                ref name,
                ref ty,
                ref mutbl,
            } => {
                mutbl.to_tokens(tokens);
                name.to_tokens(tokens);
                Token!(:)([Span::call_site()]).to_tokens(tokens);
                ty.to_tokens(tokens);
            }
        }
    }
}

impl<'a> ToTokens for Ty<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match *self {
            Ty::Unit => tokens.extend(quote! { () }),
            Ty::Char(ref tok) => tok.to_tokens(tokens),
            Ty::Bool(ref tok) => tok.to_tokens(tokens),
            Ty::Integer(ref t) => t.to_tokens(tokens),
            Ty::Borrowed(ref t) => {
                Token!(&)([Span::call_site()]).to_tokens(tokens);
                t.to_tokens(tokens)
            }
            Ty::Owned(t) => t.to_tokens(tokens),
        }
    }
}

pub mod tests {
    use super::*;

    pub fn run() -> Result<()> {
        creates_trivial_class()?;
        creates_class_with_superclass()?;
        maps_ty_to_gtype();
        Ok(())
    }

    fn test_class_and_superclass(raw: &str, class_name: &str, superclass_name: &str) -> Result<()> {
        let ast_program = parse_str::<ast::Program>(raw)?;

        let program = Program::from_ast_program(&ast_program)?;

        assert!(program.classes.len() == 1);

        let class = program.classes.get(class_name);
        assert_eq!(class.names.instance().to_owned_string(), class_name);
        assert_eq!(
            class.parent.instance().into_token_stream().to_string(),
            superclass_name
        );
        Ok(())
    }

    fn creates_trivial_class() -> Result<()> {
        let raw = "class Foo {}";

        test_class_and_superclass(raw, "Foo", "glib :: Object")
    }

    fn creates_class_with_superclass() -> Result<()> {
        let raw = "class Foo: Bar {}";

        test_class_and_superclass(raw, "Foo", "Bar")
    }

    fn maps_ty_to_gtype() {
        assert_eq!(Ty::Unit.to_gtype_string(), "gobject_sys::G_TYPE_NONE");
        assert_eq!(
            Ty::Char(Ident::from_str("char")).to_gtype_string(),
            "gobject_sys::G_TYPE_UINT"
        );
        assert_eq!(
            Ty::Bool(Ident::from_str("bool")).to_gtype_string(),
            "gobject_sys::G_TYPE_BOOLEAN"
        );

        // assert_eq!(Ty::Borrowed(...).to_gtype_string(), ...);

        assert_eq!(
            Ty::Integer(Ident::from_str("i8")).to_gtype_string(),
            "gobject_sys::G_TYPE_CHAR"
        );
        // assert_eq!(Ty::Integer(Ident::from_str("i16")).to_gtype(), ...);
        assert_eq!(
            Ty::Integer(Ident::from_str("i32")).to_gtype_string(),
            "gobject_sys::G_TYPE_INT"
        );
        assert_eq!(
            Ty::Integer(Ident::from_str("i64")).to_gtype_string(),
            "gobject_sys::G_TYPE_INT64"
        );
        // assert_eq!(Ty::Integer(Ident::from_str("isize")).to_gtype_string(), ...);

        assert_eq!(
            Ty::Integer(Ident::from_str("u8")).to_gtype_string(),
            "gobject_sys::G_TYPE_UCHAR"
        );
        // assert_eq!(Ty::Integer(Ident::new("u16", Span::call_site())).to_gtype_string(), ...);
        assert_eq!(
            Ty::Integer(Ident::from_str("u32")).to_gtype_string(),
            "gobject_sys::G_TYPE_UINT"
        );
        assert_eq!(
            Ty::Integer(Ident::from_str("u64")).to_gtype_string(),
            "gobject_sys::G_TYPE_UINT64"
        );
        // assert_eq!(Ty::Integer(Ident::from_str("usize")).to_gtype_string(), ...);

        // assert_eq!(Ty::Owned(...).to_gtype_string(), ...);
    }
}
