use crate::errors::Errors;
use syn::{ImplItemType, Type};

pub trait OptionTypeExt<'ast> {
    fn tryparse_glibtype(&'ast self, ty: &mut Option<&'ast Type>) -> Result<bool, Errors>;
}

impl<'ast> OptionTypeExt<'ast> for ImplItemType {
    fn tryparse_glibtype(&'ast self, ty: &mut Option<&'ast Type>) -> Result<bool, Errors> {
        if self.ident != "GlibType" {
            return Ok(false);
        }
        if self.attrs.len() > 0 {
            bail!(self.attrs[0], "no attributes allowed on type definitions")
        }
        if let Some(defaultness) = self.defaultness {
            bail!(
                defaultness,
                "Defaultness are not allowed on associated type definitions"
            );
        }
        // TODO: Errors or generics
        if ty.is_some() {
            bail!(self.ty, "GlibType associated type defined multiple times");
        }
        *ty = Some(&self.ty);
        Ok(true)
    }
}
