use crate::errors::Result;
use syn::token::Comma;
use syn::{
    punctuated::Punctuated, Attribute, Lit, LitStr, Meta, MetaList, MetaNameValue, NestedMeta,
};

pub struct GeneratorAttributes {
    pub h: Generator,
    pub rust: GeneratorRust,
    pub gir: Generator,
}

impl GeneratorAttributes {
    pub fn try_from(attrs: &[Attribute]) -> Result<Self> {
        let mut res = GeneratorAttributes {
            h: Generator {
                generate: None,
                compare: None,
            },
            rust: GeneratorRust {
                generate: None,
                compare: None,
                generate_only: false,
            },
            gir: Generator {
                generate: None,
                compare: None,
            },
        };
        for attr in attrs.iter() {
            if let Ok(meta) = attr.parse_meta() {
                res.add_attribute(&meta)?;
            } else {
                bail!(attr, "Error interpreting meta attribute");
            }
        }

        Ok(res)
    }

    fn add_attribute(&mut self, meta: &Meta) -> Result<()> {
        match &meta {
            Meta::List(MetaList { path, nested, .. }) if path.is_ident("generate") => {
                self.add_attribute_inner(nested, true)
            }
            Meta::List(MetaList { path, nested, .. }) if path.is_ident("compare") => {
                self.add_attribute_inner(nested, false)
            }
            Meta::List(MetaList { path, .. })
            | Meta::NameValue(MetaNameValue { path, .. })
            | Meta::Path(path) => bail!(path, "Unknown attribute"),
        }
    }

    fn add_attribute_inner(
        &mut self,
        nested: &Punctuated<NestedMeta, Comma>,
        generate: bool,
    ) -> Result<()> {
        let mut paths = Vec::new();
        let mut namevalues = false;
        let mut path = None;
        let mut extension = None;
        let mut generate_only = false;

        for nest in nested.iter() {
            match nest {
                NestedMeta::Lit(Lit::Str(litstr)) => {
                    if namevalues {
                        bail!(litstr, "Invalid attribute. Expected namevalues");
                    }
                    paths.push(litstr)
                }
                NestedMeta::Lit(lit) => {
                    bail!(lit, "Expected string literals");
                }
                NestedMeta::Meta(Meta::NameValue(MetaNameValue {
                    path: nv_path,
                    lit: Lit::Str(litstr),
                    ..
                })) if nv_path.is_ident("path") => {
                    if paths.len() > 0 {
                        bail!(litstr, "Invalid attribute. Expected paths");
                    }
                    namevalues = true;
                    if path.replace(litstr).is_some() {
                        bail!(
                            litstr,
                            "Only one path can be specified while using name-values",
                        )
                    }
                }
                NestedMeta::Meta(Meta::NameValue(MetaNameValue {
                    path: nv_path,
                    lit: Lit::Str(litstr),
                    ..
                })) if nv_path.is_ident("extension") => {
                    if paths.len() > 0 {
                        bail!(litstr, "Invalid attribute. Expected paths");
                    }
                    namevalues = true;
                    if extension
                        .replace(GeneratorExtension::try_from(litstr)?)
                        .is_some()
                    {
                        bail!(
                            litstr,
                            "Only one path can be specified while using name-values",
                        )
                    }
                }
                NestedMeta::Meta(Meta::NameValue(_)) => {
                    bail!(nest, "Unexpected namevalue");
                }
                NestedMeta::Meta(Meta::List(_)) => {
                    bail!(nest, "Unexpected list");
                }
                NestedMeta::Meta(Meta::Path(p)) if p.is_ident("generate_only") => {
                    if generate_only {
                        bail!(p, "generate-only may only be specified once");
                    }
                    generate_only = true;
                }
                NestedMeta::Meta(Meta::Path(p)) => {
                    bail!(p, "Unexpected path");
                }
            }
        }

        if paths.len() > 0 {
            for path in paths {
                let extension = GeneratorExtension::try_from_extension(path)?;
                self.add_path(path, generate, extension, &mut generate_only)?;
            }
        }
        if let Some(path) = path {
            if extension.is_none() {
                self.add_path(
                    path,
                    generate,
                    extension
                        .ok_or(())
                        .or(GeneratorExtension::try_from_extension(path))?,
                    &mut generate_only,
                )?;
            }
        }
        Ok(())
    }

    fn add_path(
        &mut self,
        path: &LitStr,
        generate: bool,
        extension: GeneratorExtension,
        generate_only: &mut bool,
    ) -> Result<()> {
        match (extension, generate) {
            (GeneratorExtension::Rust, true) => {
                if *generate_only {
                    self.rust.generate_only = true;
                    ::std::mem::replace(generate_only, false);
                }
                if self.rust.generate.replace(path.clone()).is_some() {
                    bail!(path, "Can only generate one rust file")
                }
            }
            (GeneratorExtension::Rust, false) => {
                if self.rust.compare.replace(path.clone()).is_some() {
                    bail!(path, "Can only compare one rust file")
                }
            }
            (GeneratorExtension::Gir, true) => {
                if self.gir.generate.replace(path.clone()).is_some() {
                    bail!(path, "Can only generate one gir file")
                }
            }
            (GeneratorExtension::Gir, false) => {
                if self.gir.compare.replace(path.clone()).is_some() {
                    bail!(path, "Can only compare one gir file")
                }
            }
            (GeneratorExtension::H, true) => {
                if self.h.generate.replace(path.clone()).is_some() {
                    bail!(path, "Can only generate one h file")
                }
            }
            (GeneratorExtension::H, false) => {
                if self.h.compare.replace(path.clone()).is_some() {
                    bail!(path, "Can only compare one h file")
                }
            }
        }
        Ok(())
    }
}

enum GeneratorExtension {
    Rust,
    H,
    Gir,
}

impl GeneratorExtension {
    fn try_from_extension(lit: &LitStr) -> Result<Self> {
        let litval = lit.value();
        if litval.ends_with(".rs") {
            Ok(GeneratorExtension::Rust)
        } else if litval.ends_with(".h") {
            Ok(GeneratorExtension::H)
        } else if litval.ends_with(".gir") {
            Ok(GeneratorExtension::Gir)
        } else {
            bail!(
                lit,
                "Unsupported extension. Only .rs, .h and .gir extensions are recognized"
            )
        }
    }

    fn try_from(lit: &LitStr) -> Result<Self> {
        match lit.value().as_str() {
            "rust" => Ok(GeneratorExtension::Rust),
            "h" => Ok(GeneratorExtension::H),
            "gir" => Ok(GeneratorExtension::Gir),
            _ => bail!(
                lit,
                r#"Unsupported generator extension. Only "rust", "h" and "gir" is supported."#,
            ),
        }
    }
}

pub enum GeneratorOutput {
    ComparePath,
    Path,
    PathOnly,
}

impl GeneratorOutput {
    fn try_from(lit: &LitStr) -> Result<Self> {
        match lit.value().as_str() {
            "compare-path" => Ok(GeneratorOutput::ComparePath),
            "path" => Ok(GeneratorOutput::Path),
            "path-only" => Ok(GeneratorOutput::PathOnly),
            _ => bail!(lit, r#"Unsupported generator output. Only "compare-path", "path" and "path-only" is supported."#)
        }
    }
}

pub struct Generator {
    pub generate: Option<LitStr>,
    pub compare: Option<LitStr>,
}

pub struct GeneratorRust {
    pub generate: Option<LitStr>,
    pub compare: Option<LitStr>,
    pub generate_only: bool,
}
