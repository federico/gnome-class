use super::associated_types::OptionTypeExt;
use super::ast;
use super::GeneratorAttributes;
use super::{Method, Property, Slot, VirtualMethod};
use crate::errors::Result;
use crate::hir::names::{Names, ParentNames};
use crate::hir::{Override, OverrideItem};
use proc_macro2::Ident;
use std::collections::HashMap;
use std::rc::Rc;
use syn::{Field, Lit, Path};

pub struct Classes<'ast> {
    items: HashMap<Ident, Class<'ast>>,
}

#[cfg_attr(rustfmt, rustfmt_skip)]
pub struct Class<'ast> {
    pub attrs: GeneratorAttributes, // Interpreted attributes
    pub names: Names, // The name of the class
    pub parent: ParentNames,           // Parent
    pub implements: Vec<Path>,    // names of GTypeInterfaces

    // pub class_private: Option<&'ast ast::PrivateStruct>

    pub private_fields: Vec<&'ast Field>,

    // The order of these is important; it's the order of the slots in FooClass
    pub slots: Vec<Slot<'ast>>,
    pub n_reserved_slots: usize,

    pub properties: Vec<Property<'ast>>,
    pub overrides: Vec<OverrideItem<'ast>>,
    pub ancestors: Vec<ParentNames>,
}

impl<'ast> Classes<'ast> {
    pub(super) fn new() -> Classes<'ast> {
        Classes {
            items: HashMap::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.items.len()
    }

    pub fn get(&self, name: &str) -> &Class<'_> {
        self.items.iter().find(|c| c.1.names == name).unwrap().1
    }

    pub(super) fn add(&mut self, ast_class: &'ast ast::Class) -> Result<()> {
        let parent = ast_class.extends.iter().next().map(|path| path.clone());
        let prev = self.items.insert(
            ast_class.name.clone(),
            Class {
                names: Names::new(&ast_class.name, "Class"),
                attrs: GeneratorAttributes::try_from(&ast_class.attrs)?,
                parent: parent
                    .map(|par| ParentNames::new(par))
                    .unwrap_or(ParentNames::new_from_glib(::glib::Type::BaseObject)),
                implements: Vec::new(),
                private_fields: ast_class.fields.named.iter().collect(),
                slots: Vec::new(),
                n_reserved_slots: 0,
                properties: Vec::new(),
                overrides: Vec::new(),
                ancestors: Vec::new(),
            },
        );
        if prev.is_some() {
            bail!(ast_class.name, "redefinition of class")
        }
        Ok(())
    }

    fn add_impl_method(item: &'ast ast::ImplItemMethod) -> Result<Method<'_>> {
        if let Some(spanned) = &item.signal {
            bail!(spanned, "can't implement signals for parent classes");
        }
        if !item.virtual_.is_some() {
            bail!(
                item.name,
                "can only implement virtual functions for parent classes"
            );
        }
        if let Some(spanned) = &item.public {
            bail!(spanned, "overrides are always public, no `pub` needed");
        }
        Ok(match Slot::translate_from_method(item)? {
            Slot::VirtualMethod(VirtualMethod {
                sig,
                body: Some(body),
            }) => Method {
                public: false,
                sig,
                body,
            },
            Slot::VirtualMethod(VirtualMethod { .. }) => {
                bail!(
                    item.name,
                    "overrides must provide a body for virtual methods"
                );
            }
            _ => unreachable!(),
        })
    }

    pub(super) fn add_impl(&mut self, impl_: &'ast ast::Impl) -> Result<()> {
        let mut errors = Vec::new();

        let class = match self.items.get_mut(&impl_.self_path) {
            Some(class) => class,
            None => {
                bail!(impl_.self_path, "impl for class that doesn't exist");
            }
        };
        match *impl_ {
            ast::Impl {
                trait_: Some(ref parent_class),
                ..
            } => {
                let mut glibtype = None;
                let parent_names = Rc::new(ParentNames::new(parent_class.clone()));
                for item in impl_.items.iter() {
                    match &item.node {
                        ast::ImplItemKind::Method(ref m) => match Self::add_impl_method(m) {
                            Ok(method) => {
                                class.overrides.push(OverrideItem::Method(Override {
                                    ancestor: parent_names.clone(),
                                    inner: method,
                                }));
                            }
                            Err(e) => errors.push(e),
                        },
                        ast::ImplItemKind::ReserveSlots(.., ref l) => {
                            errors.push(
                                format_err!(l, "can't reserve slots in a parent class impl").into(),
                            );
                        }
                        ast::ImplItemKind::Prop(ast::ImplProp { .. }) => {
                            // g_object_class_override_property (object_class,
                            // PROP_AUTOSAVE_FREQUENCY, "autosave-frequency");
                            match Property::translate_from_impl_item(item, true) {
                                Ok(property) => class.properties.push(property),
                                Err(e) => errors.push(e),
                            }
                        }
                        ast::ImplItemKind::TraitType(ref traittype) => {
                            if let Err(e) = traittype.tryparse_glibtype(&mut glibtype) {
                                errors.push(e);
                            }
                        }
                    }
                }
                class.ancestors.push(ParentNames::new_with_explicit_ffi(
                    parent_class.clone(),
                    glibtype.map(std::clone::Clone::clone),
                ));
            }
            ast::Impl { trait_: None, .. } => {
                for item in impl_.items.iter() {
                    match item.node {
                        ast::ImplItemKind::Prop(_) => {
                            match Property::translate_from_impl_item(item, false) {
                                Ok(property) => class.properties.push(property),
                                Err(e) => errors.push(e),
                            }
                        }
                        ast::ImplItemKind::ReserveSlots(.., ref l) => {
                            match reserved_slots_from_literal(l) {
                                Ok(n) => class.n_reserved_slots = n,
                                Err(e) => errors.push(e),
                            }
                        }
                        _ => match Slot::translate_from_item(item) {
                            Ok(slot) => class.slots.push(slot),
                            Err(e) => errors.push(e),
                        },
                    }
                }
            }
        }

        // Sort the properties in a stable way. But the own properties
        // should be listed before the override properties.
        class.properties.sort_by_key(|prop| prop.is_override);

        if errors.len() == 0 {
            Ok(())
        } else {
            Err(errors.into())
        }
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item = &'a Class<'_>> + 'a {
        self.items.values()
    }
}

fn reserved_slots_from_literal(l: &Lit) -> Result<usize> {
    match *l {
        Lit::Int(ref i) => Ok(i.base10_parse()?),
        _ => bail!(
            l,
            "expected integer literal for the number of reserved slots"
        ),
    }
}
