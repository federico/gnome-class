use crate::ast;
use crate::errors::*;
use gobject_sys::GParamFlags;
use proc_macro2::{Ident, TokenStream};
use quote::quote;
use syn::Block;
use syn::Type;
use syn::{Attribute, Lit, Meta, MetaList, NestedMeta};

pub struct Property<'ast> {
    pub name: Ident,
    pub type_: &'ast Type,
    pub getter: &'ast Block,
    pub setter: PropertySetterBlock<'ast>,
    pub attrs: PropertyAttributes,
    pub is_override: bool,
}

pub struct PropertySetterBlock<'ast> {
    pub param: Ident,
    pub body: &'ast Block,
}

impl<'ast> Property<'ast> {
    pub(super) fn translate_from_impl_item(
        item: &'ast ast::ImplItem,
        is_override: bool,
    ) -> Result<Property<'ast>> {
        if let ast::ImplItemKind::Prop(ref prop) = item.node {
            let name = &prop.name;
            let type_ = &prop.type_;

            let getter = match prop.getter() {
                Some(&ast::ImplPropBlock::Getter(ref b)) => b,
                None => bail!(prop.name, "property without getter: {}", name),
                _ => bail!(prop.name, "invalid property getter: {}", name),
            };

            let setter = match prop.setter() {
                Some(ast::ImplPropBlock::Setter(ref b)) => PropertySetterBlock {
                    param: b.param.clone(),
                    body: &b.block,
                },
                None => bail!(prop.name, "property without setter: {}", name),
                _ => bail!(prop.name, "invalid property setter: {}", name),
            };

            return Ok(Property {
                name: name.clone(),
                type_,
                getter: &getter.block,
                setter,
                attrs: PropertyAttributes::new(item.attrs.iter(), prop.attributes.iter())?,
                is_override,
            });
        }

        bail!(item.node.tokens(), "Invalid definition inside property")
    }
}

pub struct PropertyAttributes {
    readable: bool,
    writable: bool,
    construct: bool,
    construct_only: bool,
    deprecated: bool,
    explicit_notify: bool,
    lax_validation: bool,
    private: bool,
    static_blurb: bool,
    static_nick: bool,
    static_name: bool,

    blurb: Option<Lit>,
    nick: Option<Lit>,
    name: Option<Lit>,
}

impl PropertyAttributes {
    fn new<'this>(
        outer: impl Iterator<Item = &'this Attribute>,
        inner: impl Iterator<Item = &'this Attribute>,
    ) -> Result<Self> {
        let mut constructor = PropertyAttributesConstruction {
            has_flag: false,
            source_flags: false,
            property_attributes: PropertyAttributes {
                name: None,
                blurb: None,
                nick: None,
                construct: false,
                construct_only: false,
                deprecated: false,
                explicit_notify: false,
                lax_validation: false,
                private: false,
                readable: false,
                writable: false,
                static_blurb: false,
                static_name: false,
                static_nick: false,
            },
        };

        let mut errors = Vec::new();
        for item in outer.chain(inner) {
            if let Err(e) = constructor.append_attribute(item) {
                errors.push(e);
            }
        }
        if errors.len() > 0 {
            return Err(errors.into());
        }

        if !constructor.has_flag && !constructor.source_flags {
            // No relevant attributes found. Assume readable and writable
            constructor.property_attributes.readable = true;
            constructor.property_attributes.writable = true;
        }
        Ok(constructor.property_attributes)
    }

    pub fn flags(&self) -> GParamFlags {
        // let mut result = gobject_sys::G_PARAM_READABLE ^ gobject_sys::G_PARAM_READABLE;
        let mut result = 0;
        if self.readable {
            result |= gobject_sys::G_PARAM_READABLE;
        }
        if self.writable {
            result |= gobject_sys::G_PARAM_WRITABLE;
        }
        if self.construct {
            result |= gobject_sys::G_PARAM_CONSTRUCT;
        }
        if self.construct_only {
            result |= gobject_sys::G_PARAM_CONSTRUCT_ONLY;
        }
        if self.deprecated {
            result |= gobject_sys::G_PARAM_DEPRECATED;
        }
        if self.explicit_notify {
            result |= gobject_sys::G_PARAM_EXPLICIT_NOTIFY;
        }
        if self.lax_validation {
            result |= gobject_sys::G_PARAM_LAX_VALIDATION;
        }
        if self.private {
            result |= gobject_sys::G_PARAM_PRIVATE;
        }
        if self.static_blurb {
            result |= gobject_sys::G_PARAM_STATIC_BLURB;
        }
        if self.static_nick {
            result |= gobject_sys::G_PARAM_STATIC_NICK;
        }
        if self.static_name {
            result |= gobject_sys::G_PARAM_STATIC_NAME;
        }
        result
    }

    pub fn flags_tokens(&self) -> TokenStream {
        // gobject_ffi
        let flags = self.flags();
        quote! { #flags }
    }

    pub fn blurb(&self) -> &Option<Lit> {
        &self.blurb
    }

    pub fn nick(&self) -> &Option<Lit> {
        &self.nick
    }

    pub fn name(&self) -> &Option<Lit> {
        &self.name
    }
}

struct PropertyAttributesConstruction {
    property_attributes: PropertyAttributes,

    has_flag: bool,     // At least one flag is toggeled
    source_flags: bool, // Initialized with the multiple flags at once
}

impl PropertyAttributesConstruction {
    fn append_attribute(&mut self, attribute: &Attribute) -> Result<()> {
        let meta = match attribute.parse_meta() {
            Ok(m) => m,
            Err(_) => bail!(attribute, "invalid meta attribute"),
        };
        self.append_meta(&meta)
    }

    fn append_meta(&mut self, meta: &Meta) -> Result<()> {
        match meta {
            Meta::Path(ref path) => if let Some(ident) = path.get_ident() {
                self.append_ident(ident)
            } else {
                bail!(path, "expected a single identifier")
            }
            Meta::NameValue(ref nv) if nv.path.is_ident("name") => {
                if self.property_attributes.name.is_some() {
                    bail!(nv.path, "Only one name-string can be set for properties")
                }
                self.property_attributes.name = Some(nv.lit.clone());
                Ok(())
            }
            Meta::NameValue(ref nv) if nv.path.is_ident("nick") => {
                if self.property_attributes.nick.is_some() {
                    bail!(nv.path, "Only one nick-string can be set for properties")
                }
                self.property_attributes.nick = Some(nv.lit.clone());
                Ok(())
            }
            Meta::NameValue(ref nv) if nv.path.is_ident("blurb") => {
                if self.property_attributes.blurb.is_some() {
                    bail!(nv.path, "Only one blurb-string can be set for properties")
                }
                self.property_attributes.blurb = Some(nv.lit.clone());
                Ok(())
            }
            Meta::NameValue(ref nv) => bail!(nv.path, "Unexpected namevalue in attribute"),
            Meta::List(ref list) if list.path.is_ident("flags") => self.append_fields(list),
            Meta::List(ref list) => bail!(list.path, "Unexpected list item"),
        }
    }

    fn append_ident(&mut self, ident: &Ident) -> Result<()> {
        if self.source_flags {
            bail!(ident, "Only one source of property flags is allowed (1)");
        }
        self.append_private(ident)
    }

    fn append_fields(&mut self, fields: &MetaList) -> Result<()> {
        if self.has_flag || self.source_flags {
            bail!(
                fields.path,
                "Only one source of property flags is allowed (2)"
            );
        }
        self.source_flags = true;
        for field in fields.nested.iter() {
            if let NestedMeta::Meta(Meta::Path(path)) = field {
                if let Some(ident) = path.get_ident() {
                    self.append_private(ident)?;
                    continue;
                }
            } else {
                bail!(field, "Flags has unrecognized format")
            }
        }
        Ok(())
    }

    fn append_private(&mut self, ident: &Ident) -> Result<()> {
        let old;
        if ident == "construct" {
            old = self.property_attributes.construct;
            self.property_attributes.construct = true;
        } else if ident == "construct_only" {
            old = self.property_attributes.construct_only;
            self.property_attributes.construct_only = true;
        } else if ident == "deprecated" {
            old = self.property_attributes.deprecated;
            self.property_attributes.deprecated = true;
        } else if ident == "explicit_notify" {
            old = self.property_attributes.explicit_notify;
            self.property_attributes.explicit_notify = true;
        } else if ident == "lax_validation" {
            old = self.property_attributes.lax_validation;
            self.property_attributes.lax_validation = true;
        } else if ident == "private" {
            old = self.property_attributes.private;
            self.property_attributes.private = true;
        } else if ident == "readable" {
            old = self.property_attributes.readable;
            self.property_attributes.readable = true;
        } else if ident == "static_blurb" {
            old = self.property_attributes.static_blurb;
            self.property_attributes.static_blurb = true;
        } else if ident == "static_nick" {
            old = self.property_attributes.static_nick;
            self.property_attributes.static_nick = true;
        } else if ident == "static_name" {
            old = self.property_attributes.static_name;
            self.property_attributes.static_name = true;
        } else if ident == "writable" {
            old = self.property_attributes.writable;
            self.property_attributes.writable = true;
        } else {
            bail!(ident, "unknown flag")
        }
        self.has_flag = true;
        if old {
            bail!(ident, "duplicate flag")
        } else {
            Ok(())
        }
    }
}
