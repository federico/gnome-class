use proc_macro2::TokenStream;
use quote::ToTokens;
use std::result::Result as StdResult;
pub use syn::parse::Error;

/// Result type alias for an error type which allows for multiple errors
pub type Result<T> = StdResult<T, Errors>;

macro_rules! bail {
    ($($args:tt)*) => {
        return Err(format_err!($($args)*).into())
    }
}

/// Creates a `::syn::parse::Error` with the span of the provided tokens and a formatted
/// message.
///
/// # Example
///
/// ```ignore
/// let tokens = ...;
/// let foo = "...";
/// format_err!(tokens, "expected identifier {}", foo);
/// ```
macro_rules! format_err {
    ($tokens:expr, $($msg:tt)*) => {
        match &$tokens {
            t => {
                ::syn::parse::Error::new_spanned(t, format_args!($($msg)*))
            }
        }
    }
}

/// Creates a `::syn::parse::Error` with an explicit span and a formatted message
///
/// # Example
///
/// ```ignore
/// let token = ...;
/// let foo = "...";
/// format_err!(token.span(), "expected identifier {}", foo);
/// ```
macro_rules! format_err_span {
    ($span:expr, $($msg:tt)*) => {
        ::syn::parse::Error::new($span, format_args!($($msg)*))
    }
}

#[derive(Debug)]
pub struct Errors(Vec<Error>);

impl From<Error> for Errors {
    fn from(f: Error) -> Errors {
        Errors(vec![f])
    }
}

impl From<Vec<Errors>> for Errors {
    fn from(e: Vec<Errors>) -> Errors {
        let result = e.into_iter().flat_map(|v| v.0).collect::<Vec<_>>();
        assert!(result.len() > 0);
        Errors(result)
    }
}

/// Used to create a TokenStream from a list of errors
impl ToTokens for Errors {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        for item in self.0.iter() {
            item.to_compile_error().to_tokens(tokens);
        }
    }
}
