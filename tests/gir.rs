#![cfg(any(not(feature = "single-test"), feature = "single-test-gir"))]

extern crate gobject_class;
extern crate gobject_gen;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

use std::cell::Cell;

#[cfg(feature = "test-generated")]
include!("generated/gir-gen.rs");

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("Counter.gir")]
    #[generate("tests/generated/gir-gen.rs")]
    class Counter {
      f: Cell<u32>,
    }

    impl Counter {
        pub fn add(&self, x: u32) -> u32 {
            self.get_priv().f.set(self.get() + x);
            self.get()
        }

        pub fn get(&self) -> u32 {
            self.get_priv().f.get()
        }

        pub fn return_none(&self) {
            // noop
        }

        pub fn return_bool(&self) -> bool {
            true
        }
    }
}

#[test]
fn test() {
    let c: Counter = Counter::new();

    println!("Counter has value: {}", c.get());

    c.add(2);
    c.add(20);
    assert_eq!(c.get(), 22);

    println!("Counter has value: {}", c.get());
}
