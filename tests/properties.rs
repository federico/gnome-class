#![cfg(any(not(feature = "single-test"), feature = "single-test-properties"))]

extern crate gobject_class;
extern crate gobject_gen;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

use std::cell::Cell;

#[cfg(feature = "test-generated")]
include!("generated/properties-gen.rs");

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("tests/generated/properties-gen.rs")]
    class ClassWithProps {
        p: Cell<u32>,
        p2: Cell<u32>,
    }

    impl ClassWithProps {
        pub fn get(&self) -> u32 {
            self.get_priv().p.get() +
            self.get_priv().p2.get()
        }

        property MyProp: T where T: u32 {
            get(&self) -> T {
                let private = self.get_priv();
                return private.p.get();
            }

            set(&self, value: T) {
                let private = self.get_priv();
                private.p.set(value);
            }
        }

        property Prop2: T where T: u32 {
            get(&self) -> T {
                let private = self.get_priv();
                return private.p2.get();
            }

            set(&self, value: T) {
                let private = self.get_priv();
                private.p2.set(value);
            }
        }
    }
}

#[test]
fn test_props() {
    let obj: ClassWithProps = ClassWithProps::new();
    assert_eq!(obj.get(), 0);

    assert_eq!(obj.get_property_prop2(), 0);
    obj.set_property_prop2(42).unwrap();
    assert_eq!(obj.get(), 42);
    assert_eq!(obj.get_property_prop2(), 42);

    obj.set_property_myprop(58).unwrap();
    assert_eq!(obj.get_property_myprop(), 58);
}
