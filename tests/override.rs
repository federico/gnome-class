#![cfg(any(not(feature = "single-test"), feature = "single-test-override"))]
//#![deny(warnings)]

extern crate gobject_class;
extern crate gobject_gen;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

#[cfg(feature = "test-generated")]
include!("generated/override-gen.rs");

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("tests/generated/override-gen.rs")]
    class One {
    }

    impl One {
        pub fn one(&self) -> u32 {
            1
        }

        virtual fn get(&self, i: u32, j: u32) -> u32 {
            1 + i + j
        }
    }

    #[generate("tests/generated/override-gen.rs")]
    class Two: One {
    }

    impl One for Two {
        virtual fn get(&self, i: u32, j: u32) -> u32 {
            2 + i + j
        }
    }
}

#[test]
fn test() {
    let one = One::new();
    let two = Two::new();

    assert!(one.one() == 1);
    assert!(one.get(0, 0) == 1);
    assert!(two.one() == 1);
    println!("Test");
    assert!(two.get(0, 0) == 2);
}
