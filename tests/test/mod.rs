/// ! This module contains the code that can be reused in all tests in this directory
use ::glib_sys::{gpointer, GDestroyNotify};
use ::libc::{c_char, c_int, c_void, size_t};
use ::std::ptr;
use std::ffi::CString;

#[repr(C)]
struct GLogField {
    name: *const c_char,
    value: gpointer,
    size: size_t,
}

// type GLogFunc = unsafe extern "C" fn(domain: *const c_char, flags: c_int, message: *const c_char,
// user: gpointer);
type GLogWriterFunc = unsafe extern "C" fn(
    flags: c_int,
    fields: *const GLogField,
    nr_fields: size_t,
    user: gpointer,
) -> c_int;

extern "C" {
    // fn g_log_set_always_fatal(flags: ::libc::c_int) -> ::libc::c_int;
    fn g_log_writer_default(
        flags: c_int,
        fields: *const GLogField,
        nr_fields: size_t,
        user: gpointer,
    ) -> c_int;
    // fn g_log_set_handler(domain: *const c_char, log_levels: c_int,
    //        callback: GLogFunc,
    //        user: *const c_void) -> c_uint;
    fn g_log_set_writer_func(
        func: GLogWriterFunc,
        user_data: gpointer,
        user_data_free: GDestroyNotify,
    );
    fn g_log_writer_format_fields(
        log_level: c_int,
        fields: *const GLogField,
        n_fields: size_t,
        use_color: c_char,
    ) -> *mut c_char;

}

pub struct PanicOnMessage {
    msg: Box<Vec<String>>,
}

impl PanicOnMessage {
    pub fn new() -> Self {
        let mut this = PanicOnMessage {
            msg: Box::new(Vec::new()),
        };
        unsafe {
            g_log_set_writer_func(
                Self::on_struct_message_report,
                (&mut *this.msg as *mut Vec<String>) as *mut c_void,
                None,
            )
        }
        this
    }

    #[allow(dead_code)]
    pub fn clear(&mut self) -> Vec<String> {
        let mut res = Vec::new();
        ::std::mem::swap(&mut *self.msg, &mut res);
        res
    }

    unsafe extern "C" fn on_struct_message_report(
        flags: c_int,
        fields: *const GLogField,
        nr_fields: size_t,
        this: gpointer,
    ) -> c_int {
        let this = &mut *(this as *mut Vec<String>);
        let msg = CString::from_raw(g_log_writer_format_fields(flags, fields, nr_fields, 0i8));
        if let Ok(msg) = msg.into_string() {
            this.push(msg);
        } else {
            eprintln!("Unable to convert to string");
        }
        g_log_writer_default(flags, fields, nr_fields, ptr::null_mut())
    }
}

impl Drop for PanicOnMessage {
    fn drop(&mut self) {
        // Make sure that the user data is destroyed and not used again
        unsafe {
            g_log_set_writer_func(g_log_writer_default, ptr::null_mut(), None);
        }
        if self.msg.len() > 0 {
            panic!(
                "The following messages are received from the glib:{}",
                self.msg.join("")
            );
        }
    }
}
