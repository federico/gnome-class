#![cfg(any(not(feature = "single-test"), feature = "single-test-memory-layout"))]
//#![deny(warnings)]

extern crate gobject_class;
extern crate gobject_gen;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

use std::mem;

#[cfg(feature = "test-generated")]
include!("generated/memory-layout-gen.rs");

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("tests/generated/memory-layout-gen.rs")]
    class ZeroSlots {
    }

    #[generate("tests/generated/memory-layout-gen.rs")]
    class OneSlot {
    }

    impl OneSlot {
        virtual fn foo(&self) {
        }

        pub fn static_method(&self) {
        }
    }

    #[generate("tests/generated/memory-layout-gen.rs")]
    class TwoSlots {
    }

    impl TwoSlots {
        virtual fn foo(&self) {
        }

        pub fn static_method(&self) {
        }

        signal fn bar(&self);
    }

    #[generate("tests/generated/memory-layout-gen.rs")]
    class ThreeSlots: TwoSlots {
    }

    impl ThreeSlots {
        signal fn baz(&self);
    }

    impl TwoSlots for ThreeSlots
    {

    }

    #[generate("tests/generated/memory-layout-gen.rs")]
    class ThirteenSlots: ThreeSlots {
    }

    impl ThirteenSlots {
        reserve_slots(10)
    }

    impl ThreeSlots for ThirteenSlots {}
    impl TwoSlots for ThirteenSlots {}
}

fn assert_n_slots_bigger_than_gobject_class<T>(n: usize)
where
    T: glib::ObjectType,
{
    assert_eq!(
        mem::size_of::<<T as glib::ObjectType>::GlibType>(),
        mem::size_of::<gobject_sys::GObject>()
    );
    assert_eq!(
        mem::size_of::<<T as glib::ObjectType>::GlibType>(),
        mem::size_of::<gobject_sys::GObjectClass>() + n * mem::size_of::<usize>()
    );
}

#[test]
fn size_of_structs() {
    assert_n_slots_bigger_than_gobject_class::<ZeroSlots>(0);
    assert_n_slots_bigger_than_gobject_class::<OneSlot>(1);
    assert_n_slots_bigger_than_gobject_class::<TwoSlots>(2);
    assert_n_slots_bigger_than_gobject_class::<ThreeSlots>(3);
    assert_n_slots_bigger_than_gobject_class::<ThirteenSlots>(13);
}
