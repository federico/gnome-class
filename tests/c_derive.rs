#![cfg(ignore)]
#![cfg(any(not(feature = "single-test"), feature = "single-test-c_derive"))]
#![deny(warnings)]

extern crate gobject_class;
extern crate gobject_gen;
extern crate gobject_gen_test;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

#[cfg(feature = "test-generated")]
include!("generated/c_derive.rs");

use gobject_class::glib::translate::ToGlibPtr;
use gobject_gen_test::auto::ffi;
use gobject_gen_test::auto::{Counter, CounterExt};

mod test;

// ffi for the c-side
// mod ffi
// {
// use gobject_sys::{GObject,GObjectClass};
// use libc::c_int;
// use glib_sys::{gpointer};
//
//
// #[repr(C)]
// #[derive(Copy, Clone)]
// pub struct CtestCounter {
// pub parent_instance: GObject,
// }
//
// #[repr(C)]
// #[derive(Copy, Clone)]
// pub struct CtestCounterClass
// {
// pub parent_class: GObjectClass,
// pub increment: Option<unsafe extern "C" fn(*mut CtestCounter, c_int)>,
// pub get: Option<unsafe extern "C" fn(*mut CtestCounter)>,
// pub padding: [gpointer; 10],
// }
//
// extern "C" {
// pub fn ctest_counter_get_type() -> usize;
// pub fn ctest_counter_increment (this: *mut CtestCounter, value: i32);
// pub fn ctest_counter_get(this: *mut CtestCounter) -> i32;
// pub fn ctest_counter_new() -> *mut CtestCounter;
// pub fn test_increment_in_c(this: *mut CtestCounter) -> c_int;
// }
// }
//
// Wrappers for the c-side class
// use std::ptr;
// use std::mem;
// use gobject_sys as gobject_ffi;
// use glib::translate::Stash;
// use glib::translate::ToGlibPtrMut;
// use glib::translate::ToGlibPtr;
// use glib::translate::FromGlibPtrFull;
// use glib::translate::FromGlibPtrNone;
// glib_wrapper!{
// pub struct CtestCounter(Object<ffi::CtestCounter, ffi::CtestCounterClass>);
//
// match fn {
// get_type => || ffi::ctest_counter_get_type(),
// }
// }
//
// impl CtestCounter
// {
// pub fn new() -> Self {
// unsafe {
// CtestCounter::from_glib_none(ffi::ctest_counter_new()).downcast_unchecked()
// }
// }
// }
//
// trait CtestCounterExt
// {
// fn increment(&self, val: i32);
// fn get(&self) -> i32;
// }
//
// impl<T: ::glib::IsA<CtestCounter>> CtestCounterExt for T
// {
// fn increment(&self, val: i32)
// {
// unsafe { ffi::ctest_counter_increment(ToGlibPtr::to_glib_none(self).0, val) };
// }
//
// fn get(&self) -> i32
// {
// unsafe { ffi::ctest_counter_get(ToGlibPtr::to_glib_none(self).0) }
// }
// }

// The rust object
#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("tests/generated/c_derive.rs")]
    class RustCounter : Counter {
    }

    impl Counter for RustCounter {
        type GlibType = ::gobject_gen_test::auto::ffi::CtestCounter;

        virtual fn increment(&self, _foo: i32) {
            //Do nothing
        }

        property get_prop: T where T: i32 {
            get(&self) -> T {
                self.get() + 20
            }

            set(&self, value: T) {
                self.increment(value - (self.get() - 20))
            }
        }
    }
}

// This test checks if the virtual function really is used
#[test]
fn derived_class() {
    let c: RustCounter = RustCounter::new();

    println!("Counter has value {} and type {}", c.get(), unsafe {
        ffi::ctest_counter_get_type()
    });

    c.increment(2);
    c.increment(20);
    assert_eq!(c.get(), 0);

    println!("Counter has value: {}", c.get());
}

// This test checks if the c-function does what we expect.
#[test]
fn base_class() {
    let c: Counter = Counter::new();

    println!("Counter has value: {}", c.get());

    c.increment(2);
    c.increment(20);
    assert_eq!(c.get_property_get_prop(), 32);
    assert_eq!(c.get(), 22);

    println!("Counter has value: {}", c.get());
}

extern "C" {
    pub fn test_increment_in_c(this: *mut ffi::CtestCounter) -> ::libc::c_int;
}

#[test]
fn c_derived_class() {
    let _panic_on_message = test::PanicOnMessage::new();

    let c: RustCounter = RustCounter::new();

    let c_res = unsafe { test_increment_in_c(c.to_glib_none().0) } as i32;
    assert_eq!(c_res, 0);
}

#[test]
fn c_base_class() {
    let _panic_on_message = test::PanicOnMessage::new();

    let c: Counter = Counter::new();

    let c_res = unsafe { test_increment_in_c(c.to_glib_none().0) } as i32;
    assert_eq!(c_res, 23);
}
