#![cfg(ignore)]
#![cfg(any(not(feature = "single-test"), feature = "single-test-counter_c_impl"))]

extern crate gobject_class;
extern crate gobject_gen;
extern crate gobject_gen_test;

use ::gobject_gen_test::auto::ffi;
use ::gobject_gen_test::auto::{CounterImpl, MutableCounter, MutableCounterExt};

use ::gobject_class::glib::translate::ToGlibPtr;
use ::gobject_class::glib::Cast;

mod test;

extern "C" {
    fn test_increment_in_c_impl(counter: *mut ffi::CtestMutableCounter) -> ::libc::c_int;
}

// This checks the c-implementation of the counter called from c
#[test]
fn c_impl_c_test() {
    let _panic_on_message = test::PanicOnMessage::new();

    let c_counter = CounterImpl::new();
    let mutable_counter: MutableCounter = c_counter.upcast();

    unsafe { test_increment_in_c_impl(mutable_counter.to_glib_none().0) };

    assert_eq!(mutable_counter.get(), 23);
}

#[test]
fn c_impl_rust_test() {
    let _panic_on_message = test::PanicOnMessage::new();
    let c_counter = CounterImpl::new();
    let mutable_counter: MutableCounter = c_counter.upcast();

    mutable_counter.increment(18);
    mutable_counter.increment(3);

    assert_eq!(mutable_counter.get(), 21);
}
