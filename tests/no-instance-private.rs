#![cfg(any(
    not(feature = "single-test"),
    feature = "single-test-no-instance-private"
))]
#![deny(warnings)]

extern crate gobject_class;
extern crate gobject_gen;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

#[cfg(feature = "test-generated")]
include!("generated/no-instance-private-gen.rs");

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("tests/generated/no-instance-private-gen.rs")]
    class One {
    }

    impl One {
        pub fn get(&self) -> u32 {
            1
        }
    }
}

#[test]
fn test() {
    let one = One::new();
    assert!(one.get() == 1);
}
