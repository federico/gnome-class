#![cfg(any(not(feature = "single-test"), feature = "single-test-works-in-2015"))]

extern crate gobject_class;
extern crate gobject_gen;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

#[cfg(feature = "test-generated")]
include!("generated/works-in-2015-gen.rs");

use std::cell::Cell;

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("tests/generated/works-in-2015-gen.rs")]
    class Counter {
      f: Cell<u32>,
    }

    impl Counter {
        pub fn add(&self, x: u32) -> u32 {
            self.get_priv().f.set(self.get() + x);
            self.get()
        }

        pub fn get(&self) -> u32 {
            self.get_priv().f.get()
        }
    }
}

#[test]
fn test() {
    let c: Counter = Counter::new();

    c.add(2);
    c.add(20);
    assert_eq!(c.get(), 22);
}
