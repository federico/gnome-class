#![cfg(any(not(feature = "single-test"), feature = "single-test-variant"))]
#![deny(warnings)]

extern crate gobject_class;
extern crate gobject_gen;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

use std::cell::RefCell;

#[cfg(feature = "test-generated")]
include!("generated/variant-gen.rs");

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("tests/generated/variant-gen.rs")]
    class Counter2 {
      f: RefCell<Option<::glib::Variant>>,
    }

    impl Counter2 {
        pub fn set(&self, x: ::glib::Variant) {
            self.get_priv().f.replace(Some(x));
        }

        pub fn get(&self) -> ::glib::Variant {
            self.get_priv().f.borrow().clone().unwrap_or(0.into())
        }
    }
}

#[test]
fn test_variant() {
    let c: Counter2 = Counter2::new();

    println!("Counter has value: {:?}", c.get().get::<u32>());

    c.set(10u32.into());
    assert_eq!(c.get().get::<u32>(), Some(10u32));

    println!("Counter has value: {:?}", c.get().get::<u32>());
}
