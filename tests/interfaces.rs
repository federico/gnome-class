#![cfg(any(not(feature = "single-test"), feature = "single-test-interfaces"))]

extern crate gobject_class;
extern crate gobject_gen;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

#[cfg(feature = "test-generated")]
include!("generated/interfaces-gen.rs");

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("tests/generated/interfaces-gen.rs")]
    interface Frob {
        virtual fn frob(&self);

        signal fn foo_sig(&self, x: u32);
    }
}
