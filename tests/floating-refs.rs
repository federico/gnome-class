#![cfg(any(not(feature = "single-test"), feature = "single-test-floating-refs"))]

extern crate gobject_class;
extern crate gobject_gen;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

use gobject_class::glib::glib_object_wrapper;
use gobject_class::glib::glib_wrapper;

// glib_wrapper! wants these bindings
use glib_sys as glib_ffi;
use gobject_sys as gobject_ffi;
use std::mem;
use std::ptr;

use glib::translate::*;
use glib::{IsA, Object};

// The glib crate doesn't bind GInitiallyUnowned, so let's bind it here.
glib_wrapper! {
    pub struct InitiallyUnowned(
        Object<gobject_sys::GInitiallyUnowned, gobject_sys::GInitiallyUnownedClass,
               InitiallyUnownedClass>
    );

    match fn {
        get_type => || gobject_sys::g_initially_unowned_get_type(),
    }
}

#[cfg(feature = "test-generated")]
include!("generated/floating-refs-gen.rs");

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    // Make an instantiable class out of InitiallyUnowned
    #[generate("tests/generated/floating-refs-gen.rs")]
    class Floating: InitiallyUnowned {
    }

    impl Floating {
        virtual fn frob(&self) {
            println!("hello!");
        }
    }

    impl InitiallyUnowned for Floating
    {

    }

    // This will just have a method that doesn't take ownership of a Floating object
    #[generate("tests/generated/floating-refs-gen.rs")]
    class Foo {
    }

    impl Foo {
        virtual fn blah(&self, x: &Floating) {
            x.frob();
        }
    }
}

fn is_floating<T: IsA<Object>>(obj: &T) -> bool {
    from_glib(unsafe { gobject_sys::g_object_is_floating(obj.to_glib_none().0) })
}

#[test]
fn initially_unowned_is_floating() {
    let floating = Floating::new();

    assert!(is_floating(&floating));

    let foo = Foo::new();
    let _foo_is_floating: bool =
        unsafe { from_glib(gobject_sys::g_object_is_floating(foo.to_glib_none().0)) };

    assert!(!is_floating(&foo));

    foo.blah(&floating);

    assert!(is_floating(&floating));
}
