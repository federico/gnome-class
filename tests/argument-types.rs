#![cfg(any(not(feature = "single-test"), feature = "single-test-argument-types"))]
#![deny(warnings)]

extern crate gobject_class;
extern crate gobject_gen;

#[allow(dead_code)]
#[cfg(not(feature = "test-generated"))]
use gobject_gen::gobject_gen;

#[cfg(feature = "test-generated")]
include!("generated/arguments-types-gen.rs");

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("tests/generated/arguments-types-gen.rs")]
    class Foo {
    }

    impl Foo {
        pub fn method(&self) -> u32 {
            5
        }
    }
}

#[cfg(feature = "test-generated")]
include!("generated/arguments-types2-gen.rs");

#[cfg(not(feature = "test-generated"))]
gobject_gen! {
    #[generate("tests/generated/arguments-types2-gen.rs")]
    class Test {
    }

    impl Test {
        pub fn one(&self) -> bool {
            true
        }

        pub fn two(&self, a: bool, b: i32) -> bool {
            self.three(a) && b == b
        }

        fn three(&self, a: bool) -> bool {
            a
        }

        virtual fn four(&self, arg: bool) -> bool {
            self.three(arg)
        }

        pub fn five(&self) {
        }

        virtual fn six(&self) {
        }

        pub fn seven(&self, a: u32) {
            drop(a);
        }

        virtual fn eight(&self, b: usize) -> i8 {
            b as i8
        }

        virtual fn nine(&self, c: char) -> char {
            c
        }

        pub fn ten(&self, c: &Foo) -> bool {
            c.method() == self.method()
        }

        pub fn eleven(&self, c: &Test) -> bool {
            c.method() == self.method()
        }


        fn method(&self) -> u32 {
            3
        }

        pub fn twelve_string_len(&self, s: String) -> usize {
            s.len()
        }

        virtual fn thirteen_string_len(&self, s: String) -> usize {
            s.len()
        }

        virtual fn fourteen_return_string(&self) -> String {
            "Hello world".to_string()
        }
    }

    #[generate("tests/generated/arguments-types2-gen.rs")]
    class Test2: Test {
    }

    impl Test for Test2 {
    }
}

#[test]
fn test() {
    use glib_sys::*;

    let t = Test::new();
    assert!(t.two(true, 2));
    assert!(!t.two(false, 2));
    assert!(t.four(true));
    assert!(!t.four(false));
    assert_eq!(t.twelve_string_len("Hello world".to_string()), 11);
    assert_eq!(t.thirteen_string_len("Hello world".to_string()), 11);

    let t2 = Test2::new();
    assert_eq!(t2.thirteen_string_len("Hello world".to_string()), 11);
    assert_eq!(t2.fourteen_return_string(), "Hello world");

    let f = Foo::new();
    assert!(!t.ten(&f));
    assert!(t.eleven(&t));

    type T = <Test as glib::ObjectType>::GlibType;
    type F = <Foo as glib::ObjectType>::GlibType;

    // assert that the generated functions have the right type
    let _: unsafe extern "C" fn(*mut T) -> gboolean = TestMod::imp::test_one;
    let _: unsafe extern "C" fn(*mut T, gboolean, i32) -> gboolean = TestMod::imp::test_two;
    let _: unsafe extern "C" fn(*mut T, gboolean) -> gboolean = TestMod::imp::test_four;
    let _: unsafe extern "C" fn(*mut T) = TestMod::imp::test_five;
    let _: unsafe extern "C" fn(*mut T) = TestMod::imp::test_six;
    let _: unsafe extern "C" fn(*mut T, u32) = TestMod::imp::test_seven;
    let _: unsafe extern "C" fn(*mut T, usize) -> i8 = TestMod::imp::test_eight;
    let _: unsafe extern "C" fn(*mut T, u32) -> u32 = TestMod::imp::test_nine;
    let _: unsafe extern "C" fn(*mut T, *mut F) -> gboolean = TestMod::imp::test_ten;
    let _: unsafe extern "C" fn(*mut T, *mut T) -> gboolean = TestMod::imp::test_eleven;

    let _: unsafe extern "C" fn(*mut T, *mut libc::c_char) -> usize =
        TestMod::imp::test_twelve_string_len;

    let _: unsafe extern "C" fn(*mut T) -> *mut libc::c_char =
        TestMod::imp::test_fourteen_return_string;
}
