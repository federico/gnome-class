Contributing to gnome-class
===========================

Thank you for looking here!  This file tells you several ways in which
you can contribute to gnome-class.  We appreciate all kind of
contributions.

There is a **code of conduct** for contributors to gnome-class; please
see the file [`code-of-conduct.md`][coc].

* [Source repository](#source-repository)
* [Reporting bugs](#reporting-bugs)
* [Feature requests](#feature-requests)
* [Hacking on gnome-class](#hacking-on-gnome-class)

## Source repository

The main source repository for gnome-class is at gitlab.gnome.org.
You can view the web interface here:

https://gitlab.gnome.org/federico/gnome-class

Development happens in the master branch.  As of April 2018, we don't
have any stable branches; this is a completely new project.

If you need to publish a branch, feel free to do it at any
publically-accessible Git hosting service, although gitlab.gnome.org
makes things easier for the maintainers of gnome-class.

To work on the source code, you may find the "[Hacking on
gnome-class](#hacking-on-gnome-class)" section helpful.

## Reporting bugs

Please report bugs at https://gitlab.gnome.org/federico/gnome-class/issues

Examples of bugs include these:

* Incorrect code generation.  It's helpful if you can paste the
  invocation of the `gobject_gen!` macro that caused incorrect code to
  be generated, and any relevant compiler messages.  If you are not
  permitted to paste the macro invocation, please see if you can at
  least extract the relevant part of the generated source code.
  
* Warnings or assertions from glib/gobject.  Gnome-class intends to
  generate code that uses GObject in a semantically correct way.
  Please paste the warning, and a stack trace where it happens.
  **FIXME: describe how to get a stack trace of this from g_log()**
  
* Feature requests; see below.

## Feature requests

GObject and GType support a lot of esoteric options, and we don't
intend to support all of them just because they are there.  However,
we would love to know if an application or a library needs a
particular GObject feature that gnome-class does not support!

Please see the "[reporting bugs](#reporting-bugs)" section for
information about our bug tracking system; feature requests should be
directed there.

You may find it useful to consult the [syntax document][syntax] if you
want to propose a syntax extension to support your feature.

## Hacking on gnome-class

It is perfectly fine to [ask the maintainer][maintainer] if you have
questions about working on gnome-class.  We use a bunch of tricky
features in procedural macros, and we'll be glad to help.

Please read the [internals document][internals] ([rendered
version][internals-rendered], this is an [mdbook][mdbook] document);
this describes in detail how gnome-class works.  It is like a little
compiler, and this document describes the various stages, and how we
deal with GObject's peculiarities.

### The Continuous Integration pipeline

Gnome-class has as Continuous Integration (CI) pipeline in
gitlab.gnome.org that runs all the tests after every push to the
repository.  The CI configuration is in the
[`.gitlab-ci.yml`](.gitlab-ci.yml) file.

A little glossary:

* ***Continuous Integration (CI)*** - A tireless robot that builds
  gnome-class on every push, and runs various kinds of tests.
  
* ***Pipeline*** - A set of ***jobs*** that may happen on every push.
  Every pipeline has ***stages*** of things that get run.  You can
  [view recent
  pipelines](https://gitlab.gnome.org/federico/gnome-class/pipelines) and
  examine their status.
  
* ***Stages*** - Each stage runs some kind of test on gnome-class, and
  depends on the previous stages succeeding.  We have a *Test* stage
  that just builds gnome-class as quickly as possible and runs its test
  suite.  If that succeeds, then it will go to a *Lint* stage which
  runs `cargo fmt` to ensure that the coding style remains consistent.
  Finally, there is a `Pages` stage that regenerates our documentation
  and uploads it to a web page.
  
* ***Jobs*** - You can think of a job as "something that runs in a
  specific container image, and emits a success/failure result".  For
  example, the `Test` stage runs a job in a fast container with the
  Rust nightly compiler.  The `Lint` stage runs `cargo fmt` in a
  Rust-specific container that always contains a recent version of
  `rustfmt`.
  
The default CI pipeline for people's branches is set up to build your
branch and to run the test suite and lints.  If any tests fail, the
pipeline will fail and you can then examine the job's build
artifacts.  If the lint stage fails, you will have to reindent your
code.

***Automating the code formatting:*** You may want to enable a
[client-side git
hook](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) to run
`rustfmt` before you can commit something; otherwise the `Lint` stage
of CI pipelines will fail:

1. `cd gnome-class`

1. `mv .git/hooks/pre-commit.sample .git/hooks/pre-commit`

1. Edit `.git/hooks/pre-commit` and put in one of the following
   commands:

  * If you want code reformatted automatically, no questions asked:
    `cargo fmt --write-mode=overwrite`

  * If you want to examine error if rustfmt doesn't like your indentation,
    but don't want it to make changes on its own:
    `cargo fmt --write-mode=diff`

### Test suite

Please make sure that the test suite passes with the changes in your
branch.  The easiest way to run all the tests is to run `cargo test`.

Since gnome-class is a procedural macro, there are some peculiarities
around testing it.  **If you try to add a unit test inside
gnome-class/src with #[test] it will not work!** Please see the
[Testing Strategy][testing-strategy] section of the internals document
for information on how to add unit tests or integration tests.

The CI machinery will run gnome-class's test suite automatically when you
push some commits.  You can tweak what happens during CI for your
branch in the [`.gitlab-ci.yml` file](.gitlab-ci.yml).

### Creating a merge request

You may create a forked version of gnome-class in [GNOME's Gitlab
instance][gitlab], or any other publically-accesible Git hosting
service.  You can register an account there, or log in with your
account from other OAuth services.

[coc]: code-of-conduct.md
[syntax]: gobject-notes/syntax.md
[maintainer]: README.md#maintainers
[internals]: doc-internals/src/SUMMARY.md
[internals-rendered]: https://federico.pages.gitlab.gnome.org/gnome-class/book
[mdbook]: https://github.com/rust-lang-nursery/mdBook
[testing-strategy]: doc-internals/src/testing.md
[gitlab]: https://gitlab.gnome.org/federico/gnome-class
